using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using Library_Project.Entity.Entities.Abstract;
using Microsoft.IdentityModel.Tokens;

namespace Library_Project.Entity.Entities
{
    public class Cart : IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        public int AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }
        
        
    }
}