using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class BookOfAuthor:IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [ForeignKey("Author")]
        [Column("author_id")]
        public int? AuthorId { get; set; }
        public virtual Author Author { get; set; }
        
        [ForeignKey("Books")]
        [Column("book_id")]
        public int? BookId { get; set; }
        public virtual Book Book { get; set; }
        
        [ForeignKey("BookImage")]
        [Column("image_id")]
        public int? ImageId { get; set; }
        public virtual BookImage BookImage { get; set; }
        
    }
}