using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class ReturnedBook: IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        public int AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }
        
        [ForeignKey("BookOfAuthor")]
        [Column("boa_id")]
        public int BoaId { get; set; }
        public virtual BookOfAuthor BookOfAuthor { get; set; }
        
        [Column("quantity")]
        public int Quantity { get; set; }
        
        [Column("return_time")]
        public DateTime? ReturnTime { get; set; }
        
    }
}