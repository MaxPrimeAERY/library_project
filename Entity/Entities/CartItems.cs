using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class CartItems : IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [ForeignKey("Cart")]
        [Column("cart_id")]
        public int CartId { get; set; }
        public virtual Cart Cart { get; set; }
        
        [ForeignKey("BookOfAuthor")]
        [Column("boa_id")]
        public int BoaId { get; set; }
        public virtual BookOfAuthor BookOfAuthor { get; set; }
        
        [Column("quantity")]
        public int Quantity { get; set; }
    }
}