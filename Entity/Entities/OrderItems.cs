using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class OrderItems: IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [ForeignKey("Order")]
        [Column("order_id")]
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
        
        [ForeignKey("BookOfAuthor")]
        [Column("boa_id")]
        public int BoaId { get; set; }
        public virtual BookOfAuthor BookOfAuthor { get; set; }
        
        [Column("quantity")]
        public int Quantity { get; set; }
        
        [Column("get_time")]
        public DateTime? GetTime { get; set; }
        
    }
}