namespace Library_Project.Entity.Entities.Abstract
{
    public interface IBaseEntity<TKey>
    {
        TKey Id { get; }
    }
}