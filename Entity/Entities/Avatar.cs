using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class Avatar : IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [Column("image")]
        public string Image { get; set; }
        
        
        public int AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }
    }
}