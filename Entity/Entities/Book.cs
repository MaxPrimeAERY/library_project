using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class Book:IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [Column("bookname")]
        public string BookName { get; set; }
        
        [Column("genre")]
        public string Genre { get; set; }
        
        [Column("short_review")]
        public string ShortReview { get; set; }
        
    }
}