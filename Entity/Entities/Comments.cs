using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class Comments : IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        public int AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }
        
        [ForeignKey("BookOfAuthor")]
        [Column("boa_id")]
        public int BoaId { get; set; }
        public virtual BookOfAuthor BookOfAuthor { get; set; }

        [Column("text")]
        public string Text { get; set; }
        
        [Column("rating")]
        public int? Rating { get; set; }
        
        [Column("comment_time")]
        public DateTime? CommentTime { get; set; }

        [Column("viewed_by")]
        public int? ViewedBy { get; set; }
    }
}