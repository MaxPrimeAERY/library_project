using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class Order : IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        public int AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }

        [Column("order_time")]
        public DateTime? OrderTime { get; set; }

        [Column("order_status")]
        public string OrderStatus { get; set; }
    }
}