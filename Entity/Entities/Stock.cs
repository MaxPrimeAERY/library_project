using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.Entity.Entities
{
    public class Stock : IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [ForeignKey("BookOfAuthor")]
        [Column("boa_id")]
        public int boaId { get; set; }
        public virtual BookOfAuthor BookOfAuthor { get; set; }
        
        [Column("total_amount")]
        public int TotalAmount { get; set; }
        
        [Column("amount")]
        public int? Amount { get; set; }

    }
}