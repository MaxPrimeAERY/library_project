using Library_Project.Entity.Entities.Abstract;
using Microsoft.AspNetCore.Identity;

namespace Library_Project.Entity
{
    public class AppUser : IdentityUser<int>, IBaseEntity<int>
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual bool AllowComments { get; set; }
    }
}