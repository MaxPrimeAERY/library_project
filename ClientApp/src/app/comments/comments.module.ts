import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommentsRoutingModule } from './comments-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {ToastrModule} from "ngx-toastr";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {CommentsListComponent} from "./comments-list/comments-list.component";
import {CommentsByBookofauthorComponent} from "./comments-by-bookofauthor/comments-by-bookofauthor.component";
import {MatSelectModule} from "@angular/material";
import {NgxPaginationModule} from "ngx-pagination";

@NgModule({
  declarations: [
    CommentsListComponent,
    CommentsByBookofauthorComponent
  ],
  imports: [
    CommonModule,
    CommentsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    ToastrModule.forRoot(),
    MatSelectModule,
    NgxPaginationModule
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}],
  exports: [
    CommentsByBookofauthorComponent
  ],
  //multi - for several interceptors
})
export class CommentsModule { }
