import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {MyToastrService} from "../../services/my-toastr.service";
import {Comments} from "../../interfaces/comments";
import {CommentsService} from "../../services/comments.service";
import {ProfileService} from "../../services/profile.service";
import {BookOfAuthorService} from "../../services/bookofauthor.service";

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss']
})
export class CommentsListComponent implements OnInit, OnDestroy {

  // insertForm: FormGroup;
  // appUserId: FormControl;
  // boaId: FormControl;
  // text: FormControl;
  // rating: FormControl;
  // commentTime: FormControl;
  // viewedBy: FormControl;

  userIdAsNumber: number;

  updateForm: FormGroup;
  _appUserId: FormControl;
  _boaId: FormControl;
  _text: FormControl;
  _rating: FormControl;
  _commentTime: FormControl;
  _viewedBy: FormControl;
  _id: FormControl;

  // Add Modal
  @ViewChild('template', { static: true }) modal : TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', { static: true }) editmodal : TemplateRef<any>;


  // Modal properties
  modalMessage : string;
  modalRef : BsModalRef;
  selectedComments : Comments;
  comments$ : Observable<Comments[]>;
  comments : Comments[] = [];
  userRoleStatus : string;


  userName: string[] = [];
  authorname: string[] = [];
  bookname: string[] = [];
  formatDateTime: string = 'dd.MM.yyyy HH:mm:ss';


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;


  constructor(private commentservice : CommentsService,
              private profileService: ProfileService,
              private bookOfAuthorService: BookOfAuthorService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef : ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) { }


  // We will use this method to destroy old table and re-render new table

  rerender()
  {
    this.dtElement.dtInstance.then((dtInstance : DataTables.Api) =>
    {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing Comments
  onUpdate()
  {
    let editComments = this.updateForm.value;
    this.commentservice.updateComment(editComments.id, editComments).subscribe(
      result =>
      {
        this.myToastrService.messageSuccess('Comment Updated');
        this.commentservice.clearCache();
        this.comments$ = this.commentservice.getComments();
        this.comments$.subscribe(updatedlist =>
        {
          this.comments = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageError('Could Not Update Comment')
    )
  }

  // Load the update Modal

  onUpdateModal(commentEdit: Comments) : void
  {
    this._id.setValue(commentEdit.id);
    this._text.setValue(commentEdit.text);
    this._rating.setValue(commentEdit.rating);
    this._viewedBy.setValue(commentEdit.viewedBy);

    this.updateForm.setValue({
      'id' : this._id.value,
      'text' : this._text.value,
      'rating' : this._rating.value,
      'viewedBy' : this._viewedBy.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the comment
  onDelete(comment : Comments) : void
  {
    this.commentservice.deleteComment(comment.id).subscribe(result =>
    {
      this.myToastrService.messageSuccess('Comment is deleted successfully');
      this.commentservice.clearCache();
      this.comments$ = this.commentservice.getComments();
      this.comments$.subscribe(newlist =>
      {
        this.comments = newlist;

        this.rerender();
      })
    })
  }

  onSelect(comment: Comments) : void
  {
    this.selectedComments = comment;

    this.router.navigateByUrl("/comments/" + comment.id);
  }

  async ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    await this.accs.currentUserId.subscribe(result => {
      this.userIdAsNumber = +result;
    });

    this.comments$ = this.commentservice.getComments();

    this.comments$.subscribe(async result => {
      this.comments = result;
      //console.log("List comments = " + JSON.stringify(this.comments));
      for (let i = 0; i < this.comments.length; i++) {
        await this.profileService.getProfileById(this.comments[i].appUserId).then(res => {

            this.userName.push(res.userName);

          }
        );
      }
      for (let i = 0; i < this.comments.length; i++) {
        await this.bookOfAuthorService.getAuthorBookById(this.comments[i].boaId).then(res => {
            this.authorname.push(res.name);
            this.bookname.push(res.bookname);

          }
        );
      }
      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";


    // Initializing Update Comments properties
    this._text = new FormControl('', [Validators.required]);
    this._rating = new FormControl('');
    this._viewedBy = new FormControl('');
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id': this._id,
        'text': this._text,
        'rating': this._rating,
        'viewedBy': this._viewedBy

      });

  }

  ngOnDestroy()
  {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
