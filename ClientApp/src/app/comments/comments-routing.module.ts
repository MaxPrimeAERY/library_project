import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {CommentsListComponent} from "./comments-list/comments-list.component";
import {CommentsByBookofauthorComponent} from "./comments-by-bookofauthor/comments-by-bookofauthor.component";


const routes: Routes = [
  {path: '', component: CommentsListComponent, canActivate: [AuthGuardService]},
  {path: 'comments-list', component: CommentsListComponent, canActivate: [AuthGuardService]},
  {path: 'comments-by-book-of-author-list', component: CommentsByBookofauthorComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class CommentsRoutingModule {
}
