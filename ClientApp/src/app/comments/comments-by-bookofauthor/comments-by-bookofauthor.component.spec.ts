import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsByBookofauthorComponent } from './comments-by-bookofauthor.component';

describe('CommentsByBookofauthorComponent', () => {
  let component: CommentsByBookofauthorComponent;
  let fixture: ComponentFixture<CommentsByBookofauthorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsByBookofauthorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsByBookofauthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
