import {
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {AccountService} from "../../services/account.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BookOfAuthorService} from "../../services/bookofauthor.service";
import {MyToastrService} from "../../services/my-toastr.service";
import {CommentsService} from "../../services/comments.service";
import {Comments} from "../../interfaces/comments";
import {ProfileService} from "../../services/profile.service";
import {AvatarService} from "../../services/avatar.service";
import {DomSanitizer} from "@angular/platform-browser";
import {HelperService} from "../../services/helper.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {SimpleProfile} from "../../interfaces/simple-profile";

@Component({
  selector: 'app-comments-by-bookofauthor',
  templateUrl: './comments-by-bookofauthor.component.html',
  styleUrls: ['./comments-by-bookofauthor.component.scss']
})
export class CommentsByBookofauthorComponent implements OnInit {

  insertForm: FormGroup;
  appUserId: FormControl;
  boaId: FormControl;
  text: FormControl;
  rating: FormControl;
  commentTime: FormControl;
  viewedBy: FormControl;

  deleteForm: FormGroup;
  _id: FormControl;

  userRoleStatus: string;

  @Input() simpleProfile: SimpleProfile;
  hasPhoto = false;
  imageSrc: string = '';

  bookIdAsNumber: number;
  userIdAsNumber: number;
  comments: Comments[] = [];

  myCommentsCounter: number = 0;
  p: number = 1;

  firstname: string[] = [];
  lastname: string[] = [];

  checkedByFirstname: string[] = [];
  checkedByLastname: string[] = [];

  avatar: string[] = [];

  formatDateTime: string = 'dd.MM.yyyy HH:mm:ss';

  modalMessage: string;
  modalRef: BsModalRef;

  @ViewChild('deleteTemplate', {read: TemplateRef, static: false}) deletemodal: TemplateRef<any>;

  private hoveredElement: any;

  constructor(
    private accs: AccountService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer,
    private modalService: BsModalService,
    private bookOfAuthorService: BookOfAuthorService,
    private commentsService: CommentsService,
    private avatarService: AvatarService,
    private profileService: ProfileService,
    private helperService: HelperService,
    private myToastrService: MyToastrService
  ) {
  }

  async ngOnInit() {
    this.bookIdAsNumber = +this.route.snapshot.params['id'];
    //console.log("Book of author = " + this.bookIdAsNumber);

    await this.accs.currentUserId.subscribe(result => {
      this.userIdAsNumber = +result;
    });

    //console.log("userId = " + this.userIdAsNumber);

    if (this.userIdAsNumber > 0) {
      await this.profileService.getSimpleProfile(this.userIdAsNumber).then(result => {
        this.simpleProfile = result;
        if (result.image != null) {
          this.simpleProfile.image = this.sanitizer.bypassSecurityTrustUrl(result.image);
          this.imageSrc = result.image;
          this.hasPhoto = true;
        }
      });

      await this.loadCommentList();

      let date = new Date();

      this.appUserId = new FormControl(this.userIdAsNumber);
      this.boaId = new FormControl(this.bookIdAsNumber);
      this.text = new FormControl('', [Validators.required, Validators.maxLength(500), Validators.minLength(1)]);
      this.rating = new FormControl('');
      this.commentTime = new FormControl(this.helperService.fixDate(date));
      //console.log("Comment time = "+this.commentTime.value);
      this.viewedBy = new FormControl('');


      this.insertForm = this.formBuilder.group({

        'appUserId': this.appUserId,
        'boaId': this.boaId,
        'text': this.text,
        'rating': this.rating,
        'commentTime': this.commentTime,
        'viewedBy': this.viewedBy

      });

      this._id = new FormControl();

      this.deleteForm = this.formBuilder.group(
        {
          'id': this._id
        });
    } else {
      await this.loadCommentList();
    }

    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });
  }

  loadCommentList() {
    this.commentsService.getByBookOfAuthorId(this.bookIdAsNumber).subscribe(async result => {
      this.comments = result;

      this.myCommentsCounter = 0;
      for (let myCommentsCounter of result) {
        this.myCommentsCounter = this.myCommentsCounter + 1;
      }

      for (let i = 0; i < this.comments.length; i++) {
        await this.profileService.getSimpleProfile(this.comments[i].appUserId).then(res => {

            this.firstname.push(res.firstName);
            this.lastname.push(res.lastName);

          }
        );

        if (this.comments[i].viewedBy !== null) {
          await this.profileService.getSimpleProfile(this.comments[i].viewedBy).then(res => {

              this.checkedByFirstname.push(res.firstName);
              this.checkedByLastname.push(res.lastName);

            }
          );
        } else {
          this.checkedByFirstname.push(null);
          this.checkedByLastname.push(null);
        }

        await this.avatarService.getAvatarByUserId(this.comments[i].appUserId).subscribe(res => {
            try {
              this.avatar.push(res.image);
            } catch (e) {
              //console.log(e);
            }

          }
        );
      }
    });
  }

  // Method to Add new Comment
  onSubmit() {

    if (this.accs.checkLogInStatus() == true) {

      this.profileService.getSimpleProfile(this.userIdAsNumber).then(result => {
        if (result.allowComments == false) {
          this.myToastrService.messageError("You are not allowed to write comments any more :(");
          return;

        } else {
          let date = new Date();
          this.commentTime = new FormControl(this.helperService.fixDate(date));

          this.insertForm = this.formBuilder.group({

            'appUserId': this.appUserId,
            'boaId': this.boaId,
            'text': this.text,
            'rating': this.rating,
            'commentTime': this.commentTime,
            'viewedBy': this.viewedBy

          });

          let newComment = this.insertForm.value;

          this.commentsService.insertComment(newComment).then(
            result => {
              this.commentsService.clearCache();
              this.loadCommentList();
              this.myToastrService.messageSuccess("Your comment added");
              window.location.reload();

            },
            error => this.myToastrService.messageError('Could not add comment')
          )
        }
      });

    } else {
      this.myToastrService.messageWarning("You have to login first!");
    }
  }

  //this.myToastrService.messageWarning("You have to login first!");
  onDeleteMyCommentModal(commentDelete: Comments): void {

    if (this.accs.checkLogInStatus() == true) {

      this._id.setValue(commentDelete.id);

      this.deleteForm.setValue({
        'id': this._id.value
      });

      this.modalRef = this.modalService.show(this.deletemodal);
    } else {
      this.myToastrService.messageWarning("You have to login first!");
    }
  }

  // Method to Delete the comment
  onDeleteMyComment(): void {

    let deleteComment = this.deleteForm.value;

    this.commentsService.deleteMyComment(deleteComment.id, this.userIdAsNumber).then(result => {
        this.myToastrService.messageSuccess('Your comment is deleted successfully');
        this.commentsService.clearCache();
        this.modalRef.hide();

        this.loadCommentList();
        window.location.reload();
      },
      error => this.myToastrService.messageError('Could not delete comment')
    )
  }

  // Method to Delete the comment
  onDelete(comment: Comments): void {

    if (this.accs.checkLogInStatus() == true) {

      this.commentsService.deleteComment(comment.id).subscribe(result => {
          this.myToastrService.messageSuccess('Comment is deleted successfully');
          this.commentsService.clearCache();
          this.loadCommentList();
          window.location.reload();

        }, error => this.myToastrService.messageError('Could not delete comment')
      )
    } else {
      this.myToastrService.messageWarning("You have to login first!");
    }
  }

  toggleHover(id) {
    //console.log("We entered");
    this.hoveredElement = id
  }

  removeHover(id) {
    //console.log("We left");
    this.hoveredElement = null;
  }

}
