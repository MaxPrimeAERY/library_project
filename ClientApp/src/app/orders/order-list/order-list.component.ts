import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {Order} from "../../interfaces/order";
import {OrderService} from "../../services/order.service";
import {DataTableDirective} from "angular-datatables";
import {OrderItemsService} from "../../services/order-items.service";
import {OrderInfo} from "../../interfaces/order-info";
import {OrderItems} from "../../interfaces/order-items";
import {StockService} from "../../services/stock.service";
import {ProfileService} from "../../services/profile.service";
import {Profile} from "../../interfaces/profile";
import {HelperService} from "../../services/helper.service";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  appUserId: FormControl;
  orderTime: FormControl;
  orderStatus: FormControl;

  updateForm: FormGroup;
  _appUserId: FormControl;
  _orderTime: FormControl;
  _orderStatus: FormControl;
  _id: FormControl;

  differenceDate: any;
  myDayCounter: number = 1.728 * Math.pow(10, 8); //2 days before auto delete


  profile: Profile[] = [];
  userName: string[] = [];

  //////////////////////////////

  updateFormItems: FormGroup;
  _boaId: FormControl;
  _quantity: FormControl;
  _getTime: FormControl;
  _idO: FormControl;

  formatDateTime: string = 'dd.MM.yyyy HH:mm:ss';

  //////////////////////////////

  stockId: number;
  amountBook: number;
  bookOfAuthorId: FormControl;
  amountForm: FormControl;
  idStockForm: FormControl;
  updateStockForm: FormGroup;
  modalMessageErr: string;

  currentOrderStatus: string;

  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;

  @ViewChild('editTemplateItem', {static: true}) editmodalitem: TemplateRef<any>;

  @ViewChild('errorTemplate', {static: true}) modalError: TemplateRef<any>;

  //Order statuses
  newOrder: string = "New order";
  ready: string = "Ready";
  finished: string = "Finished";

  // Modal properties
  modalMessage: string;
  modalRef: BsModalRef;
  selectedOrder: Order;
  orders$: Observable<Order[]>;
  orders: Order[] = [];
  userRoleStatus: string;

  selectedOrderInfo: OrderInfo;
  orderInfo$: Observable<OrderInfo[]>;
  orderInfo: OrderInfo[] = [];

  rowSelected: number;

  orderItems$: Observable<OrderItems[]>;
  orderItems: OrderItems[] = [];


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private orderservice: OrderService,
              private orderItemsService: OrderItemsService,
              private stockService: StockService,
              private helperService: HelperService,
              private profileService: ProfileService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private fbItems: FormBuilder,
              private formBuilderStock: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) {

  }

/// Load Add New order Modal
  onAddOrder() {
    this.modalRef = this.modalService.show(this.modal);
  }

  // Method to Add new Order
  onSubmit() {
    let newOrder = this.insertForm.value;

    this.orderservice.insertOrder(newOrder).then(
      result => {
        this.orderservice.clearCache();
        this.orders$ = this.orderservice.getOrders();

        this.orders$.subscribe(newlist => {
          this.orders = newlist;

          this.modalRef.hide();
          this.insertForm.reset();
          this.rerender();

        });
        this.myToastrService.messageSuccess("New Order added");

      },
      error => this.myToastrService.messageError('Could not add Order')
    )

  }

  // We will use this method to destroy old table and re-render new table

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing Order
  async onUpdate() {
    let editOrder = this.updateForm.value;
    await this.orderservice.updateOrder(editOrder.id, editOrder).then(
      result => {
        this.myToastrService.messageSuccess("Order Updated");
        this.orderservice.clearCache();
        this.orders$ = this.orderservice.getOrders();
        this.orders$.subscribe(updatedlist => {
          this.orders = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageError('Could Not Update Order')
    );
    await this.orderservice.getOrderById(editOrder.id).then(res => {
      if (res.orderStatus == this.finished) {
        this.orderItemsService.getOrderInfoByOrderId(editOrder.id).then(async resultIn => {
          //let tempDate = this.helperService.fixDate(date);
          for (let localItem of resultIn) {
            let date = new Date();

            this._idO.setValue(localItem.id);
            this._boaId.setValue(localItem.boaId);
            this._quantity.setValue(localItem.quantity);
            this._getTime.setValue(this.helperService.fixDate(date));

            this.updateFormItems.patchValue({
              'id': this._idO.value,
              'boaId': this._boaId.value,
              'quantity': this._quantity.value,
              'getTime': this._getTime.value
            });

            let editOrderItems = this.updateFormItems.value;
            await this.orderItemsService.updateOrderItems(editOrderItems.id, editOrderItems).then();
            await window.location.reload();
          }

        });

      }else {
        this.orderItemsService.getOrderInfoByOrderId(editOrder.id).then(async resultIn => {
          //let tempDate = this.helperService.fixDate(date);
          for (let localItem of resultIn) {

            this._idO.setValue(localItem.id);
            this._boaId.setValue(localItem.boaId);
            this._quantity.setValue(localItem.quantity);
            this._getTime.setValue(null);

            this.updateFormItems.patchValue({
              'id': this._idO.value,
              'boaId': this._boaId.value,
              'quantity': this._quantity.value,
              'getTime': this._getTime.value
            });

            let editOrderItems = this.updateFormItems.value;
            await this.orderItemsService.updateOrderItems(editOrderItems.id, editOrderItems).then();
            await window.location.reload();
          }

        });
      }
    });
  }

  // Load the update Modal

  onUpdateModal(orderEdit: Order): void {
    this._id.setValue(orderEdit.id);
    this._appUserId.setValue(orderEdit.appUserId);
    //this._orderTime.setValue(orderEdit.orderTime);
    this._orderStatus.setValue(orderEdit.orderStatus);

    this.updateForm.patchValue({
      'id': this._id.value,
      'appUserId': this._appUserId.value,
      'orderStatus': this._orderStatus.value
    });
    this.modalRef = this.modalService.show(this.editmodal);

  }

  onUpdateItem() {
    let editOrderItems = this.updateFormItems.value;
    this.orderItemsService.updateOrderItems(editOrderItems.id, editOrderItems).then(
      result => {
        //console.log('Order item Updated');
        this.orderItemsService.clearCache();////////////////
        this.orderInfo$ = this.orderItemsService.getOrderInfo();
        this.orderInfo$.subscribe(updatedlist => {
          this.orderInfo = updatedlist;

          this.modalRef.hide();
          this.rerender();
          window.location.reload();
        });
      },
      error => console.log('Could Not Update Orderitem')
    )
  }

  onUpdateModalItem(orderInfoEdit: OrderInfo): void {
    this._idO.setValue(orderInfoEdit.id);
    this._boaId.setValue(orderInfoEdit.boaId);
    this._quantity.setValue(orderInfoEdit.quantity);
    this._getTime.setValue(orderInfoEdit.getTime);


    this.updateFormItems.patchValue({
      'id': this._idO.value,
      'boaId': this._boaId.value,
      'quantity': this._quantity.value,
      'getTime': this._getTime.value
    });

    this.modalRef = this.modalService.show(this.editmodalitem);

  }

  // Method to Delete the order
  async onDelete(order: Order) {
    await this.orderservice.getOrderById(order.id).then(result => {
      this.currentOrderStatus = result.orderStatus;
    });

    if (this.currentOrderStatus === this.finished) {
      this.modalRef = this.modalService.show(this.modalError);
      return;
    } else {

      this.orderItemsService.getByOrderId(order.id).subscribe(async res => {

        for (let localItem of res) {

          if (localItem.getTime === null) {

            await this.stockService.getStockByBookOfAuthorId(localItem.boaId).then(result => {
              this.stockId = result.id;
              this.amountBook = result.amount;
              this.amountBook = this.amountBook + +localItem.quantity;

              if (this.amountBook > result.totalAmount) {
                this.amountBook = result.totalAmount;
              }
            });


            this.bookOfAuthorId = new FormControl(localItem.boaId);
            this.amountForm = new FormControl(this.amountBook);
            this.idStockForm = new FormControl(this.stockId);

            this.updateStockForm = this.formBuilderStock.group({
              'id': this.idStockForm,
              'boaId': this.bookOfAuthorId,
              'amount': this.amountForm

            });

            let editedStock = this.updateStockForm.value;
            await this.stockService.patchStock(editedStock.id, editedStock).then();

          }
        }

        this.orderservice.deleteOrder(order.id).subscribe(result => {
          this.myToastrService.messageSuccess('Order is deleted successfully');

          this.orderservice.clearCache();
          this.orders$ = this.orderservice.getOrders();
          this.orders$.subscribe(newlist => {
            this.orders = newlist;

            this.rerender();
          });
        });
      });
    }
  }

  onDeleteItem(orderInfo: OrderInfo): void {

    this.orderservice.getOrderById(orderInfo.orderId).then(result => {
        if (result.orderStatus !== this.finished) {
          this.orderItemsService.getOrderItemsById(orderInfo.id).then(async res => {
            if (res.getTime === null) {

              await this.stockService.getStockByBookOfAuthorId(orderInfo.boaId).then(result => {
                this.stockId = result.id;
                this.amountBook = result.amount;
                this.amountBook = this.amountBook + +res.quantity;
                if (this.amountBook > result.totalAmount) {
                  this.amountBook = result.totalAmount
                }
              });


              this.bookOfAuthorId = new FormControl(orderInfo.boaId);
              this.amountForm = new FormControl(this.amountBook);
              this.idStockForm = new FormControl(this.stockId);

              this.updateStockForm = this.formBuilderStock.group({
                'id': this.idStockForm,
                'boaId': this.bookOfAuthorId,
                'amount': this.amountForm

              });

              let editedStock = this.updateStockForm.value;
              await this.stockService.patchStock(editedStock.id, editedStock).then();

              await this.orderItemsService.deleteOrderItems(orderInfo.id).subscribe(result => {
                this.myToastrService.messageSuccess('Order item is deleted successfully');

                this.orderItemsService.clearCache();
                this.orderItems$ = this.orderItemsService.getOrderItems();
                this.orderItems$.subscribe(newlist => {
                  this.orderItems = newlist;

                  window.location.reload();
                })
              })


            } else {

              this.modalRef = this.modalService.show(this.modalError);
              return;

            }
          });
        } else {
          this.modalRef = this.modalService.show(this.modalError);
          return;
        }
      }
    );

  }

  onSelect(order: Order): void {
    this.selectedOrder = order;
    this.orderItemsService.getByOrderId(order.id).subscribe(result => {
      this.orderItems = result;
      //console.log("My orders info = " + JSON.stringify(this.orderItems));
      this.chRef.detectChanges();
      //this.dtTrigger.next();

    });

    //this.router.navigateByUrl("/orders/" + order.id);
  }

  openCloseRow(idReserva: number): void {

    //console.log("Id reserva = " + idReserva);

    if (this.rowSelected === -1) {
      this.rowSelected = idReserva
    } else {
      if (this.rowSelected == idReserva) {
        this.rowSelected = -1
      } else {
        this.rowSelected = idReserva
      }
    }

    this.orderItemsService.getOrderInfoByOrderId(idReserva).then(result => {
      this.orderInfo = result;
      //console.log("My orders info = " + JSON.stringify(this.orderInfo));
      if (this.orderInfo.length < 1) {
        this.orderservice.deleteOrder(idReserva).subscribe();
        window.location.reload();
      }
      this.chRef.detectChanges();
      //this.dtTrigger.next();

    });
  }

  ngOnInit() {

    this.rowSelected = -1;
    this.dtOptions = {
      search: true,
      searching: true,
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      paging: false,
      info: false,
      order: [[0, 'asc']]
    };

    //console.log("Tester = " + new Date());

    this.orders$ = this.orderservice.getOrders();

    this.orders$.subscribe(async result => {
      this.orders = result;
      //console.log("List orders = " + JSON.stringify(this.orders));
      for (let i = 0; i < this.orders.length; i++) {
        await this.profileService.getProfileById(this.orders[i].appUserId).then(res => {

            this.userName.push(res.userName);

          }
        );
      }
      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.orderItems$ = this.orderItemsService.getOrderItems();
    this.orderItems$.subscribe(result => {
      this.orderItems = result;
      //console.log("List ordersItems = " + JSON.stringify(this.orderItems));

      this.chRef.detectChanges();
    });

    this.orderInfo$ = this.orderItemsService.getOrderInfo();
    this.orderInfo$.subscribe(result => {
      this.orderInfo = result;
      //console.log("List ordersInfo = " + JSON.stringify(this.orderInfo));

      this.chRef.detectChanges();
    });

    this.modalMessageErr = "You can't delete finished order";

    //console.log("My orders info = "+JSON.stringify(this.ordersInfo));


    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    this.orders$.subscribe(async result => {
      this.orders = result;
      for (let localOrder of this.orders) {
        let currentDate = new Date() as any;
        let orderDate = new Date(localOrder.orderTime) as any;
        this.differenceDate = currentDate - orderDate;
        //console.log("Difference = " + this.differenceDate + " order id " + localOrder.id);
        //console.log("New order" + "||" + this.newOrder);
        if (localOrder.orderStatus !== this.finished && (this.differenceDate > this.myDayCounter)) {

          this.onDelete(localOrder);
        }

      }
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add order properties

    //let validateImageUrl: string = '^(https?:\/\/.*\.(?:png|jpg))$';

    this.appUserId = new FormControl('', [Validators.required]);
    this.orderTime = new FormControl('', [Validators.required]);
    this.orderStatus = new FormControl('', [Validators.required]);


    this.insertForm = this.fb.group({

      'appUserId': this.appUserId,
      'orderTime': this.orderTime,
      'orderStatus': this.orderStatus

    });

    // Initializing Update Order properties
    this._appUserId = new FormControl('', [Validators.required]);
    this._orderTime = new FormControl('', [Validators.required]);
    this._orderStatus = new FormControl('', [Validators.required]);
    this._id = new FormControl();


    this.updateForm = this.fb.group(
      {
        'id': this._id,
        'appUserId': this._appUserId,
        //'orderTime': this._orderTime,
        'orderStatus': this._orderStatus

      });

    this._boaId = new FormControl('', [Validators.required]);
    this._quantity = new FormControl('', [Validators.required]);
    this._getTime = new FormControl('', [Validators.required]);
    this._idO = new FormControl();

    this.updateFormItems = this.fbItems.group(
      {
        'id': this._idO,
        'boaId': this._boaId,
        'quantity': this._quantity,
        'getTime': this._getTime

      });

    //console.log("Update form " + JSON.stringify(this.updateForm.value));

    this.profileService.getProfiles().subscribe(res => {
      this.profile = res;
    });

  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
