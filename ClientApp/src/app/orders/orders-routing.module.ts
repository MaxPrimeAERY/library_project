import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {OrderListComponent} from "./order-list/order-list.component";


const routes: Routes = [
  {path: '', component: OrderListComponent, canActivate: [AuthGuardService]},
  {path: 'order-list', component:OrderListComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class OrdersRoutingModule { }
