import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Order} from "../../interfaces/order";
import {OrderService} from "../../services/order.service";
import {Observable, Subject} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {OrderInfo} from "../../interfaces/order-info";
import {OrderItems} from "../../interfaces/order-items";
import {DataTableDirective} from "angular-datatables";
import {OrderItemsService} from "../../services/order-items.service";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";

@Component({
  selector: 'app-orders-by-user-id',
  templateUrl: './orders-by-user-id.component.html',
  styleUrls: ['./orders-by-user-id.component.scss']
})
export class OrdersByUserIdComponent implements OnInit, OnDestroy {


  formatDateTime: string = 'dd.MM.yyyy HH:mm:ss';


  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;

  userIdAsNumber: number;

  selectedOrder: Order;
  orders$: Observable<Order[]>;
  orders: Order[] = [];
  userRoleStatus: string;

  selectedOrderInfo: OrderInfo;
  orderInfo$: Observable<OrderInfo[]>;
  orderInfo: OrderInfo[] = [];

  rowSelected: number;

  orderItems$: Observable<OrderItems[]>;
  orderItems: OrderItems[] = [];


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private orderservice: OrderService,
              private orderItemsService: OrderItemsService,
              private fb: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService) {
  }


  // We will use this method to destroy old table and re-render new table

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }


  openCloseRow(userId: number, orderId: number): void {

    //console.log("Id order = "+orderId);

    if (this.rowSelected === -1) {
      this.rowSelected = orderId
    } else {
      if (this.rowSelected == orderId) {
        this.rowSelected = -1
      } else {
        this.rowSelected = orderId
      }
    }

    this.orderInfo$ = this.orderItemsService.getOrderInfoByUserOrderId(userId, orderId);
    this.orderInfo$.subscribe(result => {
      this.orderInfo = result;
      //console.log("My orders info = " + JSON.stringify(this.orderInfo));
      this.chRef.detectChanges();
      //this.dtTrigger.next();

    });
  }

  async ngOnInit() {

    this.rowSelected = -1;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    await this.accs.currentUserId.subscribe(result => {
      this.userIdAsNumber = +result;
    });

    this.orders$ = await this.orderservice.getOrderByUserId(this.userIdAsNumber);

    this.orders$.subscribe(result => {
      this.orders = result;
      //console.log("List orders = " + JSON.stringify(this.orders));


      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.orderInfo$ = await this.orderItemsService.getOrderInfoByUserId(this.userIdAsNumber);
    this.orderInfo$.subscribe(result => {
      this.orderInfo = result;
      //console.log("List ordersItems = " + JSON.stringify(this.orders));

      this.chRef.detectChanges();
    });


    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });

  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
