import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersByUserIdComponent } from './orders-by-user-id.component';

describe('OrdersByUserIdComponent', () => {
  let component: OrdersByUserIdComponent;
  let fixture: ComponentFixture<OrdersByUserIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersByUserIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersByUserIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
