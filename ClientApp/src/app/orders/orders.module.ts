import {forwardRef, LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrdersRoutingModule} from './orders-routing.module';
import {OrderListComponent} from './order-list/order-list.component';
import {FormsModule,  ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";

import localeUk from '@angular/common/locales/uk';
import localeUkExtra from '@angular/common/locales/extra/uk';
import {registerLocaleData} from "@angular/common";
import {MatOptionModule, MatSelectModule} from "@angular/material";
import {ToastrModule} from "ngx-toastr";
registerLocaleData(localeUk, localeUkExtra);

@NgModule({
  declarations: [
    OrderListComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    MatSelectModule,
    MatOptionModule,
    ToastrModule.forRoot()

  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors
})
export class OrdersModule {
}
