import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookImageListComponent } from './bookimage-list.component';

describe('BookImageListComponent', () => {
  let component: BookImageListComponent;
  let fixture: ComponentFixture<BookImageListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookImageListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookImageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
