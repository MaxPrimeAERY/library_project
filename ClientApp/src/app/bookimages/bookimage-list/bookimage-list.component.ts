import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {DomSanitizer} from "@angular/platform-browser";
import {BookImage} from "../../interfaces/book-image";
import {BookImageService} from "../../services/book-image.service";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-bookimage-list',
  templateUrl: './bookimage-list.component.html',
  styleUrls: ['./bookimage-list.component.scss']
})
export class BookImageListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  image: FormControl;
  imageSrc: string = '';
  hasImage = false;

  updateForm: FormGroup;
  _image: FormControl;
  _id: FormControl;

  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;


  // Modal properties
  modalMessage: string;
  modalRef: BsModalRef;
  selectedBookImage: BookImage;
  bookImages$: Observable<BookImage[]>;
  bookImages: BookImage[] = [];
  userRoleStatus: string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private bookImageservice: BookImageService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private sanitizer: DomSanitizer,
              private myToastrService: MyToastrService) {
  }


  /// Load Add New avatar Modal
  onAddBookImage() {
    this.imageSrc = null;
    this.hasImage = false;
    if (this.imageSrc == null) {
      this.insertForm.reset();
      this.rerender();
    }
    this.modalRef = this.modalService.show(this.modal);

  }

  transform() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.image.value);
  }

  // Method to Add new BookImage
  onSubmit() {
    let newBookImage = this.insertForm.value;

    this.bookImageservice.insertBookImage(newBookImage).subscribe(
      result => {
        this.bookImageservice.clearCache();
        this.bookImages$ = this.bookImageservice.getBookImages();

        this.bookImages$.subscribe(newlist => {
          this.bookImages = newlist;
          this.modalRef.hide();
          //console.log("This is render log 2" + this.image.value);
          this.insertForm.reset();
          this.rerender();
          //console.log("This is render log 3" + this.image.value);

        });
        this.myToastrService.messageSuccess("New BookImage added");

      },
      error => this.myToastrService.messageError('Could not add BookImage')
    )

  }

  // We will use this method to destroy old table and re-render new table

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing BookImage
  onUpdate() {
    let editBookImage = this.updateForm.value;
    this.bookImageservice.updateBookImage(editBookImage.id, editBookImage).subscribe(
      result => {
        this.myToastrService.messageSuccess('BookImage Updated');
        this.bookImageservice.clearCache();
        this.bookImages$ = this.bookImageservice.getBookImages();
        this.bookImages$.subscribe(updatedlist => {
          this.bookImages = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageError('Could Not Update BookImage')
    )
  }

  // Load the update Modal

  onUpdateModal(avatarEdit: BookImage): void {
    this._id.setValue(avatarEdit.id);
    this._image.setValue(avatarEdit.image);


    this.updateForm.setValue({
      'id': this._id.value,
      'image': this._image.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the avatar
  onDelete(avatar: BookImage): void {
    this.bookImageservice.deleteBookImage(avatar.id).subscribe(result => {
      this.myToastrService.messageSuccess('Book image is deleted successfully');

      this.bookImageservice.clearCache();
      this.bookImages$ = this.bookImageservice.getBookImages();
      this.bookImages$.subscribe(newlist => {
        this.bookImages = newlist;

        this.rerender();
      })
    })
  }

  onSelect(avatar: BookImage): void {
    this.selectedBookImage = avatar;

    this.router.navigateByUrl("/bookImages/" + avatar.id);
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    this.bookImages$ = this.bookImageservice.getBookImages();

    this.bookImages$.subscribe(result => {
      this.bookImages = result;

      this.transform();

      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add avatar properties

    this.image = new FormControl('', [Validators.required]);


    this.insertForm = this.fb.group({

      'image': this.image
    });

    // Initializing Update BookImage properties
    this._image = new FormControl('', [Validators.required]);
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id': this._id,
        'image': this._image

      });


  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
    //console.log("This is render log 1.3  " + this.imageSrc);
    this.hasImage = true;
    this.image.setValue(this.imageSrc);
    this._image.setValue(this.imageSrc)
  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
