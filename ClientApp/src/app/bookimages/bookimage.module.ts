import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookImageRoutingModule } from './bookimage-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {BookImageListComponent} from "./bookimage-list/bookimage-list.component";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {MatOptionModule} from "@angular/material";
import {ToastrModule} from "ngx-toastr";


@NgModule({
  declarations: [
    BookImageListComponent
  ],
  imports: [
    CommonModule,
    BookImageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors
})
export class BookImageModule { }
