import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {BookImageListComponent} from "./bookimage-list/bookimage-list.component";


const routes: Routes = [
  {path: '', component: BookImageListComponent, canActivate: [AuthGuardService]},
  {path: 'bookImage-list', component:BookImageListComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class BookImageRoutingModule { }
