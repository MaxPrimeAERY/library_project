import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  temper: number;

  constructor() { }

  // convertUTCDateToLocalDate(date) : Date{
  //   let newDate = date.toLocaleString();
  //   return newDate;
  // }

  fixDate(d: Date): Date {
    //console.log("Helper = "+d);
    // this.temper = new Date(d).getHours();
    //console.log("Helper = "+this.temper);
    d.setHours( d.getHours() -  d.getTimezoneOffset() / 60);
    return d;
  }
}
