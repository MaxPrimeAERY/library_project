import { TestBed } from '@angular/core/testing';

import { CardOfReaderService } from './card-of-reader.service';

describe('CardOfReaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CardOfReaderService = TestBed.get(CardOfReaderService);
    expect(service).toBeTruthy();
  });
});
