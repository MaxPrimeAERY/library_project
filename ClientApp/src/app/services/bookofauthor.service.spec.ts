import { TestBed } from '@angular/core/testing';

import { BookOfAuthorService } from './bookofauthor.service';

describe('BookOfAuthorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookOfAuthorService = TestBed.get(BookOfAuthorService);
    expect(service).toBeTruthy();
  });
});
