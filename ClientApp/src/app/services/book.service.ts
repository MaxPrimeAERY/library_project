import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Book} from "../interfaces/book";
import {first, flatMap, shareReplay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/book/getbooks";

  private bookUrl : string = "/api/book/addbook";

  private updateUrl: string = "/api/book/updatebook/";

  private deleteUrl: string = "/api/book/deletebook/";

  private book$: Observable<Book[]>;

  getBooks(): Observable<Book[]> {
    if (!this.book$) {
      //shareReplay for caching
      this.book$ = this.http.get<Book[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.book$;

  }

  insertBook(newBook : Book) :  Observable<Book>
  {
    return this.http.post<Book>(this.bookUrl, newBook);
  }

  async getBookById(id: number): Promise<Book> {
    return await this.getBooks().pipe(flatMap(result => result), first(book => book.id == id)).toPromise();
  }

  updateBook(id: number, editBook: Book): Observable<Book> {
    return this.http.put<Book>(this.updateUrl + id, editBook);
  }

  deleteBook(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }


  //clear cache
  clearCache() {
    this.book$ = null;
  }
}
