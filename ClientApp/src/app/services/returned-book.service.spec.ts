import { TestBed } from '@angular/core/testing';

import { ReturnedBookService } from './returned-book.service';

describe('ReturnedBookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReturnedBookService = TestBed.get(ReturnedBookService);
    expect(service).toBeTruthy();
  });
});
