import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {Author} from "../interfaces/author";

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/author/getauthors";

  //private getByIdUrl : string = "/api/author/getauthor/";

  private authorUrl : string = "/api/author/addauthor";

  private updateUrl: string = "/api/author/updateauthor/";

  private deleteUrl: string = "/api/author/deleteauthor/";

  private author$: Observable<Author[]>;

  getAuthors(): Observable<Author[]> {
    if (!this.author$) {
      //shareReplay for caching
      this.author$ = this.http.get<Author[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.author$;

  }

  insertAuthor(newAuthor : Author) :  Observable<Author>
  {
    return this.http.post<Author>(this.authorUrl, newAuthor);
  }

  async getAuthorById(id: number): Promise<Author> {
    return await this.getAuthors().pipe(flatMap(result => result), first(author => author.id == id)).toPromise();
  }

  updateAuthor(id: number, editAuthor: Author): Observable<Author> {
    return this.http.put<Author>(this.updateUrl + id, editAuthor);
  }

  deleteAuthor(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }


  //clear cache
  clearCache() {
    this.author$ = null;
  }
}
