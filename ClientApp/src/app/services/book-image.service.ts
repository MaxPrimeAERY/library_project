import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {BookImage} from "../interfaces/book-image";
import {first, flatMap, shareReplay} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class BookImageService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/bookImage/getbookImages";

  private bookImageUrl : string = "/api/bookImage/addbookImage";

  private updateUrl: string = "/api/bookImage/updatebookImage/";

  private deleteUrl: string = "/api/bookImage/deletebookImage/";

  private bookImage$: Observable<BookImage[]>;

  getBookImages(): Observable<BookImage[]> {
    if (!this.bookImage$) {
      //shareReplay for caching
      this.bookImage$ = this.http.get<BookImage[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.bookImage$;

  }

  insertBookImage(newBookImage : BookImage) :  Observable<BookImage>
  {
    return this.http.post<BookImage>(this.bookImageUrl, newBookImage);
  }

  async getBookImageById(id: number): Promise<BookImage> {
    return await this.getBookImages().pipe(flatMap(result => result), first(bookImage => bookImage.id == id)).toPromise();
  }

  updateBookImage(id: number, editBookImage: BookImage): Observable<BookImage> {
    return this.http.put<BookImage>(this.updateUrl + id, editBookImage);
  }

  deleteBookImage(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }


  //clear cache
  clearCache() {
    this.bookImage$ = null;
  }
}
