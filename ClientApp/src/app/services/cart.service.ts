import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {first, flatMap, map, shareReplay} from "rxjs/operators";
import {Cart} from "../interfaces/cart";

@Injectable({
  providedIn: 'root'
})
export class CartService {


  constructor(private http: HttpClient) { }

  private baseUrl: string = "/api/cart/getcarts";

  private byUserIDUrl: string = "/api/cart/GetCartByUserId/";

  private cartUrl : string = "/api/cart/addcart";

  private updateUrl: string = "/api/cart/updatecart/";

  //private deleteUrl: string = "/api/cart/deletecart/";

  private cart$: Observable<Cart[]>;

  getCarts(): Observable<Cart[]> {
    if (!this.cart$) {
      //shareReplay for caching
      this.cart$ = this.http.get<Cart[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.cart$;

  }

  insertCart(newCart : Cart) :  Observable<Cart>
  {
    return this.http.post<Cart>(this.cartUrl, newCart);
  }

  getCartById(id: number): Observable<Cart> {
    return this.getCarts().pipe(flatMap(result => result), first(cart => cart.id == id));
  }

  getCartByUserId(userId: number): Observable<Cart> {
    return this.http.get<Cart>(this.byUserIDUrl+userId);
  }


  updateCart(id: number, editCart: Cart): Observable<Cart> {
    return this.http.put<Cart>(this.updateUrl + id, editCart);
  }

  //clear cache
  clearCache() {
    this.cart$ = null;
  }

}
