import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CartItems} from "../interfaces/cart-items";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {CartInfo} from "../interfaces/cart-info";

@Injectable({
  providedIn: 'root'
})
export class CartItemsService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/cartItems/getcartItems";

  private getByCartIdUrl: string = "/api/cartItems/GetByCartId/";

  private getCartInfoByUserIdUrl: string = "/api/cartItems/GetCartInfoByUserId/";

  private getByBookOfAuthorIdUrl: string = "/api/cartItems/GetByBookOfAuthorId/";

  private cartItemsUrl: string = "/api/cartItems/addcartItems";

  private updateUrl: string = "/api/cartItems/updatecartItems/";

  private deleteUrl: string = "/api/cartItems/deletecartItems/";

  private deleteByCartIdUrl: string = "/api/cartItems/DeleteByCartId/";

  private cartItems$: Observable<CartItems[]>;
  private boaItems$: Observable<CartInfo[]>;

  getCartItems(): Observable<CartItems[]> {
    if (!this.cartItems$) {
      //shareReplay for caching
      this.cartItems$ = this.http.get<CartItems[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.cartItems$;

  }

  insertCartItems(newCartItems: CartItems): Observable<CartItems> {
    return this.http.post<CartItems>(this.cartItemsUrl, newCartItems);
  }

  getCartItemsById(id: number): Observable<CartItems> {
    return this.getCartItems().pipe(flatMap(result => result), first(cartItems => cartItems.id == id));
  }

  async getByCartId(id: number): Promise<CartItems[]> {
      return await this.http.get<CartItems[]>(this.getByCartIdUrl + id).toPromise();
  }

  async getByBookOfAuthorId(id: number, cartId: number): Promise<CartItems> {
    return await this.http.get<CartItems>(this.getByBookOfAuthorIdUrl + id + '/' + cartId).toPromise();
  }


  getCartInfoByUserId(userId: number): Observable<CartInfo[]> {
    if (!this.boaItems$) {

      this.boaItems$ = this.http.get<CartInfo[]>(this.getCartInfoByUserIdUrl + userId).pipe(shareReplay());
    }
    return this.boaItems$;
  }

  async updateCartItems(id: number, editCartItems: CartItems): Promise<CartItems> {
    return await this.http.put<CartItems>(this.updateUrl + id, editCartItems).toPromise();
  }

  deleteCartItems(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  async deleteByCartId(cartId: number): Promise<any> {
    return await this.http.delete(this.deleteByCartIdUrl + cartId).toPromise();
  }

  //clear cache
  clearCache() {
    this.cartItems$ = null;
  }
}
