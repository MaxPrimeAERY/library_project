import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ReturnedBook} from "../interfaces/returned-book";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {OrderItems} from "../interfaces/order-items";

@Injectable({
  providedIn: 'root'
})
export class ReturnedBookService {

  constructor(private http: HttpClient) { }

  private baseUrl: string = "/api/returnedBook/getreturnedBooks";

  private returnedBookUrl : string = "/api/returnedBook/addreturnedBook";

  private returnedBookModUrl : string = "/api/returnedBook/addreturnedBookMod";

  private updateUrl: string = "/api/returnedBook/updatereturnedBook/";

  private deleteUrl: string = "/api/returnedBook/deletereturnedBook/";

  private returnedBookByUserIdUrl : string = "/api/returnedBook/GetReturnedBookByUserId/";

  private getLastReturnedBookByUserIdUrl : string = "/api/returnedBook/GetLastReturnedBookByUserId/";

  private returnedBook$: Observable<ReturnedBook[]>;

  handleError : any;

  getReturnedBooks(): Observable<ReturnedBook[]> {
    if (!this.returnedBook$) {
      //shareReplay for caching
      this.returnedBook$ = this.http.get<ReturnedBook[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.returnedBook$;

  }

  async insertReturnedBook(newReturnedBook : ReturnedBook) : Promise<ReturnedBook>
  {
    return await this.http.post<ReturnedBook>(this.returnedBookUrl, newReturnedBook).toPromise();

  }

  getReturnedBookById(id: number): Observable<ReturnedBook> {
    return this.getReturnedBooks().pipe(flatMap(result => result), first(returnedBook => returnedBook.id == id));
  }

  getReturnedBookByUserId(userId: number): Observable<ReturnedBook[]> {//list of returnedBooks
    return this.http.get<ReturnedBook[]>(this.returnedBookByUserIdUrl + userId);
  }

  getByBookOfAuthorId(id: number): Observable<ReturnedBook> {
    return this.getReturnedBooks().pipe(flatMap(result => result), first(orderItems => orderItems.boaId == id));
  }

  updateReturnedBook(id: number, editReturnedBook: ReturnedBook): Observable<ReturnedBook> {
    return this.http.put<ReturnedBook>(this.updateUrl + id, editReturnedBook);
  }

  deleteReturnedBook(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  //clear cache
  clearCache() {
    this.returnedBook$ = null;
  }

}
