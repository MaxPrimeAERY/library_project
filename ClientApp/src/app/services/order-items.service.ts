import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {OrderInfo} from "../interfaces/order-info";
import {OrderItems} from "../interfaces/order-items";

@Injectable({
  providedIn: 'root'
})
export class OrderItemsService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/orderItems/getorderItems";

  private getByOrderIdUrl: string = "/api/orderItems/GetByOrderId/";

  private getOrderInfoByUserIdUrl: string = "/api/orderItems/GetOrderInfoByUserId/";

  private getOrderInfoByOrderIdUrl: string = "/api/orderItems/GetOrderInfoByOrderId/";

  private getOrderInfoByUserOrderIdUrl: string = "/api/orderItems/GetOrderInfoByUserOrderId/";

  private getOrderInfoUrl: string = "/api/orderItems/GetOrderInfo/";

  private getByBookOfAuthorIdUrl: string = "/api/orderItems/GetByBookOfAuthorId/";

  private orderItemsUrl: string = "/api/orderItems/addorderItems";

  private updateUrl: string = "/api/orderItems/updateorderItems/";

  private deleteUrl: string = "/api/orderItems/deleteorderItems/";

  private deleteByOrderIdUrl: string = "/api/orderItems/DeleteByOrderId/";

  private orderItems$: Observable<OrderItems[]>;
  private boaItems$: Observable<OrderInfo[]>;

  getOrderItems(): Observable<OrderItems[]> {
    if (!this.orderItems$) {
      //shareReplay for caching
      this.orderItems$ = this.http.get<OrderItems[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.orderItems$;

  }

  async insertOrderItems(newOrderItems: OrderItems): Promise<OrderItems> {
    return await this.http.post<OrderItems>(this.orderItemsUrl, newOrderItems).toPromise();
  }

  async getOrderItemsById(id: number): Promise<OrderItems> {
    return await this.getOrderItems().pipe(flatMap(result => result), first(orderItems => orderItems.id == id)).toPromise();
  }

  getByOrderId(id: number): Observable<OrderItems[]> {
    return this.http.get<OrderItems[]>(this.getByOrderIdUrl + id);
  }

  async getByBookOfAuthorId(id: number): Promise<OrderItems> {
    return await this.http.get<OrderItems>(this.getByBookOfAuthorIdUrl + id).toPromise();
  }


  getOrderInfoByUserId(userId: number): Observable<OrderInfo[]> {
    if (!this.boaItems$) {

      this.boaItems$ = this.http.get<OrderInfo[]>(this.getOrderInfoByUserIdUrl + userId);///////
    }
    return this.boaItems$;
  }

  async getOrderInfoByOrderId(orderId: number): Promise<OrderInfo[]> {

    return await this.http.get<OrderInfo[]>(this.getOrderInfoByOrderIdUrl + orderId).toPromise();//////
  }

  getOrderInfoByUserOrderId(userId: number, orderId: number): Observable<OrderInfo[]> {

    return this.http.get<OrderInfo[]>(this.getOrderInfoByUserOrderIdUrl + userId + '/' + orderId);//////
  }

  getOrderInfo(): Observable<OrderInfo[]> {
    if (!this.boaItems$) {
      //shareReplay for caching
      this.boaItems$ = this.http.get<OrderInfo[]>(this.getOrderInfoUrl).pipe(shareReplay());
    }
    return this.boaItems$;

  }

  updateOrderItems(id: number, editOrderItems: OrderItems): Promise<OrderItems> {
    return this.http.patch<OrderItems>(this.updateUrl + id, editOrderItems).toPromise();
  }

  deleteOrderItems(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  deleteByOrderId(orderId: number): Observable<any> {
    return this.http.delete(this.deleteByOrderIdUrl + orderId);
  }

  //clear cache
  clearCache() {
    this.orderItems$ = null;
  }
}
