import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserRole} from "../interfaces/user-role";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {Role} from "../interfaces/role";

@Injectable({
  providedIn: 'root'
})
export class UserRoleService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/userRole/getuserRoles";

  private byUserIdUrl: string = "/api/userRole/GetUserRolesByUserId/";

  private userRoleUrl: string = "/api/userRole/adduserRole";

  private deleteUrl: string = "/api/userRole/deleteuserRole/";

  private roleUrl: string = "/api/userRole/getRoles";

  private userRole$: Promise<UserRole[]>;
  private role$: Observable<Role[]>;

  async getUserRoles(): Promise<UserRole[]> {
    if (!this.userRole$) {
      //shareReplay for caching
      this.userRole$ =  this.http.get<UserRole[]>(this.baseUrl).pipe(shareReplay()).toPromise();
    }
    return await this.userRole$;
  }

  async getUserRolesByUserId(userId: number): Promise<UserRole[]>{
    return await this.http.get<UserRole[]>(this.byUserIdUrl + userId).toPromise();
  }

  getRoles(): Observable<Role[]> {
    if (!this.role$) {
      //shareReplay for caching
      this.role$ = this.http.get<Role[]>(this.roleUrl).pipe(shareReplay());
    }
    return this.role$;

  }

  async getRoleById(id: number): Promise<Role> {
    return await this.getRoles().pipe(flatMap(result => result), first(role => role.id == id)).toPromise();
  }

  insertUserRole(newUserRole: UserRole): Observable<UserRole> {
    return this.http.post<UserRole>(this.userRoleUrl, newUserRole);
  }

  deleteUserRole(userId: number, roleId: number): Observable<any> {
    return this.http.delete(this.deleteUrl + userId + '/' + roleId);
  }

  //clear cache
  clearCache() {
    this.userRole$ = null;
  }
}
