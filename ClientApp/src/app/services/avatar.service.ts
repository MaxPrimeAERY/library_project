import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Avatar} from "../interfaces/avatar";
import {first, flatMap, shareReplay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AvatarService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/avatar/getavatars";

  private avatarUrl : string = "/api/avatar/addavatar";

  private avatarByUserIdUrl : string = "/api/avatar/GetByAppUserId/";

  private updateUrl: string = "/api/avatar/updateavatar/";

  private deleteUrl: string = "/api/avatar/deleteavatar/";

  private avatar$: Promise<Avatar[]>;

  async getAvatars(): Promise<Avatar[]> {
    if (!this.avatar$) {
      //shareReplay for caching
      this.avatar$ = this.http.get<Avatar[]>(this.baseUrl).pipe(shareReplay()).toPromise();
    }
    return await this.avatar$;

  }

  insertAvatar(newAvatar : Avatar) :  Observable<Avatar>
  {
    return this.http.post<Avatar>(this.avatarUrl, newAvatar);
  }

  // getAvatarById(id: number): Observable<Avatar> {
  //   return this.getAvatars().pipe(flatMap(result => result), first(avatar => avatar.id == id));
  // }

  getAvatarByUserId(userId: number): Observable<Avatar> {
    return this.http.get<Avatar>(this.avatarByUserIdUrl+userId);
  }
  updateAvatar(id: number, editAvatar: Avatar): Observable<Avatar> {
    return this.http.put<Avatar>(this.updateUrl + id, editAvatar);
  }

  deleteAvatar(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }


  //clear cache
  clearCache() {
    this.avatar$ = null;
  }
}
