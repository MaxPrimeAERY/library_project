import {Injectable, Input} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import * as jwt_decode from "jwt-decode";
import {MyProfile} from "../interfaces/my-profile";
import {MyToastrService} from "./my-toastr.service";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient,
              private router: Router,
              private myToastrService: MyToastrService) {
  }

  private baseUrl: string = window.location.origin;
  private baseUrlLogin: string = "/account/login";
  private baseUrlRegister: string = "/account/register";
  private baseUrlForgotPassword: string = "/account/forgotpassword";

  private baseUrlResetPasswordForComponent: string = this.baseUrl + "/resetpassword/";
  private baseUrlResetPasswordForAPI: string = "/account/resetpassword/";

  private baseUrlConfirmPassForComponent: string = this.baseUrl + "/confirmemail/";
  private baseUrlConfirmPassForApi: string = "/account/confirmemail";

  private loginStatus = new BehaviorSubject<boolean>(this.checkLogInStatus());
  private UserId = new BehaviorSubject<string>("");
  private UserName = new BehaviorSubject<string>("");

  private tokenInfo;

  private UserRole = new BehaviorSubject<string>("");
  @Input() myProfile: MyProfile;

  // Register Method
  register(firstname: string, lastname: string, username: string, password: string, email: string, allowcomments: boolean) {
    let headers = new HttpHeaders({
      baseUrlConfirmPassForComponent: this.baseUrlConfirmPassForComponent
    });
    let options = {headers: headers};

    return this.http.post<any>(this.baseUrlRegister, {
      firstname, lastname,
      username, password, email, allowcomments
    }, options).pipe(map(result => {
      //registration was successful
      return result;

    }, error => {
      return error;
    }));
  }

  login(username: string, password: string, rememberMe: boolean) {

    //pipe helps combine multiple functions into a single function
    //functions runs one by one
    return this.http.post<any>(this.baseUrlLogin, {username, password, rememberMe}).pipe(
      map(res => {
        if (res && res.token) {
          //store user details and jwt token in local storage to keep user logged in
          this.loginStatus.next(true);
          localStorage.setItem('loginStatus', '1');
          localStorage.setItem('jwt', res.token);

          this.tokenInfo = this.getDecodedAccessToken(localStorage.getItem('jwt'));
          let tempRole = this.tokenInfo.role;
          let tempUserId = this.tokenInfo.nameid;
          let tempUserName = this.tokenInfo.unique_name;

          localStorage.setItem('expiration', res.expiration);

          //localStorage.setItem('username', res.username);
          //localStorage.setItem('userId', res.userId);
          //localStorage.setItem('userRole', res.userRole);

          this.UserName.next(tempUserName);
          this.UserRole.next(tempRole);
        }
        //console.log(res);
        return res;
      })
    );
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  forgotPassword(email: string) {
    let headers = new HttpHeaders({
      baseUrlResetPasswordForComponent: this.baseUrlResetPasswordForComponent
    });
    let options = {headers: headers};
    return this.http.post<any>(this.baseUrlForgotPassword, {email}, options);
  }

  resetPassword(model: any) {
    return this.http.post<any>(this.baseUrlResetPasswordForAPI, model);
  }

  confirmEmail(model: any) {
    return this.http.post(this.baseUrlConfirmPassForApi, model);
  }

  logout() {
    //delete saved jwt info
    this.loginStatus.next(false);
    localStorage.removeItem('jwt');
    localStorage.removeItem('id');
    //localStorage.removeItem('username');
    //localStorage.removeItem('userId');
    localStorage.removeItem('expiration');
    //localStorage.removeItem('userRole');
    localStorage.removeItem('cartId');
    localStorage.setItem('loginStatus', '0');
    //this.router.navigate(['/bookofauthor']);
    //console.log("Logged out Successfully");

  }

  checkLogInStatus(): boolean {

    var loginCookie = localStorage.getItem("loginStatus");

    if (loginCookie == "1") {

      if (localStorage.getItem('jwt') === null || localStorage.getItem('jwt') === undefined) {
        return false;
      }

      const token = localStorage.getItem('jwt');
      const decoded = jwt_decode(token); //for checking token if expired

      if (decoded.exp === undefined) {
        return false;
      }

      const date = new Date(0); //date and time from 1970
      let tokenExpDate = date.setUTCSeconds(decoded.exp);//convert time to UTC

      if (tokenExpDate.valueOf() > new Date().valueOf()) {
        return true;
      }

      //console.log("New Date " + new Date().valueOf());
      //console.log("Token Date " + tokenExpDate.valueOf());

    }
    return false;
  }

  get isLoggedIn() {
    return this.loginStatus.asObservable();
  }

  get currentUserId(){
    this.tokenInfo = this.getDecodedAccessToken(localStorage.getItem('jwt'));
    if (this.tokenInfo == null){
      //this.myToastrService.messageWarning("Login or Register to continue");
      //return;
    } else {
      let tempUserId = (this.tokenInfo.nameid);
      this.UserId.next(tempUserId);
    }
    return this.UserId.asObservable();
  }

  get currentUserName() {
    this.tokenInfo = this.getDecodedAccessToken(localStorage.getItem('jwt'));
    if (this.tokenInfo == null){
      //this.myToastrService.messageWarning("Login or Register to continue");
      //return;
    } else {
      let tempUserName = (this.tokenInfo.unique_name);
      this.UserName.next(tempUserName);
    }
    return this.UserName.asObservable();
  }

  get currentUserRole() {
    this.tokenInfo = this.getDecodedAccessToken(localStorage.getItem('jwt'));
    if (this.tokenInfo == null){
      this.myToastrService.messageWarning("Login or Register to continue");
      //return;
    } else {
      let tempRole = (this.tokenInfo.role);
      this.UserRole.next(tempRole);
    }
    return this.UserRole.asObservable();
  }


}
