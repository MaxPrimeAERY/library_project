import { Injectable } from '@angular/core';
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class MyToastrService {

  constructor(private toastrService: ToastrService) {  }

  messageError(message: string){
    this.toastrService.error(
      message,
      "Error!",
      {
        positionClass: 'toast-top-center',
        closeButton: true,
        timeOut: 5000,
        onActivateTick: true,
        progressBar: true
      }
    )
  }

  messageSuccess(message: string){
    this.toastrService.success(
      message,
      "Success!",
      {
        positionClass: 'toast-top-center',
        closeButton: true,
        timeOut: 5000,
        onActivateTick: true,
        progressBar: true
      }
    )
  }

  messageWarning(message: string){
    this.toastrService.warning(
      message,
      "Warning!",
      {
        positionClass: 'toast-top-center',
        closeButton: true,
        timeOut: 5000,
        onActivateTick: true,
        progressBar: true
      }
    )
  }

  messageInfo(message: string){
    this.toastrService.info(
      message,
      "Info!",
      {
        positionClass: 'toast-top-center',
        closeButton: true,
        timeOut: 5000,
        onActivateTick: true,
        progressBar: true
      }
    )
  }


}
