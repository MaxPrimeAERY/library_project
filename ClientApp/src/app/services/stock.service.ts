import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Stock} from "../interfaces/stock";
import {first, flatMap, shareReplay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/stock/getstocks";

  private byBookIdUrl: string = "/api/stock/GetStockByBookOfAuthorId/";

  private stockUrl : string = "/api/stock/addstock";

  private updateUrl: string = "/api/stock/updatestock/";

  private patchUrl: string = "/api/stock/patchstock/";

  private deleteUrl: string = "/api/stock/deletestock/";

  private stock$: Observable<Stock[]>;

  getStocks(): Observable<Stock[]> {
    if (!this.stock$) {
      //shareReplay for caching
      this.stock$ = this.http.get<Stock[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.stock$;

  }

  insertStock(newStock : Stock) :  Observable<Stock>
  {
    return this.http.post<Stock>(this.stockUrl, newStock);
  }

  getStockById(id: number): Observable<Stock> {
    return this.getStocks().pipe(flatMap(result => result), first(stock => stock.id == id));
  }

  async getStockByBookOfAuthorId(id: number): Promise<Stock> {
    return await this.http.get<Stock>(this.byBookIdUrl + id).toPromise();
  }

  async updateStock(id: number, editStock: Stock): Promise<Stock> {
    return await this.http.put<Stock>(this.updateUrl + id, editStock).toPromise();
  }

  async patchStock(id: number, editStock: Stock): Promise<Stock> {
    return await this.http.patch<Stock>(this.patchUrl + id, editStock).toPromise();
  }

  deleteStock(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }


  //clear cache
  clearCache() {
    this.stock$ = null;
  }
}
