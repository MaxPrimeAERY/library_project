import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {BookOfAuthor} from "../interfaces/bookofauthor";
import {BookOfAuthorIds} from "../interfaces/bookofauthorids";

@Injectable({
  providedIn: 'root'
})
export class BookOfAuthorService {

  setGroupFilter$ = new Subject<any>();
  getGroupFilter = this.setGroupFilter$.asObservable();

  constructor(private http: HttpClient) {
  }

  private authorBookUrl: string = "/api/bookofauthor/getauthorbook";

  private authorBookUrlForCart: string = "/api/bookofauthor/GetAuthorBookForCart/";

  private authorbook$: Observable<BookOfAuthor[]>;

  private genreUrl: string = "/api/bookofauthor/getgenre";


  private baseUrl: string = "/api/bookofauthor/getbookofauthors";
  private baseByIdUrl: string = "/api/bookofauthor/GetBookOfAuthorById/";
  private bookOfAuthorUrl : string = "/api/bookOfAuthor/addbookOfAuthor";
  private updateUrl: string = "/api/bookOfAuthor/updatebookOfAuthor/";
  private deleteUrl: string = "/api/bookOfAuthor/deletebookOfAuthor/";

  private bookOfAuthor$: Promise<BookOfAuthorIds[]>;

  getAuthorBooks(): Observable<BookOfAuthor[]> {
    if (!this.authorbook$) {
      //shareReplay for caching
      this.authorbook$ = this.http.get<BookOfAuthor[]>(this.authorBookUrl).pipe(shareReplay());
    }
    return this.authorbook$;
  }
  async getAuthorBookById(id: number): Promise<BookOfAuthor> {
    return await this.getAuthorBooks().pipe(flatMap(result => result), first(authorBook => authorBook.id == id)).toPromise();
  }

  getAuthorBookForCart(id: number): Observable<BookOfAuthor[]> {
    if (!this.authorbook$) {
      //shareReplay for caching
      this.authorbook$ = this.http.get<BookOfAuthor[]>(this.authorBookUrlForCart+id).pipe(shareReplay());
    }
    return this.authorbook$;
  }

  //----------------------------------------------------------------------------------------------------------

  async getBookOfAuthorIds(): Promise<BookOfAuthorIds[]> {
    if (!this.bookOfAuthor$) {
      //shareReplay for caching
      this.bookOfAuthor$ = this.http.get<BookOfAuthorIds[]>(this.baseUrl).pipe(shareReplay()).toPromise();
    }
    return await this.bookOfAuthor$;

  }

  insertBookOfAuthorIds(newBookOfAuthor : BookOfAuthorIds) :  Observable<BookOfAuthorIds>
  {
    return this.http.post<BookOfAuthorIds>(this.bookOfAuthorUrl, newBookOfAuthor);
  }

  async getBookOfAuthorById(id: number): Promise<BookOfAuthorIds> {
    return await this.http.get<BookOfAuthorIds>(this.baseByIdUrl + id).toPromise();
  }

  updateBookOfAuthorIds(id: number, editBookOfAuthor: BookOfAuthorIds): Observable<BookOfAuthorIds> {
    return this.http.put<BookOfAuthorIds>(this.updateUrl + id, editBookOfAuthor);
  }

  deleteBookOfAuthorIds(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }


  //clear cache
  clearCache() {
    this.bookOfAuthor$ = null;
    this.authorbook$ = null;
  }
}
