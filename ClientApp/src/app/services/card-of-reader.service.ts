import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {shareReplay} from "rxjs/operators";
import {CardOfReader} from "../interfaces/card-of-reader";

@Injectable({
  providedIn: 'root'
})
export class CardOfReaderService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/cardOfReader/getcardOfReaders";

  private baseByUserIdUrl: string = "/api/cardOfReader/GetCardOfReaderByUserId/";


  private cardOfReader$: Observable<CardOfReader[]>;
  private cardOfReaderById$: Observable<CardOfReader[]>;

  getCardOfReaders(): Observable<CardOfReader[]> {
    if (!this.cardOfReader$) {
      //shareReplay for caching
      this.cardOfReader$ = this.http.get<CardOfReader[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.cardOfReader$;
  }

  getCardOfReaderByUserId(userId: number): Observable<CardOfReader[]> {
    if (!this.cardOfReaderById$) {
      this.cardOfReaderById$ =  this.http.get<CardOfReader[]>(this.baseByUserIdUrl + userId).pipe(shareReplay());
    }
    return this.cardOfReaderById$;
  }

  //clear cache
  clearCache() {
    this.cardOfReader$ = null;
  }
}
