import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {Comments} from "../interfaces/comments";

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) { }

  private baseUrl: string = "/api/comments/getcomments";

  private commentUrl : string = "/api/comments/addcomment";

  private commentModUrl : string = "/api/comments/addcommentMod";

  private updateUrl: string = "/api/comments/updatecomment/";

  private deleteUrl: string = "/api/comments/deletecomment/";

  private patchMyUrl: string = "/api/comments/PatchMyComment/";

  private deleteMyUrl: string = "/api/comments/DeleteMyComment/";

  private commentByUserIdUrl : string = "/api/comments/GetCommentByUserId/";

  private commentByBoaIdUrl : string = "/api/comments/GetByBookOfAuthorId/";

  private comment$: Observable<Comments[]>;

  handleError : any;

  getComments(): Observable<Comments[]> {
    if (!this.comment$) {
      //shareReplay for caching
      this.comment$ = this.http.get<Comments[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.comment$;

  }

  async insertComment(newComment : Comments) : Promise<Comments>
  {
    return await this.http.post<Comments>(this.commentUrl, newComment).toPromise();
  }

  getCommentById(id: number): Observable<Comments> {
    return this.getComments().pipe(flatMap(result => result), first(comment => comment.id == id));
  }

  getCommentByUserId(userId: number): Observable<Comments[]> {
    return this.http.get<Comments[]>(this.commentByUserIdUrl + userId);
  }

  getByBookOfAuthorId(id: number): Observable<Comments[]> {
    return this.http.get<Comments[]>(this.commentByBoaIdUrl + id);;
  }

  updateComment(id: number, editComment: Comments): Observable<Comments> {
    return this.http.put<Comments>(this.updateUrl + id, editComment);
  }

  deleteComment(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  async patchMyComment(id: number, userId: number, editComment: Comments) : Promise<Comments>
  {
    return await this.http.patch<Comments>(this.patchMyUrl + id + '/' + userId, editComment).toPromise();
  }

  async deleteMyComment(id: number, userId: number) : Promise<Comments>
  {
    return await this.http.delete<Comments>(this.deleteMyUrl + id + '/' + userId).toPromise();
  }

  //clear cache
  clearCache() {
    this.comment$ = null;
  }

}
