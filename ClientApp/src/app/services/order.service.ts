import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {catchError, first, flatMap, map, shareReplay, tap} from "rxjs/operators";
import {Order} from "../interfaces/order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  private baseUrl: string = "/api/order/getorders";

  private orderUrl : string = "/api/order/addorder";

  private orderModUrl : string = "/api/order/addorderMod";

  private updateUrl: string = "/api/order/updateorder/";

  private deleteUrl: string = "/api/order/deleteorder/";

  private orderByUserIdUrl : string = "/api/order/GetOrderByUserId/";

  private getLastOrderByUserIdUrl : string = "/api/order/GetLastOrderByUserId/";

  private order$: Observable<Order[]>;

  handleError : any;

  getOrders(): Observable<Order[]> {
    if (!this.order$) {
      //shareReplay for caching
      this.order$ = this.http.get<Order[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.order$;

  }

   async insertOrder(newOrder : Order) : Promise<Order>
  {
    return await this.http.post<Order>(this.orderUrl, newOrder).toPromise();

  }

  async insertOrderMod(newOrder : Order) : Promise<Order>
  {
    const resp = await this.http.post<Order>(this.orderModUrl, newOrder).toPromise();
    return resp;
  }

  async getOrderById(id: number): Promise<Order> {
    return this.getOrders().pipe(flatMap(result => result), first(order => order.id == id)).toPromise();
  }

  getOrderByUserId(userId: number): Observable<Order[]> {//list of orders
    return this.http.get<Order[]>(this.orderByUserIdUrl + userId);
  }

  async getLastOrderByUserId(userId: number): Promise<Order> {
    return await this.http.get<Order>(this.getLastOrderByUserIdUrl + userId).toPromise();
  }


  async updateOrder(id: number, editOrder: Order): Promise<Order> {
    return await this.http.put<Order>(this.updateUrl + id, editOrder).toPromise();
  }

  deleteOrder(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  //clear cache
  clearCache() {
    this.order$ = null;
  }

}
