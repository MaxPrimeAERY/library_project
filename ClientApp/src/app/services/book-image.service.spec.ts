import { TestBed } from '@angular/core/testing';

import { BookImageService } from './book-image.service';

describe('BookImageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookImageService = TestBed.get(BookImageService);
    expect(service).toBeTruthy();
  });
});
