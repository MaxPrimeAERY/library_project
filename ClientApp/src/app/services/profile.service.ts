import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Profile} from "../interfaces/profile";
import {first, flatMap, shareReplay} from "rxjs/operators";
import {MyProfile} from "../interfaces/my-profile";
import {SimpleProfile} from "../interfaces/simple-profile";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) {
  }

  private baseUrl: string = "/api/profile/getprofiles";

  private byIdUrl: string = "/api/profile/GetProfileById/";

  private updateUrl: string = "/api/profile/updateprofile/";

  private patchUrl: string = "/api/profile/patchprofile/";

  private deleteUrl: string = "/api/profile/deleteprofile/";

  private myProfileUrl: string = "/api/profile/myProfile/";

  private simpleProfileUrl: string = "/api/profile/simpleProfile/";

  private profile$: Observable<Profile[]>;
  private myProfile$: Observable<MyProfile[]>;

  getProfiles(): Observable<Profile[]> {
    if (!this.profile$) {
      //shareReplay for caching
      this.profile$ = this.http.get<Profile[]>(this.baseUrl).pipe(shareReplay());
    }
    return this.profile$;

  }

  getProfilesImage(): Observable<MyProfile[]> {
    if (!this.myProfile$) {
      //shareReplay for caching
      this.myProfile$ = this.http.get<MyProfile[]>(this.myProfileUrl).pipe(shareReplay());
    }
    return this.myProfile$;

  }

  async getMyProfile(id: number): Promise<any> {
    return await this.http.get<MyProfile>(this.myProfileUrl + id).toPromise();
  }

  async getSimpleProfile(id: number): Promise<any> {
    return await this.http.get<SimpleProfile>(this.simpleProfileUrl + id).toPromise();
  }

  async getProfileById(id: number): Promise<Profile> {
    return await this.http.get<Profile>(this.byIdUrl + id).toPromise();
  }

  updateProfile(id: number, editProfile: Profile): Observable<Profile> {
    return this.http.put<Profile>(this.updateUrl + id, editProfile);
  }

  patchProfile(id: number, editProfile: Profile): Observable<Profile> {
    return this.http.patch<Profile>(this.patchUrl + id, editProfile);
  }

  deleteProfile(id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  async getProfileByUserName(userName: string): Promise<Profile> {
    return await this.getProfiles().pipe(flatMap(result => result), first(profile => profile.userName == userName)).toPromise();
  }


  //clear cache
  clearCache() {
    this.profile$ = null;
  }
}
