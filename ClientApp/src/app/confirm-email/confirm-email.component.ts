import { Component, OnInit } from '@angular/core';
import {AccountService} from "../services/account.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {CartService} from "../services/cart.service";
import {MyToastrService} from "../services/my-toastr.service";

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  emailConfirmed: boolean = false;
  urlParams: any = {};
  currentUserId : number;

  insertFormCart: FormGroup;
  appUserId: FormControl;
  cartItems: FormControl;
  quantity : FormControl;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private accS: AccountService,
              private formBuilder: FormBuilder,
              private cartService: CartService,
              private myToastrService: MyToastrService
              ) { }

  ngOnInit() {

    if (this.accS.checkLogInStatus() == true){
      this.router.navigate(['/']);
    }

    this.urlParams.token = this.route.snapshot.queryParamMap.get('token');
    this.urlParams.userid = this.route.snapshot.queryParamMap.get('userid');
    this.currentUserId = this.urlParams.userid;
    //console.log("Current user id = "+this.currentUserId);
    localStorage.setItem('userId1', this.currentUserId.toString());

    this.appUserId = new FormControl(this.currentUserId);
    this.cartItems = new FormControl(null);
    this.quantity = new FormControl(null);

    this.insertFormCart = this.formBuilder.group({
      'appUserId': this.appUserId,
      'cartItems': this.cartItems,
      'quantity' : this.quantity
    });
    let cartDetails = this.insertFormCart.value;

    this.cartService.insertCart(cartDetails).subscribe();
    localStorage.removeItem('userId1');

    this.confirmEmail();
  }

  confirmEmail() {
    this.accS.confirmEmail(this.urlParams).subscribe(() => {
      //console.log("success");
      this.myToastrService.messageSuccess("Email Confirmed successfully");
      this.emailConfirmed = true;
    }, error => {
      //console.log(error);
      this.myToastrService.messageError("Some unnecessary error");
      this.emailConfirmed = false;
    })
  }

}
