import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Stock} from "../../interfaces/stock";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {StockService} from "../../services/stock.service";
import {BookOfAuthorService} from "../../services/bookofauthor.service";
import {BookOfAuthor} from "../../interfaces/bookofauthor";
import {ToastrService} from "ngx-toastr";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.scss']
})
export class StockListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  boaId: FormControl;
  totalAmount: FormControl;
  amount: FormControl;

  updateForm: FormGroup;
  _boaId: FormControl;
  _totalAmount: FormControl;
  _amount: FormControl;
  _id: FormControl;

  bookname: string[] =[];
  authorname: string[] =[];
  bookOfAuthor: BookOfAuthor[] =[];

  // Add Modal
  @ViewChild('template', { static: true }) modal : TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', { static: true }) editmodal : TemplateRef<any>;


  // Modal properties
  modalMessage : string;
  modalRef : BsModalRef;
  selectedStock : Stock;
  stock$ : Observable<Stock[]>;
  stock : Stock[] = [];
  userRoleStatus : string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;



  constructor(private stockService : StockService,
              private bookOfAuthorService : BookOfAuthorService,
              private toastrService: ToastrService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef : ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) { }

  /// Load Add New stock Modal
  onAddStock()
  {
    this.modalRef = this.modalService.show(this.modal);
  }

  // Method to Add new Stock
  onSubmit()
  {
    let newStock = this.insertForm.value;

    this.stockService.insertStock(newStock).subscribe(
      result =>
      {
        this.stockService.clearCache();
        this.stock$ = this.stockService.getStocks();

        this.stock$.subscribe(newlist => {
          this.stock = newlist;
          this.modalRef.hide();
          this.insertForm.reset();
          this.rerender();

        });
       //console.log("New Stock added");
        this.myToastrService.messageSuccess("New Stock added");
        window.location.reload();
      },
      error => this.myToastrService.messageError("You can't add the same book to the stock")
    )

  }

  // We will use this method to destroy old table and re-render new table

  rerender()
  {
    this.dtElement.dtInstance.then((dtInstance : DataTables.Api) =>
    {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing Stock
  onUpdate()
  {
    let editStock = this.updateForm.value;
    this.stockService.updateStock(editStock.id, editStock).then(
      result =>
      {
        this.myToastrService.messageSuccess('Stock Updated');
        this.stockService.clearCache();
        this.stock$ = this.stockService.getStocks();
        this.stock$.subscribe(updatedlist =>
        {
          this.stock = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageError("You can't add the same book to the stock")
    )

  }

  // Load the update Modal

  onUpdateModal(stockEdit: Stock) : void
  {
    this._id.setValue(stockEdit.id);
    this._boaId.setValue(stockEdit.boaId);
    this._totalAmount.setValue(stockEdit.totalAmount);
    this._amount.setValue(stockEdit.amount);

    this.updateForm.setValue({
      'id' : this._id.value,
      'boaId' : this._boaId.value,
      'totalAmount' : this._totalAmount.value,
      'amount' : this._amount.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the stock
  onDelete(stock : Stock) : void
  {
    this.stockService.deleteStock(stock.id).subscribe(result =>
    {
      this.stockService.clearCache();
      this.stock$ = this.stockService.getStocks();
      this.stock$.subscribe(newlist =>
      {
        this.stock = newlist;

        this.rerender();
      })
    })
  }

  onSelect(stock: Stock) : void
  {
    this.selectedStock = stock;

    this.router.navigateByUrl("/stock/" + stock.id);
  }

  rangeValidator(totalAmountControl: AbstractControl): ValidatorFn {
    //key contains string with error, boolean = true if we have an error, null if NO error
    return (amountControl: AbstractControl):
      { [key: string]: boolean } | null => {
      //null if haven't initialized yet
      if (!totalAmountControl && !amountControl) {
        return null;
      }
      //null if another validator has already found an error on the MatchingControl
      if (amountControl.hasError && !totalAmountControl.hasError) {
        return null;
      }
      //if validation fails
      if (totalAmountControl.value < amountControl.value) {
        return {'rangeValidator': true};
      } else {
        return null;
      }

    }
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    this.stock$ = this.stockService.getStocks();


    this.stock$.subscribe(async result => {
      this.stock = result;
      for (let i = 0; i < this.stock.length; i++) {
        await this.bookOfAuthorService.getAuthorBookById(this.stock[i].boaId).then(res => {
            this.authorname.push(res.name);
            this.bookname.push(res.bookname);

          }
        );
      }
      //console.log(this.stock);
      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {this.userRoleStatus = result});


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add stock properties

    //let validateImageUrl: string = '^(https?:\/\/.*\.(?:png|jpg))$';

    this.totalAmount = new FormControl('', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]);
    this.amount = new FormControl('', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$"), this.rangeValidator(this.totalAmount)]);
    this.boaId = new FormControl('', [Validators.required, Validators.min(1)]);

    //this.imageUrl = new FormControl('', [Validators.pattern(validateImageUrl)]);

    this.insertForm = this.fb.group({

      'boaId': this.boaId,
      'totalAmount': this.totalAmount,
      'amount' : this.amount

    });

    // Initializing Update Stock properties
    this._totalAmount = new FormControl('', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]);
    this._amount = new FormControl('', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$"), this.rangeValidator(this._totalAmount)]);
    this._boaId = new FormControl('', [Validators.required, Validators.min(1)]);
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id' : this._id,
        'boaId': this._boaId,
        'totalAmount': this._totalAmount,
        'amount' : this._amount

      });

    this.bookOfAuthorService.getAuthorBooks().subscribe(result =>{
      this.bookOfAuthor = result;
    });

  }

  ngOnDestroy()
  {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
