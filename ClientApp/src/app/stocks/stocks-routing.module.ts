import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {StockListComponent} from "./stock-list/stock-list.component";


const routes: Routes = [
  {path: '', component: StockListComponent, canActivate: [AuthGuardService]},
  {path: 'stock-list', component:StockListComponent, canActivate: [AuthGuardService]}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class StocksRoutingModule { }
