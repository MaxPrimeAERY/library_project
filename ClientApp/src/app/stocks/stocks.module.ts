import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {StockListComponent} from "./stock-list/stock-list.component";
import {StocksRoutingModule} from "./stocks-routing.module";
import {MatOptionModule, MatSelectModule} from "@angular/material";
import {ToastrModule} from "ngx-toastr";


@NgModule({
  declarations: [
    StockListComponent
  ],
  imports: [
    CommonModule,
    StocksRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    MatSelectModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors
})
export class StocksModule { }
