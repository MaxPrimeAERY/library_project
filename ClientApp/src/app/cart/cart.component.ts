import {
  AfterViewInit, ApplicationRef,
  ChangeDetectorRef,
  Component, ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {CartService} from "../services/cart.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {BookOfAuthorService} from "../services/bookofauthor.service";
import {AsyncSubject, EMPTY, forkJoin, Observable, of, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {OrderService} from "../services/order.service";
import {CartItemsService} from "../services/cart-items.service";
import {OrderItemsService} from "../services/order-items.service";
import {Book} from "../interfaces/book";
import {CartItems} from "../interfaces/cart-items";
import {CartInfo} from "../interfaces/cart-info";
import {catchError, concatMap, delay, map, repeat, shareReplay, take, takeWhile} from "rxjs/operators";
import {Order} from "../interfaces/order";
import {StockService} from "../services/stock.service";
import {HelperService} from "../services/helper.service";
import {AccountService} from "../services/account.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {


  cartBookName: string;

  userIdAsNumber: number;

  boaIdValue: number;
  quantityValue: number;
  cartId: number;
  cartItems: CartItems[] = [];
  cartInfo: CartInfo[] = [];
  cartInfo$: Observable<CartInfo[]>;

  cartItems$: Observable<CartItems[]>;


  updateFormCart: FormGroup;

  quantity: FormControl;

  quantityAsNumber: number;
  myIndex: number;

  /////////////////////////

  orderIdValue: number;

  appUserId: FormControl;
  orderTime: FormControl;
  orderStatus: FormControl;
  insertFormOrder: FormGroup;


  orderId: FormControl;
  bookOfAuthorId: FormControl;
  quantityOrderItemsForm: FormControl;
  getTimeOrderItemsForm: FormControl;
  returnTimeOrderItemsForm: FormControl;
  insertFormOrderItems: FormGroup;

  myBool: boolean = true;
  resCart: any;

  orders$: Observable<Order[]>;
  orders: Order[] = [];

  @ViewChild('quantityHTML', {static: false}) quantityHTML: ElementRef;

  /////////////////////////////////////

  stockId: number;
  amountBook: number;
  amountForm: FormControl;
  idStockForm: FormControl;
  updateStockForm: FormGroup;

  constructor(
    private accs: AccountService,
    private cartService: CartService,
    private cartItemsService: CartItemsService,
    private chRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private formBuilderOrder: FormBuilder,
    private formBuilderOrderItems: FormBuilder,
    private formBuilderStock: FormBuilder,
    private bookOfAuthorService: BookOfAuthorService,
    private orderService: OrderService,
    private orderItemsService: OrderItemsService,
    private stockService: StockService,
    private helperService: HelperService
  ) {

  }

  async ngOnInit() {

    await this.reloadCartComponent();

    // await this.accs.currentUserId.subscribe(result => {
    //   this.userIdAsNumber = +result;
    // });
    //
    // //this.userIdAsNumber = +localStorage.getItem('userId');
    // await this.cartService.getCartByUserId(this.userIdAsNumber).subscribe(result => {
    //   localStorage.setItem('cartId', result.id.toString());
    // });
    // this.cartId = +localStorage.getItem('cartId');
    //
    // this.cartInfo$ = await this.cartItemsService.getCartInfoByUserId(this.userIdAsNumber);
    // this.cartInfo$.subscribe(result => {
    //   this.cartInfo = result;
    //   //console.log("Cart info =" + this.cartInfo);
    // });
    //
    // await this.cartItemsService.getByCartId(this.cartId).then(result => {
    //   this.resCart = result;
    // });


  }

  async reloadCartComponent() {

    await this.accs.currentUserId.subscribe(result => {
      this.userIdAsNumber = +result;
    });

    await this.cartService.getCartByUserId(this.userIdAsNumber).subscribe(result => {
      localStorage.setItem('cartId', result.id.toString());
    });
    this.cartId = +localStorage.getItem('cartId');

    this.cartInfo$ = await this.cartItemsService.getCartInfoByUserId(this.userIdAsNumber);
    this.cartInfo$.subscribe(result => {
      this.cartInfo = result;
      //console.log("Cart info =" + this.cartInfo);
    });

    await this.cartItemsService.getByCartId(this.cartId).then(result => {
      this.resCart = result;
    });
  }


  async applyNewQuantity(itemId: number, id: number) {
    //console.log("New value");
    const index: number = id;
    //console.log("Index =" + index);
    var quantityInputValue = (<HTMLInputElement>document.getElementById("quantity-" + index)).value;
    this.quantityAsNumber = +quantityInputValue;


    this.quantity = new FormControl(this.quantityAsNumber);

    this.updateFormCart = this.formBuilder.group({
      'quantity': this.quantity
    });
    let cartDetails = this.updateFormCart.value;

    await this.cartItemsService.updateCartItems(itemId, cartDetails).then();

    await this.cartItemsService.getByCartId(this.cartId).then(result => {
      this.resCart = result;
    });
  }


  onDelete(id: any, index: number) {

    this.cartItemsService.deleteCartItems(id).subscribe(result => {

      this.cartInfo$ = this.cartItemsService.getCartInfoByUserId(this.userIdAsNumber);
      this.cartInfo$.subscribe(newList => {
        this.cartInfo = newList;
        this.cartInfo.splice(index, 1);
        //console.log("Render 1");

      });
    });
  }


  async clearCart() {
    var index;
    await this.cartItemsService.deleteByCartId(this.cartId).then();
    this.cartInfo$ = this.cartItemsService.getCartInfoByUserId(this.userIdAsNumber);
    this.cartInfo$.subscribe(newList => {
      this.cartInfo = newList;
      this.cartInfo.splice(index, this.cartInfo.length);
      //console.log("Render 3");

    });
    if (this.updateFormCart !== undefined) {
      this.updateFormCart.reset();
    }
    window.location.reload();
  }

  async checkout() {

    let date = new Date();//current date and time
    //console.log("Curr date = " + date);

    //console.log("Curr date = " + date.toLocaleString());

    this.appUserId = new FormControl(this.userIdAsNumber);
    this.orderTime = new FormControl(this.helperService.fixDate(date));
    this.orderStatus = new FormControl('New order');
    this.insertFormOrder = this.formBuilderOrder.group({

      'appUserId': this.userIdAsNumber,
      'orderTime': this.orderTime,
      'orderStatus': this.orderStatus

    });

    let newOrder = this.insertFormOrder.value;


    await this.orderService.insertOrder(newOrder).then(
      result => {
        //console.log("New Order added");

      },
      error => console.log('Could not add Book')
    );

    //this.orderService.getOrders().subscribe(); //костыль

    await this.checkoutItem();

    await this.clearCart();

  }

  async checkoutItem() {
    await this.orderService.getLastOrderByUserId(this.userIdAsNumber).then( result => {

      // if (result === null) {
      //   this.orderService.getOrders().subscribe();
      // }
      this.orderIdValue = result.id;
      //console.log("Order id = " + this.orderIdValue);

      for (let itemCart of this.resCart) {


        this.orderId = new FormControl(this.orderIdValue);
        this.bookOfAuthorId = new FormControl(itemCart.boaId);
        this.quantityOrderItemsForm = new FormControl(itemCart.quantity);
        this.getTimeOrderItemsForm = new FormControl(null);
        this.returnTimeOrderItemsForm = new FormControl(null);
        this.insertFormOrderItems = this.formBuilderOrderItems.group({

          'orderId': this.orderIdValue,
          'boaId': this.bookOfAuthorId,
          'quantity': this.quantityOrderItemsForm,
          'getTime': this.getTimeOrderItemsForm,
          'returnTime': this.returnTimeOrderItemsForm

        });

        let newOrderItems = this.insertFormOrderItems.value;
        this.orderItemsService.insertOrderItems(newOrderItems).then();

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        this.stockService.getStockByBookOfAuthorId(itemCart.boaId).then(result => {
          this.stockId = result.id;
          this.amountBook = result.amount;
          //console.log("Amount book = " + this.amountBook);

          this.amountBook = this.amountBook - itemCart.quantity;
          //console.log("Amount book- = " + this.amountBook);

          this.bookOfAuthorId = new FormControl(itemCart.boaId);
          this.amountForm = new FormControl(this.amountBook);
          this.idStockForm = new FormControl(this.stockId);

          this.updateStockForm = this.formBuilderStock.group({
            'id' : this.idStockForm,
            'boaId': this.bookOfAuthorId,
            'amount': this.amountForm

          });

          let editedStock = this.updateStockForm.value;
          this.stockService.patchStock(editedStock.id, editedStock).then();
        });

      }

    });
  }

}
