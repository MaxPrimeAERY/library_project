export interface CartItems {
  id : number;
  cartId: number;
  boaId: number;
  quantity: number;
}
