export interface BookOfAuthorIds {
  id: number;
  authorId: number;
  bookId: number;
  imageId: number;
}
