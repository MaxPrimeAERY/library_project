export interface Avatar {
  id: number;
  image: string;
  appUserId : number;
}
