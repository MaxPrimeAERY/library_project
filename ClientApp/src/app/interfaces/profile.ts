export interface Profile {
  id: number;
  userName: string;
  email: string;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  allowComments: boolean;
}
