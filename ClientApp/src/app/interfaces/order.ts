export interface Order {
  id : number;
  appUserId : number;
  orderTime : Date;
  orderStatus : string;
}
