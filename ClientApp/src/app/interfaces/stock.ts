export interface Stock {
  id: number;
  boaId: number;
  totalAmount: number;
  amount: number;
}
