export interface BookImage {
  id: number;
  image: string;
}
