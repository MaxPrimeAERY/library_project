export interface CartInfo {
  id: number;
  name: string;
  bookname: string;
  genre: string;
  short_review: string;
  image: any;
  quantity: number;
}
