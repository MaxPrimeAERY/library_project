export interface CardOfReader {
  appUserId: number;
  boaId: number;
  quantityGet: number;
  quantityReturn?: number;
  remainedQuantity?: number;
}
