export interface OrderInfo {
  id: number;
  orderId: number;
  boaId: number;
  name: string;
  bookname: string;
  genre: string;
  short_review: string;
  image: any;
  quantity: number;
  getTime : Date;
}
