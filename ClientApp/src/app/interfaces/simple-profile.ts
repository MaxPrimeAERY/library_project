export interface SimpleProfile {
  id: number;
  userName: string;
  firstName: string;
  lastName: string;
  image?: any;
  allowComments: boolean;
}
