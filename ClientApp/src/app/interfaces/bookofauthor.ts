export interface BookOfAuthor {
  id: number;
  name: string;
  bookname: string;
  genre: string;
  short_review: string;
  image: any;
  amount?: number;
}
