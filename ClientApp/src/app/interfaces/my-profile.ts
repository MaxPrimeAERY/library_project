export interface MyProfile {
  id: number;
  userName: string;
  email: string;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  image?: any;
  allowComments: boolean;
}
