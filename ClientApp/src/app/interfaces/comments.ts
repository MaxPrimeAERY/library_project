export interface Comments {
  id: number;
  appUserId: number;
  boaId: number;
  text: string;
  rating?: number;
  commentTime: Date;
  viewedBy?: number;
}
