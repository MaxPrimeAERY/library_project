export interface Book {
  id:number;
  bookName:string;
  genre:string;
  shortReview:string;
}
