export interface OrderItems {
  id : number;
  orderId: number;
  boaId: number;
  quantity: number;
  getTime : Date;
}
