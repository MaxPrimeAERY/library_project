export interface ReturnedBook {
  id: number;
  appUserId: number;
  boaId: number;
  quantity: number;
  returnTime: Date;
}
