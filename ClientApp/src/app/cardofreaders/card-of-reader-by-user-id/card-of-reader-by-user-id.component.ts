import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {CardOfReaderService} from "../../services/card-of-reader.service";
import {ProfileService} from "../../services/profile.service";
import {BookOfAuthorService} from "../../services/bookofauthor.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {Observable, Subject} from "rxjs";
import {CardOfReader} from "../../interfaces/card-of-reader";
import {DataTableDirective} from "angular-datatables";

@Component({
  selector: 'app-card-of-reader-by-user-id',
  templateUrl: './card-of-reader-by-user-id.component.html',
  styleUrls: ['./card-of-reader-by-user-id.component.scss']
})
export class CardOfReaderByUserIdComponent implements OnInit {

  // Modal properties
  modalMessage : string;
  modalRef : BsModalRef;
  cardOfReaders$ : Observable<CardOfReader[]>;
  cardOfReaders : CardOfReader[] = [];
  userRoleStatus : string;

  userName: string[] = [];
  authorname: string[] = [];
  bookname: string[] = [];

  userIdAsNumber: number;

  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;

  constructor(private cardOfReaderService: CardOfReaderService,
              private profileService: ProfileService,
              private bookOfAuthorService: BookOfAuthorService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef : ChangeDetectorRef,
              private router: Router,
              private accs: AccountService) { }

  async ngOnInit() {
    await this.accs.currentUserId.subscribe(result => {
      this.userIdAsNumber = +result;
    });

    await this.cardOfReaderService.getCardOfReaderByUserId(this.userIdAsNumber).subscribe(async result => {
      this.cardOfReaders = result;

      for (let i = 0; i < this.cardOfReaders.length; i++) {
        await this.bookOfAuthorService.getAuthorBookById(this.cardOfReaders[i].boaId).then(res => {
            this.authorname.push(res.name);
            this.bookname.push(res.bookname);

          }
        );
      }

      this.chRef.detectChanges();

      this.dtTrigger.next();

    });
  }

}
