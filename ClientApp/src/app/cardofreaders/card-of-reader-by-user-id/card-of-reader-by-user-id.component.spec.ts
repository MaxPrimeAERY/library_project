import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardOfReaderByUserIdComponent } from './card-of-reader-by-user-id.component';

describe('CardOfReaderByUserIdComponent', () => {
  let component: CardOfReaderByUserIdComponent;
  let fixture: ComponentFixture<CardOfReaderByUserIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardOfReaderByUserIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardOfReaderByUserIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
