import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {CardOfReaderListComponent} from "./card-of-reader-list/card-of-reader-list.component";


const routes: Routes = [
  {path: '', component: CardOfReaderListComponent, canActivate: [AuthGuardService]},
  {path: 'card-of-reader-list', component:CardOfReaderListComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class CardOfReaderRoutingModule { }
