import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardOfReaderListComponent } from './card-of-reader-list.component';

describe('CardOfReaderListComponent', () => {
  let component: CardOfReaderListComponent;
  let fixture: ComponentFixture<CardOfReaderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardOfReaderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardOfReaderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
