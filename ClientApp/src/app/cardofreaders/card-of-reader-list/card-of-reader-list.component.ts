import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {CardOfReader} from "../../interfaces/card-of-reader";
import {CardOfReaderService} from "../../services/card-of-reader.service";
import {ProfileService} from "../../services/profile.service";
import {BookOfAuthorService} from "../../services/bookofauthor.service";

@Component({
  selector: 'app-card-of-reader-list',
  templateUrl: './card-of-reader-list.component.html',
  styleUrls: ['./card-of-reader-list.component.scss']
})
export class CardOfReaderListComponent implements OnInit {

  // Modal properties
  modalMessage : string;
  modalRef : BsModalRef;
  cardOfReaders$ : Observable<CardOfReader[]>;
  cardOfReaders : CardOfReader[] = [];
  userRoleStatus : string;

  userName: string[] = [];
  authorname: string[] = [];
  bookname: string[] = [];


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;

  constructor(private cardOfReaderService: CardOfReaderService,
              private profileService: ProfileService,
              private bookOfAuthorService: BookOfAuthorService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef : ChangeDetectorRef,
              private router: Router,
              private accs: AccountService) { }

  ngOnInit() {
    this.dtOptions = {
      search: true,
      searching: true,
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      paging: false,
      info: false,
      order: [[0, 'asc']]
    };

    this.cardOfReaders$ = this.cardOfReaderService.getCardOfReaders();

    this.cardOfReaders$.subscribe(async result => {
      this.cardOfReaders = result;
      for (let i = 0; i < this.cardOfReaders.length; i++) {
        await this.profileService.getProfileById(this.cardOfReaders[i].appUserId).then(res => {

            this.userName.push(res.userName);

          }
        );
      }
      for (let i = 0; i < this.cardOfReaders.length; i++) {
        await this.bookOfAuthorService.getAuthorBookById(this.cardOfReaders[i].boaId).then(res => {
            this.authorname.push(res.name);
            this.bookname.push(res.bookname);

          }
        );
      }

      this.chRef.detectChanges();

      this.dtTrigger.next();

    });

    //console.log("My returnedBooks info = "+JSON.stringify(this.returnedBooksInfo));


    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });
  }

}
