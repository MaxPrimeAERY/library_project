import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardOfReaderRoutingModule } from './card-of-reader-routing.module';
import {CardOfReaderListComponent} from "./card-of-reader-list/card-of-reader-list.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";


@NgModule({
  declarations: [
    CardOfReaderListComponent
  ],
  imports: [
    CommonModule,
    CardOfReaderRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors
})
export class CardOfReaderModule { }
