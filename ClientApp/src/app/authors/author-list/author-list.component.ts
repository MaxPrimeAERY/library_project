import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Author} from "../../interfaces/author";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {AuthorService} from "../../services/author.service";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss']
})
export class AuthorListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  name: FormControl;

  updateForm: FormGroup;
  _name: FormControl;
  _id: FormControl;

  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;


  // Modal properties
  modalMessage: string;
  modalRef: BsModalRef;
  selectedAuthor: Author;
  authors$: Observable<Author[]>;
  authors: Author[] = [];
  userRoleStatus: string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private authorservice: AuthorService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) {
  }

  /// Load Add New author Modal
  onAddAuthor() {
    this.modalRef = this.modalService.show(this.modal);
  }

  // Method to Add new Author
  onSubmit() {
    let newAuthor = this.insertForm.value;

    this.authorservice.insertAuthor(newAuthor).subscribe(
      result => {
        this.authorservice.clearCache();
        this.authors$ = this.authorservice.getAuthors();

        this.authors$.subscribe(newlist => {
          this.authors = newlist;
          this.modalRef.hide();
          this.insertForm.reset();
          this.rerender();

        });
        this.myToastrService.messageSuccess("New Author added");
        //console.log("New Author added");

      },
      error => this.myToastrService.messageSuccess('Could not add Author')
    )

  }

  // We will use this method to destroy old table and re-render new table

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing Author
  onUpdate() {
    let editAuthor = this.updateForm.value;
    this.authorservice.updateAuthor(editAuthor.id, editAuthor).subscribe(
      result => {
        //console.log('Author Updated');
        this.myToastrService.messageSuccess('Author Updated');
        this.authorservice.clearCache();
        this.authors$ = this.authorservice.getAuthors();
        this.authors$.subscribe(updatedlist => {
          this.authors = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageSuccess('Could not update Author')
    )
  }

  // Load the update Modal

  onUpdateModal(authorEdit: Author): void {
    this._id.setValue(authorEdit.id);
    this._name.setValue(authorEdit.name);

    this.updateForm.setValue({
      'id': this._id.value,
      'name': this._name.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the author
  onDelete(author: Author): void {
    this.authorservice.deleteAuthor(author.id).subscribe(result => {
      this.myToastrService.messageSuccess('Author is deleted successfully');

      this.authorservice.clearCache();
      this.authors$ = this.authorservice.getAuthors();
      this.authors$.subscribe(newlist => {
        this.authors = newlist;

        this.rerender();
      })
    })
  }

  onSelect(author: Author): void {
    this.selectedAuthor = author;

    this.router.navigateByUrl("/authors/" + author.id);
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    this.authors$ = this.authorservice.getAuthors();

    this.authors$.subscribe(result => {
      this.authors = result;

      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add author properties

    //let validateImageUrl: string = '^(https?:\/\/.*\.(?:png|jpg))$';

    this.name = new FormControl('', [Validators.required,
      Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);

    //this.imageUrl = new FormControl('', [Validators.pattern(validateImageUrl)]);

    this.insertForm = this.fb.group({

      'name': this.name
    });

    // Initializing Update Author properties
    this._name = new FormControl('', [Validators.required,
      Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    //this._imageUrl = new FormControl('', [Validators.pattern(validateImageUrl)]);
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id': this._id,
        'name': this._name

      });


  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
