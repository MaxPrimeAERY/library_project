import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorsRoutingModule } from './authors-routing.module';
import {AuthorListComponent} from "./author-list/author-list.component";
import {AuthorDetailsComponent} from "./author-details/author-details.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {MatOptionModule} from "@angular/material";
import {ToastrModule} from "ngx-toastr";


@NgModule({
  declarations: [
    AuthorListComponent,
    AuthorDetailsComponent
  ],
  imports: [
    CommonModule,
    AuthorsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors

})
export class AuthorsModule { }
