import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthorListComponent} from "./author-list/author-list.component";
import {AuthorDetailsComponent} from "./author-details/author-details.component";
import {AuthGuardService} from "../guards/auth-guard.service";


const routes: Routes = [
  {path: '', component: AuthorListComponent, canActivate: [AuthGuardService]},
  {path: 'author-list', component:AuthorListComponent, canActivate: [AuthGuardService]},
  {path: ':id', component:AuthorDetailsComponent, canActivate: [AuthGuardService]},
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class AuthorsRoutingModule { }
