import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Avatar} from "../../interfaces/avatar";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {AvatarService} from "../../services/avatar.service";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {DomSanitizer} from "@angular/platform-browser";
import {ProfileService} from "../../services/profile.service";
import {Profile} from "../../interfaces/profile";
import {MyToastrService} from "../../services/my-toastr.service";
import {DOC_ORIENTATION, NgxImageCompressService} from "ngx-image-compress";

@Component({
  selector: 'app-avatar-list',
  templateUrl: './avatar-list.component.html',
  styleUrls: ['./avatar-list.component.scss'],
  providers: [NgxImageCompressService]
})
export class AvatarListComponent implements OnInit, OnDestroy {
  insertForm: FormGroup;
  image: FormControl;
  appUserId: FormControl;
  imageSrc: string = '';
  hasImage = false;

  updateForm: FormGroup;
  _image: FormControl;
  _appUserId: FormControl;
  _id: FormControl;

  /////////////////////////////////

  profile: Profile[] = [];
  userName: string[] = [];

  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;


  // Modal properties
  modalMessage: string;
  modalRef: BsModalRef;
  selectedAvatar: Avatar;
  avatars$: Promise<Avatar[]>;
  avatars: Avatar[] = [];
  userRoleStatus: string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private avatarservice: AvatarService,
              private profileService: ProfileService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private sanitizer: DomSanitizer,
              private imageCompress: NgxImageCompressService,
              private myToastrService: MyToastrService) {
  }

  /// Load Add New avatar Modal
  onAddAvatar() {
    this.imageSrc = null;
    this.hasImage = false;
    if (this.imageSrc == null) {
      this.insertForm.reset();
      this.rerender();
    }
    this.modalRef = this.modalService.show(this.modal);
  }

  transform() {
    //console.log("This is render log" + this.image.value);
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.image.value);
  }

  // Method to Add new Avatar
  onSubmit() {
    let newAvatar = this.insertForm.value;

    this.avatarservice.insertAvatar(newAvatar).subscribe(
      result => {
        this.avatarservice.clearCache();
        this.avatars$ = this.avatarservice.getAvatars();

        this.avatars$.then(newlist => {
          this.avatars = newlist;
          this.modalRef.hide();
          this.insertForm.reset();
          this.rerender();

        });
        //console.log("New Avatar added");
        this.myToastrService.messageSuccess("New Avatar added");
      },
      error => this.myToastrService.messageError('Could not add Avatar')
    )

  }

  // We will use this method to destroy old table and re-render new table

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing Avatar
  onUpdate() {
    let editAvatar = this.updateForm.value;
    this.avatarservice.updateAvatar(editAvatar.id, editAvatar).subscribe(
      result => {
        this.myToastrService.messageSuccess('Avatar Updated');
        this.avatarservice.clearCache();
        this.avatars$ = this.avatarservice.getAvatars();
        this.avatars$.then(updatedlist => {
          this.avatars = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageError('Could Not Update Avatar')
    )
  }

  // Load the update Modal

  onUpdateModal(avatarEdit: Avatar): void {
    this._id.setValue(avatarEdit.id);
    this._image.setValue(avatarEdit.image);
    this._appUserId.setValue(avatarEdit.appUserId);

    this.updateForm.setValue({
      'id': this._id.value,
      'image': this._image.value,
      'AppUserId': this._appUserId.value
    });


    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the avatar
  onDelete(avatar: Avatar): void {
    this.avatarservice.deleteAvatar(avatar.id).subscribe(result => {
      this.myToastrService.messageSuccess('Avatar is deleted successfully');

      this.avatarservice.clearCache();
      this.avatars$ = this.avatarservice.getAvatars();
      this.avatars$.then(newlist => {
        this.avatars = newlist;

        this.rerender();
      })
    })
  }

  onSelect(avatar: Avatar): void {
    this.selectedAvatar = avatar;

    this.router.navigateByUrl("/avatars/" + avatar.id);
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    this.avatars$ = this.avatarservice.getAvatars();

    this.avatars$.then(async result => {
      this.avatars = result;

      for (let i = 0; i < this.avatars.length; i++) {
        await this.profileService.getProfileById(this.avatars[i].appUserId).then(res => {

            this.userName.push(res.userName);

          }
        );
      }
      //console.log("UserAva = "+this.userName);

      this.transform();

      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add avatar properties

    this.image = new FormControl('', [Validators.required]);
    this.appUserId = new FormControl('', [Validators.required]);


    this.insertForm = this.fb.group({

      'image': this.image,
      'AppUserId': this.appUserId
    });

    // Initializing Update Avatar properties
    this._image = new FormControl('', [Validators.required]);
    this._appUserId = new FormControl('', [Validators.required]);
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id': this._id,
        'image': this._image,
        'AppUserId': this._appUserId

      });

    this.profileService.getProfiles().subscribe(res => {
      this.profile = res;
    });

  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {

    let reader = e.target;
    this.imageSrc = reader.result;
    //console.log(this.imageSrc);
    this.imageCompress.compressFile(this.imageSrc, DOC_ORIENTATION, 75, 75).then(
      result => {
        //console.log(result);
        this.imageSrc = result;
        this.image.setValue(this.imageSrc);
        this._image.setValue(this.imageSrc);
        //console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
      }
    );
    this.hasImage = true;
    this.image.setValue(this.imageSrc);
    this._image.setValue(this.imageSrc);

  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
