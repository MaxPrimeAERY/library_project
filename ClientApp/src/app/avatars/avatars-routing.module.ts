import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {AvatarListComponent} from "./avatar-list/avatar-list.component";


const routes: Routes = [
  {path: '', component: AvatarListComponent, canActivate: [AuthGuardService]},
  {path: 'avatar-list', component:AvatarListComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class AvatarsRoutingModule { }
