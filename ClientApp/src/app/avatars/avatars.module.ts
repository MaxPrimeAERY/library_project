import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AvatarsRoutingModule } from './avatars-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {AvatarListComponent} from "./avatar-list/avatar-list.component";
import {MatOptionModule, MatSelectModule} from "@angular/material";
import {ToastrModule} from "ngx-toastr";
import {NgxImageCompressService} from "ngx-image-compress";


@NgModule({
  declarations: [
    AvatarListComponent
  ],
  imports: [
    CommonModule,
    AvatarsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    MatSelectModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}, //multi - for several interceptors
    NgxImageCompressService]
})
export class AvatarsModule { }
