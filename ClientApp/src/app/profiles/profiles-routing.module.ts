import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileListComponent} from "./profile-list/profile-list.component";
import {AuthGuardService} from "../guards/auth-guard.service";


const routes: Routes = [

  {path: '', component: ProfileListComponent, canActivate: [AuthGuardService]},
  {path: 'profile-list', component:ProfileListComponent, canActivate: [AuthGuardService]}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class ProfilesRoutingModule {

}
