import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilesRoutingModule } from './profiles-routing.module';
import {ProfileListComponent} from "./profile-list/profile-list.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {MatOptionModule} from "@angular/material";
import {ToastrModule} from "ngx-toastr";
import {NgxImageCompressService} from "ngx-image-compress";


@NgModule({
  declarations: [
    ProfileListComponent
  ],
  imports: [
    CommonModule,
    ProfilesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    ToastrModule.forRoot(),
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    NgxImageCompressService
    ], //multi - for several interceptors

})
export class ProfilesModule { }
