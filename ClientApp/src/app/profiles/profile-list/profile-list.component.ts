import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {Profile} from "../../interfaces/profile";
import {ProfileService} from "../../services/profile.service";
import {DataTableDirective} from "angular-datatables";
import {ToastrService} from "ngx-toastr";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  userName: FormControl;
  email: FormControl;
  phoneNumber: FormControl;
  firstName: FormControl;
  lastName: FormControl;
  allowComments: FormControl;

  updateForm: FormGroup;
  _userName: FormControl;
  _email: FormControl;
  _phoneNumber: FormControl;
  _firstName: FormControl;
  _lastName: FormControl;
  _allowComments: FormControl;
  _id: FormControl;

  // Add Modal
 // @ViewChild('template', { static: true }) modal : TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', { static: true }) editmodal : TemplateRef<any>;


  // Modal properties
  modalMessage : string;
  modalRef : BsModalRef;
  selectedProfile : Profile;
  profiles$ : Observable<Profile[]>;
  profiles : Profile[] = [];
  userRoleStatus : string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;



  constructor(private profileservice : ProfileService,
              private modalService: BsModalService,
              private toastrService: ToastrService,
              private fb: FormBuilder,
              private chRef : ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) { }

  // /// Load Add New profile Modal
  // onAddProfile()
  // {
  //   this.modalRef = this.modalService.show(this.modal);
  // }
  //
  // // Method to Add new Profile
  // onSubmit()
  // {
  //   let newProfile = this.insertForm.value;
  //
  //   this.profileservice.insertProfile(newProfile).subscribe(
  //     result =>
  //     {
  //       this.profileservice.clearCache();
  //       this.profiles$ = this.profileservice.getProfiles();
  //
  //       this.profiles$.subscribe(newlist => {
  //         this.profiles = newlist;
  //         this.modalRef.hide();
  //         this.insertForm.reset();
  //         this.rerender();
  //
  //       });
  //       //console.log("New Profile added");
  //
  //     },
  //     error => //console.log('Could not add Profile')
  //
  //   )
  //
  // }

  // We will use this method to destroy old table and re-render new table

  rerender()
  {
    this.dtElement.dtInstance.then((dtInstance : DataTables.Api) =>
    {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing Profile
  onUpdate()
  {
    let editProfile = this.updateForm.value;
    this.profileservice.updateProfile(editProfile.id, editProfile).subscribe(
      result =>
      {
        this.myToastrService.messageSuccess('Profile Updated');
        this.profileservice.clearCache();
        this.profiles$ = this.profileservice.getProfiles();
        this.profiles$.subscribe(updatedlist =>
        {
          this.profiles = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageError('Could Not Update Profile')
    )
  }

  // Load the update Modal

  onUpdateModal(profileEdit: Profile) : void
  {
    this._id.setValue(profileEdit.id);
    this._userName.setValue(profileEdit.userName);
    this._email.setValue(profileEdit.email);
    this._phoneNumber.setValue(profileEdit.phoneNumber);
    this._firstName.setValue(profileEdit.firstName);
    this._lastName.setValue(profileEdit.lastName);
    this._allowComments.setValue(profileEdit.allowComments);

    this.updateForm.setValue({
      'id' : this._id.value,
      'userName' : this._userName.value,
      'email' :  this._email.value,
      'phoneNumber' : this._phoneNumber.value,
      'firstName' : this._firstName.value,
      'lastName' : this._lastName.value,
      'allowComments': this._allowComments.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the profile
  async onDelete(profile: Profile) {

    let adminName;
    await this.profileservice.getProfileByUserName("admin").then(result => {
      adminName = result.userName;

      if (profile.userName == adminName) { //In other projects might be other values!!!

        this.toastrService.error(
          "Admin can't remove himself :)",
          "Error",
          {
            positionClass: 'toast-top-center',
            closeButton: true,
            timeOut: 5000,
            onActivateTick: true
          }
        )

      } else {
        this.profileservice.deleteProfile(profile.id).subscribe(result => {
          this.profileservice.clearCache();
          this.profiles$ = this.profileservice.getProfiles();
          this.profiles$.subscribe(newlist => {
            this.profiles = newlist;

            this.rerender();
          })
        })
      }
    });

  }

  onSelect(profile: Profile) : void
  {
    this.selectedProfile = profile;

    this.router.navigateByUrl("/profiles/" + profile.id);
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    this.profiles$ = this.profileservice.getProfiles();

    this.profiles$.subscribe(result => {
      this.profiles = result;

      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {this.userRoleStatus = result});


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add profile properties

    //let validateImageUrl: string = '^(https?:\/\/.*\.(?:png|jpg))$';

    this.userName = new FormControl('', [Validators.required,
      Validators.maxLength(12), Validators.minLength(3)]);
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.phoneNumber = new FormControl('', [Validators.maxLength(25)]);
    this.firstName = new FormControl('',[Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this.lastName = new FormControl('',[Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this.allowComments = new FormControl('', Validators.required);

    this.insertForm = this.fb.group({

      'userName' : this.userName,
      'email' : this.email,
      'phoneNumber' : this.phoneNumber,
      'firstName' : this.firstName,
      'lastName' : this.lastName,
      'allowComments': this.allowComments

    });

    // Initializing Update Profile properties
    this._userName = new FormControl('', [Validators.required,
      Validators.maxLength(12), Validators.minLength(3)]);
    this._email = new FormControl('', [Validators.required, Validators.email]);
    this._phoneNumber = new FormControl('', [Validators.maxLength(25)]);
    this._firstName = new FormControl('',[Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this._lastName = new FormControl('',[Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this._allowComments = new FormControl('', Validators.required);
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id' : this._id,
        'userName' : this._userName,
        'email' : this._email,
        'phoneNumber' : this._phoneNumber,
        'firstName' : this._firstName,
        'lastName' : this._lastName,
        'allowComments': this._allowComments

      });


  }

  ngOnDestroy()
  {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
