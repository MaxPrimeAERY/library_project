import {Component, Input, OnInit} from '@angular/core';
import {MyProfile} from "../../interfaces/my-profile";
import {Observable} from "rxjs";
import {ProfileService} from "../../services/profile.service";
import {DomSanitizer} from "@angular/platform-browser";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AvatarService} from "../../services/avatar.service";
import {Avatar} from "../../interfaces/avatar";
import {MyToastrService} from "../../services/my-toastr.service";
import {AccountService} from "../../services/account.service";
import {DOC_ORIENTATION, NgxImageCompressService} from "ngx-image-compress";

@Component({
  selector: 'app-profile-by-user-id',
  templateUrl: './profile-by-user-id.component.html',
  styleUrls: ['./profile-by-user-id.component.scss'],
  providers: [NgxImageCompressService]
})
export class ProfileByUserIdComponent implements OnInit {

  userIdAsNumber: number;

  @Input() myProfile: MyProfile;
  @Input() myAvatar: Avatar;
  hasPhoto = false;
  UserName$: Observable<string>;
  imageSrc: string = '';

  insertFormImage: FormGroup;
  image: FormControl;
  appUserId: FormControl;

  updateFormImage: FormGroup;
  _image: FormControl;
  _appUserId: FormControl;
  _idI: FormControl;

  updateFormProfile: FormGroup;
  _firstname: FormControl;
  _lastname: FormControl;
  _phoneNumber: FormControl;
  _id: FormControl;

  enableEdit = false;

  applyNewImage = false;

  constructor(private accs: AccountService,
              private profileService: ProfileService,
              private avatarService: AvatarService,
              private sanitizer: DomSanitizer,
              private imageCompress: NgxImageCompressService,
              private fb: FormBuilder,
              private fbImage: FormBuilder,
              private myToastrService: MyToastrService) {
  }

  async ngOnInit() {

    await this.accs.currentUserId.subscribe(result => {
      this.userIdAsNumber = +result;
    });

    await this.profileService.getMyProfile(this.userIdAsNumber).then(result => {
      this.myProfile = result;
      //console.log("My profile" + this.myProfile);
      //console.log("Current id = " + this.myProfile.id);
      if (result.image != null) {
        this.myProfile.image = this.sanitizer.bypassSecurityTrustUrl(result.image);
        this.imageSrc = result.image;
        this.hasPhoto = true;
      }
    });

    await this.avatarService.getAvatarByUserId(this.userIdAsNumber).subscribe(result => {
      this.myAvatar = result;
      if (this.myAvatar == null || this.myAvatar.image.length < 1) {
        this.hasPhoto = false;
      }
      //console.log("My current avatar = "+this.myAvatar.image);
    });

    this.image = new FormControl('', [Validators.required]);
    this.appUserId = new FormControl('', [Validators.required]);


    this.insertFormImage = this.fb.group({

      'image': this.image,
      'AppUserId': this.appUserId
    });


    this._image = new FormControl('', [Validators.required]);
    this._appUserId = new FormControl('', [Validators.required]);
    this._idI = new FormControl();


    this._firstname = new FormControl('', [Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this._lastname = new FormControl('', [Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this._phoneNumber = new FormControl('');
    this._id = new FormControl();

    this.updateFormProfile = this.fb.group(
      {
        'id': this._id,
        'firstname': this._firstname,
        'lastname': this._lastname,
        'phonenumber': this._phoneNumber

      });

    this.updateFormImage = this.fbImage.group(
      {
        'id': this._idI,
        'image': this._image,
        'appUserId': this._appUserId

      });

  }

  onUpdate() {
    let editProfile = this.updateFormProfile.value;
    this.profileService.patchProfile(editProfile.id, editProfile).subscribe(
      result => {
        this.myToastrService.messageSuccess('Profile Updated');
        this.profileService.clearCache();
        this.profileService.getMyProfile(this.userIdAsNumber).then(result => {
          this.myProfile = result;
        });
      },
      error => this.myToastrService.messageError('Could Not Update Profile')
    )
  }

  onUpdateFakeModal(profileEdit: MyProfile) {
    this._id.setValue(profileEdit.id);
    this._firstname.setValue(profileEdit.firstName);
    this._lastname.setValue(profileEdit.lastName);
    this._phoneNumber.setValue(profileEdit.phoneNumber);

    this.updateFormProfile.patchValue({
      'id': this._id.value,
      'firstname': this._firstname.value,
      'lastname': this._lastname.value,
      'phonenumber': this._phoneNumber.value,
    });
  }

  onUpdateImage(avatarEdit: Avatar) {

    //console.log("_image 2 =" + this._image.value);
    this._idI.setValue(avatarEdit.id);
    //this._image.setValue(avatarEdit.image);
    this._appUserId.setValue(avatarEdit.appUserId);

    this.updateFormImage.setValue({
      'id': this._idI.value,
      'image': this._image.value,
      'appUserId': this._appUserId.value
    });
    //console.log("My new avatar = " + this._image.value);

    let editImage = this.updateFormImage.value;

    //console.log("Editimage = " + editImage.id);
    this.avatarService.updateAvatar(editImage.id, editImage).subscribe(
      result => {
        this.myToastrService.messageSuccess('Profile Image Updated');
        this.avatarService.clearCache();
        this.avatarService.getAvatarByUserId(this.userIdAsNumber).subscribe(result => {
          this.myAvatar = result;
          //console.log("My new avatar 2 len= " + this.myAvatar.image.length);
        });
        //window.location.reload();
      },
      error => this.myToastrService.messageError('Could Not Update Profile Image')
    );

    this.applyNewImage = false;

  }

  onInsertImage() {

    // this.imageSrc = null;
    // this.hasPhoto = false;
    // if (this.imageSrc == null){
    //   this.insertFormImage.reset();
    // }
    this.appUserId.setValue(this.userIdAsNumber);

    let newAvatar = this.insertFormImage.value;
    //console.log(newAvatar);

    this.avatarService.insertAvatar(newAvatar).subscribe(
      result => {
        this.avatarService.clearCache();
        this.avatarService.getAvatarByUserId(this.userIdAsNumber).subscribe(result => {
          this.myAvatar = result;
          //console.log("My new avatar 2 = " + this.myAvatar.image);
        });
        //console.log("New Avatar added");
        this.myToastrService.messageSuccess("New Avatar added successfully");
      },
      error => this.myToastrService.messageError('Could not add Avatar')
    );
    this.applyNewImage = false;
  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);

  }

  async _handleReaderLoaded(e) {

    let reader = e.target;
    this.imageSrc = reader.result;

    this.imageCompress.compressFile(this.imageSrc, DOC_ORIENTATION, 75, 75).then(
      result => {
        //console.log(result);
        this.imageSrc = result;
        this.image.setValue(this.imageSrc);
        this._image.setValue(this.imageSrc);
        //console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
      }
    );

    this.hasPhoto = true;
    this.image.setValue(this.imageSrc);
    this._image.setValue(this.imageSrc);

    //console.log("_image 1 =" + this._image.value);
  }

  editable(value: boolean, profileEdit: MyProfile) {
    this.enableEdit = value;
    if (this.enableEdit == true) {
      this.onUpdateFakeModal(profileEdit);
    }
  }

  onDoubleSubmit(profileEdit: MyProfile) {
    this.editable(false, profileEdit);
    this.onUpdate();
  }

  onApplyNewImage(value: boolean) {
    this.applyNewImage = value;
  }

}
