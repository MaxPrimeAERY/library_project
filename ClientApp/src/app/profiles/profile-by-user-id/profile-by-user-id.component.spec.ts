import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileByUserIdComponent } from './profile-by-user-id.component';

describe('ProfileByUserIdComponent', () => {
  let component: ProfileByUserIdComponent;
  let fixture: ComponentFixture<ProfileByUserIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileByUserIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileByUserIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
