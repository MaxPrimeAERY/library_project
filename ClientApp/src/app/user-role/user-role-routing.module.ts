import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {UserRoleListComponent} from "./user-role-list/user-role-list.component";


const routes: Routes = [
  {path: '', component: UserRoleListComponent, canActivate: [AuthGuardService]},
  {path: 'user-role-list', component: UserRoleListComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class UserRoleRoutingModule {
}
