import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {UserRole} from "../../interfaces/user-role";
import {UserRoleService} from "../../services/user-role.service";
import {Role} from "../../interfaces/role";
import {Profile} from "../../interfaces/profile";
import {ProfileService} from "../../services/profile.service";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-user-role-list',
  templateUrl: './user-role-list.component.html',
  styleUrls: ['./user-role-list.component.scss']
})
export class UserRoleListComponent implements OnInit, OnDestroy {


  insertForm: FormGroup;
  userId: FormControl;
  roleId: FormControl;

  roleName: string[] = [];
  roleNameAdd: string[] = [];
  firstname: string[] = [];
  lastname: string[] = [];
  email: string[] = [];

  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;


  // Modal properties
  modalMessage: string;
  modalRef: BsModalRef;
  selectedUserRole: UserRole;
  userRoles$: Observable<UserRole[]>;
  userRoles: UserRole[] = [];

  /////////////////////////////
  userRoleStatus: string;

  /////////////////////////////

  roles$: Observable<Role[]>;
  roles: Role[] = [];

  /////////////////////////////

  profiles$: Observable<Profile[]>;
  profiles: Profile[] = [];


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private userRoleservice: UserRoleService,
              private profileService: ProfileService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) {
  }

  /// Load Add New book Modal
  onAddUserRole() {
    this.modalRef = this.modalService.show(this.modal);
  }

  // Method to Add new UserRole
  onSubmit() {
    let newUserRole = this.insertForm.value;

    this.userRoleservice.insertUserRole(newUserRole).subscribe(
      result => {
        this.userRoleservice.clearCache();

        this.userRoleservice.getUserRoles().then(newlist => {
          this.userRoles = newlist;
          this.modalRef.hide();
          this.insertForm.reset();
          this.rerender();

        });
        this.myToastrService.messageSuccess("New UserRole added");
        window.location.reload();
      },
      error => this.myToastrService.messageError('Could not add UserRole')
    )

  }

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }


  onSelect(userRole: UserRole): void {
    this.selectedUserRole = userRole;

    this.router.navigateByUrl("/userRoles/" + userRole.userId);
  }

  async onDelete(userRole: UserRole) {

    let adminId;
    await this.profileService.getProfileByUserName("admin").then(result => {
      adminId = result.id;

      if (userRole.userId == adminId && userRole.roleId == 1) { //In other projects might be other values!!!

        this.myToastrService.messageError("Admin can't remove his admin role");

      } else {
        this.userRoleservice.deleteUserRole(userRole.userId, userRole.roleId).subscribe(result => {
          this.userRoleservice.clearCache();
          this.userRoleservice.getUserRoles().then(newlist => {
            this.userRoles = newlist;

            this.rerender();
          })
        })
      }
    });


  }

  async ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    await this.userRoleservice.getUserRoles().then(async result => {
      this.userRoles = result;

      for (let i = 0; i < this.userRoles.length; i++) {

        await this.profileService.getProfileById(this.userRoles[i].userId).then(res => {
            //console.log("USEROLESLEN = " + res.firstName);
            this.firstname.push(res.firstName);
            this.lastname.push(res.lastName);
            this.email.push(res.email);
          }
        );

        await this.userRoleservice.getRoleById(this.userRoles[i].roleId).then(res => {
            this.roleNameAdd.push(res.name);

          }
        );
      }

      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    this.userId = new FormControl('', [Validators.required]);
    this.roleId = new FormControl('', [Validators.required]);


    this.insertForm = this.fb.group({
      'userId': this.userId,
      'roleId': this.roleId
    });

    this.userRoleservice.getRoles().subscribe(async result => {
      this.roles = result;
    });

    this.profileService.getProfiles().subscribe(result => {
      this.profiles = result;
    });

  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
