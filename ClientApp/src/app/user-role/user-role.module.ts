import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoleRoutingModule} from './user-role-routing.module';
import {UserRoleListComponent} from "./user-role-list/user-role-list.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule, MatOptionModule, MatSelectModule
} from "@angular/material";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {ToastrModule} from "ngx-toastr";


@NgModule({
  declarations: [
    UserRoleListComponent
  ],
  imports: [
    CommonModule,
    UserRoleRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    MatListModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors
})
export class UserRoleModule {
}
