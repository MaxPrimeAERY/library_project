import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {AccountService} from "../services/account.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {MyToastrService} from "../services/my-toastr.service";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  model: any = {};
  successMessage: string;
  errorMessage: string;
  insertForm: FormGroup;
  password: FormControl;
  cpassword: FormControl;
  errorList: string[];

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private accs: AccountService,
              private router: Router,
              private myToastrService: MyToastrService) {
  }

  ngOnInit() {

    if (this.accs.checkLogInStatus() == true){
      this.router.navigate(['/']);
    }

    this.model.code = this.route.snapshot.queryParamMap.get('code');
    this.model.userid = this.route.snapshot.queryParamMap.get('userid');

    this.password = new FormControl('', [Validators.required,
      Validators.minLength(4)]);
    this.cpassword = new FormControl('', [Validators.required,
      this.mustMatch(this.password)]);


    this.insertForm = this.formBuilder.group({
      "password": this.password,
      'cpassword': this.cpassword,
    })
  }

  onSubmit() {

    this.accs.resetPassword(this.model).subscribe(
      data => {
        this.successMessage = "Password was reset successfully";
        this.myToastrService.messageSuccess("Password was reset successfully");
        setTimeout(() => {
          this.successMessage = null;
          this.router.navigate(['/login']);
        }, 5000);

      }, error => {
        //console.log(error);
        this.myToastrService.messageError("Something went wrong");
      })
  }


  mustMatch(passwordControl: AbstractControl): ValidatorFn {
    //key contains string with error, boolean = true if we have an error, null if NO error
    return (cpasswordControl: AbstractControl):
      { [key: string]: boolean } | null => {
      //null if haven't initialized yet
      if (!passwordControl && !cpasswordControl) {
        return null;
      }
      //null if another validator has already found an error on the MatchingControl
      if (cpasswordControl.hasError && !passwordControl.hasError) {
        return null;
      }
      //if validation fails
      if (passwordControl.value !== cpasswordControl.value) {
        return {'mustMatch': true};
      } else {
        return null;
      }

    }
  }
}
