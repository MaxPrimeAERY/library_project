import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder, FormArray} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {ReturnedBook} from "../../interfaces/returned-book";
import {ReturnedBookService} from "../../services/returned-book.service";
import {DataTableDirective} from "angular-datatables";
import {Book} from "../../interfaces/book";
import {StockService} from "../../services/stock.service";
import {OrderService} from "../../services/order.service";
import {OrderItemsService} from "../../services/order-items.service";
import {ProfileService} from "../../services/profile.service";
import {BookOfAuthorService} from "../../services/bookofauthor.service";
import {Profile} from "../../interfaces/profile";
import {BookOfAuthor} from "../../interfaces/bookofauthor";
import {HelperService} from "../../services/helper.service";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-returned-book-list',
  templateUrl: './returned-book-list.component.html',
  styleUrls: ['./returned-book-list.component.scss']
})
export class ReturnedBookListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  appUserId: FormControl;
  boaId: FormControl;
  quantity: FormControl;
  returnTime: FormControl;

  updateForm: FormGroup;
  _appUserId: FormControl;
  _boaId: FormControl;
  _quantity: FormControl;
  _returnTime: FormControl;
  _id: FormControl;

  userName: string[] = [];
  authorname: string[] = [];
  bookname: string[] = [];
  profile: Profile[] = [];
  bookOfAuthor: BookOfAuthor[] = [];

  formatDateTime: string = 'dd.MM.yyyy HH:mm:ss';

  //////////////////////////////

  stockId: number;
  amountBook: number;
  bookOfAuthorId: FormControl;
  amountForm: FormControl;
  idStockForm: FormControl;
  updateStockForm: FormGroup;

  getTimeOfOrderItem: Date;


  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;

  @ViewChild('editTemplateItem', {static: true}) editmodalitem: TemplateRef<any>;

  @ViewChild('errorTemplate', {static: true}) modalError: TemplateRef<any>;

  @ViewChild('errorTemplate2', {static: true}) modalError2: TemplateRef<any>;

  @ViewChild('errorTemplate3', {static: true}) modalError3: TemplateRef<any>;

  // Modal properties
  modalMessage: string;
  modalMessageErr: string;
  modalMessageErr2: string;
  modalMessageErr3: string;
  modalRef: BsModalRef;
  selectedReturnedBook: ReturnedBook;
  returnedBooks$: Observable<ReturnedBook[]>;
  returnedBooks: ReturnedBook[] = [];
  userRoleStatus: string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private returnedBookService: ReturnedBookService,
              private stockService: StockService,
              private orderItemsService: OrderItemsService,
              private profileService: ProfileService,
              private bookOfAuthorService: BookOfAuthorService,
              private helperService: HelperService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private fbItems: FormBuilder,
              private formBuilderStock: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) {
  }

  /// Load Add New returnedBook Modal
  onAddReturnedBook() {
    this.modalRef = this.modalService.show(this.modal);
  }

  // Method to Add new ReturnedBook
  onSubmit() {
    let newReturnedBook = this.insertForm.value;
    //console.log("Book id = " + newReturnedBook.boaId);
    this.stockService.getStockByBookOfAuthorId(newReturnedBook.boaId).then(async result => {
      this.stockId = result.id;
      this.amountBook = result.amount;
      //console.log("Amount book = " + this.amountBook);

      this.amountBook = this.amountBook + +newReturnedBook.quantity;
      //console.log("Amount book+ = " + this.amountBook);

      if (result.totalAmount < this.amountBook) {

        this.modalRef = this.modalService.show(this.modalError);
        return;
      }

      this.bookOfAuthorId = new FormControl(newReturnedBook.boaId);
      this.amountForm = new FormControl(this.amountBook);
      this.idStockForm = new FormControl(this.stockId);

      this.updateStockForm = this.formBuilderStock.group({
        'id': this.idStockForm,
        'boaId': this.bookOfAuthorId,
        'amount': this.amountForm

      });

      let editedStock = this.updateStockForm.value;
      this.stockService.patchStock(editedStock.id, editedStock).then();

      this.returnedBookService.insertReturnedBook(newReturnedBook).then(
        result => {
          this.returnedBookService.clearCache();
          this.returnedBooks$ = this.returnedBookService.getReturnedBooks();

          this.returnedBooks$.subscribe(newlist => {
            this.returnedBooks = newlist;

            this.modalRef.hide();
            this.insertForm.reset();

          });
          //console.log("New ReturnedBook added");
          this.myToastrService.messageSuccess("New ReturnedBook added");
          window.location.reload();
        },
        error => this.myToastrService.messageError('Could not add ReturnedBook')
      );
      //this.rerender();
    });
    this.modalRef.hide();

  }

  // We will use this method to destroy old table and re-render new table

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  onSelect(returnedBook: ReturnedBook): void {
    this.selectedReturnedBook = returnedBook;

    this.router.navigateByUrl("/returned-book/" + returnedBook.id);
  }

  // Update an Existing ReturnedBook
  onUpdate() {
    let editReturnedBook = this.updateForm.value;
    this.returnedBookService.updateReturnedBook(editReturnedBook.id, editReturnedBook).subscribe(
      result => {
        this.myToastrService.messageSuccess('ReturnedBook Updated');
        this.returnedBookService.clearCache();
        this.returnedBooks$ = this.returnedBookService.getReturnedBooks();
        this.returnedBooks$.subscribe(updatedlist => {
          this.returnedBooks = updatedlist;

          this.modalRef.hide();
          this.rerender();
          window.location.reload();
        });
      },
      error => {//console.log('Could Not Update ReturnedBook');
        this.myToastrService.messageError("Could Not Update ReturnedBook");}
    )
  }

  // Load the update Modal

  onUpdateModal(returnedBookEdit: ReturnedBook): void {
    this._id.setValue(returnedBookEdit.id);
    this._appUserId.setValue(returnedBookEdit.appUserId);
    this._boaId.setValue(returnedBookEdit.boaId);
    this._quantity.setValue(returnedBookEdit.quantity);
    this._returnTime.setValue(returnedBookEdit.returnTime);


    this.updateForm.setValue({
      'id': this._id.value,
      'appUserId': this._appUserId.value,
      'boaId': this._boaId.value,
      'quantity': this._quantity.value,
      'returnTime': this._returnTime.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }


  // Method to Delete the returnedBook
  onDelete(returnedBook: ReturnedBook): void {
    this.returnedBookService.deleteReturnedBook(returnedBook.id).subscribe(result => {
      this.myToastrService.messageSuccess('Returned book is deleted successfully');

      this.returnedBookService.clearCache();
      this.returnedBooks$ = this.returnedBookService.getReturnedBooks();
      this.returnedBooks$.subscribe(newlist => {
        this.returnedBooks = newlist;

        this.rerender();
      })
    })
  }


  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    this.returnedBooks$ = this.returnedBookService.getReturnedBooks();

    this.returnedBooks$.subscribe(async result => {
      this.returnedBooks = result;
      //console.log("List returnedBooks = " + JSON.stringify(this.returnedBooks));
      for (let i = 0; i < this.returnedBooks.length; i++) {
        await this.profileService.getProfileById(this.returnedBooks[i].appUserId).then(res => {

            this.userName.push(res.userName);

          }
        );
      }
      for (let i = 0; i < this.returnedBooks.length; i++) {
        await this.bookOfAuthorService.getAuthorBookById(this.returnedBooks[i].boaId).then(res => {
            this.authorname.push(res.name);
            this.bookname.push(res.bookname);

          }
        );
      }
      this.chRef.detectChanges();

      this.dtTrigger.next();


    });


    //console.log("My returnedBooks info = "+JSON.stringify(this.returnedBooksInfo));


    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    this.modalMessageErr = "You can't return more books than available in stock";

    this.modalMessageErr2 = "Return time must be bigger than get time";

    this.modalMessageErr3 = "There is no order so you can't return book";

    // Initializing Add returnedBook properties

    //let validateImageUrl: string = '^(https?:\/\/.*\.(?:png|jpg))$';

    this.appUserId = new FormControl('', [Validators.required]);
    this.boaId = new FormControl('', [Validators.required]);
    this.quantity = new FormControl('', [Validators.required, Validators.min(1), Validators.pattern("^[0-9]*$")]);
    this.returnTime = new FormControl(this.helperService.fixDate(new Date()));

    this.insertForm = this.fb.group({

      'appUserId': this.appUserId,
      'boaId': this.boaId,
      'quantity': this.quantity,
      'returnTime': this.returnTime

    });

    // Initializing Update ReturnedBook properties
    this._appUserId = new FormControl('', [Validators.required]);
    this._boaId = new FormControl('', [Validators.required]);
    this._quantity = new FormControl('', [Validators.required, Validators.min(1), Validators.pattern("^[0-9]*$")]);
    this._returnTime = new FormControl('', [Validators.required]);
    this._id = new FormControl();


    this.updateForm = this.fb.group(
      {
        'id': this._id,
        'appUserId': this._appUserId,
        'boaId': this._boaId,
        'quantity': this._quantity,
        'returnTime': this._returnTime

      });

    //console.log("Update form " + JSON.stringify(this.updateForm.value));

    this.profileService.getProfiles().subscribe(res => {
      this.profile = res;
    });

    this.bookOfAuthorService.getAuthorBooks().subscribe(res => {
      this.bookOfAuthor = res;
    });

  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
