import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {ReturnedBookListComponent} from "./returned-book-list/returned-book-list.component";


const routes: Routes = [
  {path: '', component: ReturnedBookListComponent, canActivate: [AuthGuardService]},
  {path: 'returned-book-list', component:ReturnedBookListComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class ReturnedBookRoutingModule { }
