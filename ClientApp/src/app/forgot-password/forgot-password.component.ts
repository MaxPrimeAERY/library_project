import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AccountService} from "../services/account.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MyToastrService} from "../services/my-toastr.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  insertForm: FormGroup;
  email: FormControl;
  errorMessage: string;
  successMessage: string;


  constructor(private accs: AccountService,
              private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private myToastrService: MyToastrService
  ) {
  }

  ngOnInit() {

    if (this.accs.checkLogInStatus() == true){
      this.router.navigate(['/']);
    }

    this.email = new FormControl('', [Validators.required, Validators.email]);


    this.insertForm = this.formBuilder.group({
      "email": this.email
    })

  }

  onSubmit() {
    let userEmail = this.insertForm.value;
    this.accs.forgotPassword(userEmail.email).subscribe(
      data => {
        this.successMessage = "Reset password link send to email successfully.";
        this.myToastrService.messageSuccess(this.successMessage);
        setTimeout(() => {
          this.successMessage = null;
          this.router.navigate(['/login']);
        }, 5000);
      },
      err => {

        if (err.error.message) {
          this.errorMessage = err.error.message;
        }
      }
    );
  }
}

