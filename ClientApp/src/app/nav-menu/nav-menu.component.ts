import {Component, OnInit} from '@angular/core';
import {Observable,} from "rxjs";
import {AccountService} from "../services/account.service";
import {ProfileService} from "../services/profile.service";
import {AuthorService} from "../services/author.service";
import {BookService} from "../services/book.service";
import {AvatarService} from "../services/avatar.service";
import {BookOfAuthorService} from "../services/bookofauthor.service";
import {BookImageService} from "../services/book-image.service";
import {StockService} from "../services/stock.service";
import {CartService} from "../services/cart.service";
import {OrderService} from "../services/order.service";
import {CartItemsService} from "../services/cart-items.service";
import {OrderItemsService} from "../services/order-items.service";
import {ReturnedBookService} from "../services/returned-book.service";
import {UserRoleService} from "../services/user-role.service";
import {CardOfReaderService} from "../services/card-of-reader.service";
import {CommentsService} from "../services/comments.service";

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {

  userRoleStatus: string;
  userIdAsNumber: number;
  allOrdersCounter: number = 0;
  myOrdersCounter: number = 0;
  allCommentsCounter: number = 0;
  newOrderStatus: string = "New order";
  readyOrderStatus: string = "Ready";

  constructor(private accs: AccountService,
              private profs: ProfileService,
              private auths: AuthorService,
              private avas: AvatarService,
              private boos: BookService,
              private boas: BookOfAuthorService,
              private bi: BookImageService,
              private cartS: CartService,
              private cartItS: CartItemsService,
              private ordS: OrderService,
              private orderItS: OrderItemsService,
              private retBook: ReturnedBookService,
              private sts: StockService,
              private urs: UserRoleService,
              private cor: CardOfReaderService,
              private comm: CommentsService) {
  }

  loginStatus$: Observable<boolean>;
  UserName$: Observable<string>;
  UserRole$: Observable<string>;

  ngOnInit() {

    this.loginStatus$ = this.accs.isLoggedIn;
    this.UserName$ = this.accs.currentUserName;

    if (this.accs.checkLogInStatus() == true) {

      this.accs.currentUserRole.subscribe(result => {
        this.userRoleStatus = result;
      });

      this.accs.currentUserId.subscribe(result => {
        this.userIdAsNumber = +result;
      });

      //console.log("WE ARE HERE");

      if (this.userRoleStatus == "Admin" || this.userRoleStatus == "Librarian") {
        this.ordS.getOrders().subscribe(result => {
          for (let allOrdersCounter of result) {
            if (allOrdersCounter.orderStatus === this.newOrderStatus) {
              this.allOrdersCounter = this.allOrdersCounter + 1;
            }
          }
        });
        this.comm.getComments().subscribe(result => {
          this.allCommentsCounter = 0;
          for (let allCommentsCounter of result) {
            if (allCommentsCounter.viewedBy == null) {
              this.allCommentsCounter = this.allCommentsCounter + 1;
            }
          }
        })

      }
      if (this.userRoleStatus == "Admin" || this.userRoleStatus == "Librarian" || this.userRoleStatus == "Reader") {
        this.ordS.getOrderByUserId(this.userIdAsNumber).subscribe(result => {
          for (let myOrdersCounter of result) {
            if (myOrdersCounter.orderStatus === this.readyOrderStatus) {
              this.myOrdersCounter = this.myOrdersCounter + 1;
            }
          }
        });
      }

    } else {
      this.onLogOut();
    }

  }

  onLogOut() {
    this.profs.clearCache();
    this.auths.clearCache();
    this.avas.clearCache();
    this.boos.clearCache();
    this.boas.clearCache();
    this.bi.clearCache();
    this.cartS.clearCache();
    this.cartItS.clearCache();
    this.ordS.clearCache();
    this.orderItS.clearCache();
    this.retBook.clearCache();
    this.sts.clearCache();
    this.urs.clearCache();
    this.cor.clearCache();
    this.comm.clearCache();
    this.accs.logout();
  }

}
