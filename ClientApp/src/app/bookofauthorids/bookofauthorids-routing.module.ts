import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {BookOfAuthorIdsListComponent} from "./bookofauthorids-list/bookofauthorids-list.component";


const routes: Routes = [
  {path: '', component: BookOfAuthorIdsListComponent, canActivate: [AuthGuardService]},
  {path: 'bookofauthorids-list', component:BookOfAuthorIdsListComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class BookOfAuthorIdsRoutingModule { }
