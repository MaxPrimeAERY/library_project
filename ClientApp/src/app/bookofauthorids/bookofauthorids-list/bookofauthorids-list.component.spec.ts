import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOfAuthorIdsListComponent } from './bookofauthorids-list.component';

describe('BookOfAuthorIdsListComponent', () => {
  let component: BookOfAuthorIdsListComponent;
  let fixture: ComponentFixture<BookOfAuthorIdsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOfAuthorIdsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOfAuthorIdsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
