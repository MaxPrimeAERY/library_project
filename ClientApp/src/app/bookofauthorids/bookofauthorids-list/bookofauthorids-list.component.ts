import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {BookOfAuthorIds} from "../../interfaces/bookofauthorids";
import {BookOfAuthorService} from "../../services/bookofauthor.service";
import {AuthorService} from "../../services/author.service";
import {BookService} from "../../services/book.service";
import {Author} from "../../interfaces/author";
import {Book} from "../../interfaces/book";
import {BookImageService} from "../../services/book-image.service";
import {BookImage} from "../../interfaces/book-image";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-bookofauthorids-list',
  templateUrl: './bookofauthorids-list.component.html',
  styleUrls: ['./bookofauthorids-list.component.scss']
})
export class BookOfAuthorIdsListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  authorId: FormControl;
  bookId: FormControl;
  imageId: FormControl;

  updateForm: FormGroup;
  _authorId: FormControl;
  _bookId: FormControl;
  _imageId: FormControl;
  _id: FormControl;

  ////////////////////////////

  authorName: string[] = [];
  bookName: string[] = [];
  bookImageStr: string[] = [];

  author: Author[] = [];
  book: Book[] = [];
  bookImage: BookImage[] =[];

  // Add Modal
  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', {static: true}) editmodal: TemplateRef<any>;


  // Modal properties
  modalMessage: string;
  modalRef: BsModalRef;
  selectedBookOfAuthorIds: BookOfAuthorIds;
  bookOfAuthorIds$: Promise<BookOfAuthorIds[]>;
  bookOfAuthorIds: BookOfAuthorIds[] = [];
  userRoleStatus: string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, {static: true}) dtElement: DataTableDirective;


  constructor(private bookOfAuthorservice: BookOfAuthorService,
              private authorService: AuthorService,
              private bookService: BookService,
              private bookImageService: BookImageService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) {
  }

  /// Load Add New bookOfAuthorIds Modal
  onAddBookOfAuthorIds() {
    this.modalRef = this.modalService.show(this.modal);
  }

  // Method to Add new BookOfAuthorIds
  onSubmit() {
    let newBookOfAuthorIds = this.insertForm.value;

    this.bookOfAuthorservice.insertBookOfAuthorIds(newBookOfAuthorIds).subscribe(
      result => {
        this.bookOfAuthorservice.clearCache();
        this.bookOfAuthorIds$ = this.bookOfAuthorservice.getBookOfAuthorIds();

        this.bookOfAuthorIds$.then(newlist => {
          this.bookOfAuthorIds = newlist;
          this.modalRef.hide();
          this.insertForm.reset();
          this.rerender();

        });
        this.myToastrService.messageSuccess("New BookOfAuthorIds added");
        window.location.reload();
      },
      error => this.myToastrService.messageError('Could not add BookOfAuthorIds')
    )

  }

  // We will use this method to destroy old table and re-render new table

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing BookOfAuthorIds
  onUpdate() {
    let editBookOfAuthorIds = this.updateForm.value;
    this.bookOfAuthorservice.updateBookOfAuthorIds(editBookOfAuthorIds.id, editBookOfAuthorIds).subscribe(
      result => {
        this.myToastrService.messageSuccess('BookOfAuthorIds Updated');
        this.bookOfAuthorservice.clearCache();
        this.bookOfAuthorIds$ = this.bookOfAuthorservice.getBookOfAuthorIds();
        this.bookOfAuthorIds$.then(updatedlist => {
          this.bookOfAuthorIds = updatedlist;

          this.modalRef.hide();
          this.rerender();
          window.location.reload();
        });
      },
      error => this.myToastrService.messageError('Could Not Update BookOfAuthorIds')
    )
  }

  // Load the update Modal

  onUpdateModal(bookOfAuthorIdsEdit: BookOfAuthorIds): void {
    this._id.setValue(bookOfAuthorIdsEdit.id);
    this._authorId.setValue(bookOfAuthorIdsEdit.authorId);
    this._bookId.setValue(bookOfAuthorIdsEdit.bookId);
    this._imageId.setValue(bookOfAuthorIdsEdit.imageId);

    this.updateForm.setValue({
      'id': this._id.value,
      'authorId': this._authorId.value,
      'bookId': this._bookId.value,
      'imageId': this._imageId.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the bookOfAuthorIds
  onDelete(bookOfAuthorIds: BookOfAuthorIds): void {
    this.bookOfAuthorservice.deleteBookOfAuthorIds(bookOfAuthorIds.id).subscribe(result => {
      this.myToastrService.messageSuccess('Book of author is deleted successfully');

      this.bookOfAuthorservice.clearCache();
      this.bookOfAuthorIds$ = this.bookOfAuthorservice.getBookOfAuthorIds();
      this.bookOfAuthorIds$.then(newlist => {
        this.bookOfAuthorIds = newlist;

        this.rerender();
      })
    })
  }

  onSelect(bookOfAuthorIds: BookOfAuthorIds): void {
    this.selectedBookOfAuthorIds = bookOfAuthorIds;

    this.router.navigateByUrl("/bookOfAuthorIds/" + bookOfAuthorIds.id);
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };


    this.accs.currentUserRole.subscribe(result => {
      this.userRoleStatus = result
    });


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add bookOfAuthorIds properties

    //let validateImageUrl: string = '^(https?:\/\/.*\.(?:png|jpg))$';

    this.authorId = new FormControl('', [Validators.required, Validators.min(1)]);
    this.bookId = new FormControl('', [Validators.required, Validators.min(1)]);
    this.imageId = new FormControl('', [Validators.required]);

    //this.imageUrl = new FormControl('', [Validators.pattern(validateImageUrl)]);

    this.insertForm = this.fb.group({

      'authorId': this.authorId,
      'bookId': this.bookId,
      'imageId': this.imageId
    });

    // Initializing Update BookOfAuthorIds properties
    this._authorId = new FormControl('', [Validators.required, Validators.min(1)]);
    this._bookId = new FormControl('', [Validators.required, Validators.min(1)]);
    this._imageId = new FormControl('', [Validators.required]);
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id': this._id,
        'authorId': this._authorId,
        'bookId': this._bookId,
        'imageId': this._imageId

      });


    this.bookOfAuthorIds$ = this.bookOfAuthorservice.getBookOfAuthorIds();

    this.bookOfAuthorIds$.then(async result => {
      this.bookOfAuthorIds = result;

      for (let i = 0; i < this.bookOfAuthorIds.length; i++) {
        await this.authorService.getAuthorById(this.bookOfAuthorIds[i].authorId).then(res => {
            this.authorName.push(res.name);

          }
        );
        await this.bookService.getBookById(this.bookOfAuthorIds[i].bookId).then(res => {
            this.bookName.push(res.bookName);

          }
        );

        await this.bookImageService.getBookImageById(this.bookOfAuthorIds[i].imageId).then(res => {
            this.bookImageStr.push(res.image);

          }
        );
      }
      //console.log("Author name = " + this.authorName);

      //console.log(this.bookOfAuthorIds);
      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.authorService.getAuthors().subscribe(res => {
      this.author = res;
    });

    this.bookService.getBooks().subscribe(res => {
      this.book = res;
    });

    this.bookImageService.getBookImages().subscribe(res => {
      this.bookImage = res;
    });
  }

  ngOnDestroy() {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
