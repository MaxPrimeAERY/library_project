import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookOfAuthorIdsRoutingModule } from './bookofauthorids-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {BookOfAuthorIdsListComponent} from "./bookofauthorids-list/bookofauthorids-list.component";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {MatOptionModule, MatSelectModule} from "@angular/material";
import {ToastrModule} from "ngx-toastr";


@NgModule({
  declarations: [
    BookOfAuthorIdsListComponent
  ],
  imports: [
    CommonModule,
    BookOfAuthorIdsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    MatSelectModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors
})
export class BookOfAuthorIdsModule { }
