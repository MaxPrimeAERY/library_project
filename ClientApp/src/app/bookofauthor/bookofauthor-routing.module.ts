import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from "../guards/auth-guard.service";
import {BookOfAuthorListComponent} from "./bookofauthor-list/bookofauthor-list.component";
import {BookOfAuthorDetailsComponent} from "./bookofauthor-details/bookofauthor-details.component";
import {BookOfAuthorComplexComponent} from "./bookofauthor-complex/bookofauthor-complex.component";


const routes: Routes = [
  {path: '', component: BookOfAuthorComplexComponent}, //, canActivate: [AuthGuardService]
  {path: 'bookOfAuthor-list', component:BookOfAuthorComplexComponent}, //, canActivate: [AuthGuardService]
  {path: ':id', component:BookOfAuthorDetailsComponent} // , canActivate: [AuthGuardService]
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class BookOfAuthorRoutingModule { }
