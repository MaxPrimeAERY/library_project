import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {BookOfAuthor} from "../../interfaces/bookofauthor";
import {BookOfAuthorService} from "../../services/bookofauthor.service";

@Component({
  selector: 'app-bookofauthor-list',
  templateUrl: './bookofauthor-list.component.html',
  styleUrls: ['./bookofauthor-list.component.scss']
})
export class BookOfAuthorListComponent implements OnChanges {


  // Modal properties
  selectedBookOfAuthor: BookOfAuthor;
  bookOfAuthor$: Observable<BookOfAuthor[]>;
  bookOfAuthor: BookOfAuthor[] = [];
  userRoleStatus: string;
  imageSrc: string = '';
  private hoveredElement: any;
  filterQuery: string = "";

  nameSearch : string = "";
  booknameSearch : string = "";
  genreSearch : string = "";

  @Input() groupFilters: Object;
  @Input() searchByKeyword: string;

  users: any[] = [];
  filteredUsers: any[] = [];

  filteredBookOfAuthor: any[] = [];

  filterItems = [];

  p: number = 1;

  constructor(private bookOfAuthorervice: BookOfAuthorService,
              private chRef: ChangeDetectorRef,
              private router: Router,
              private accs: AccountService) {

  }


  onSelect(bookOfAuthor: BookOfAuthor): void {
    this.selectedBookOfAuthor = bookOfAuthor;
    this.router.navigateByUrl("/bookofauthor/" + bookOfAuthor.id);
  }

  ngOnInit(): void {
    this.loadBookOfAuthors();
    // this.bookOfAuthor$ = this.bookOfAuthorervice.getAuthorBooks();
    // this.bookOfAuthor$.subscribe(result => {
    //   this.bookOfAuthor = result;
    //
    //   //console.log(result);
    //   this.chRef.detectChanges();
    //
    // });

    // this.accs.currentUserRole.subscribe(result => {
    //   this.userRoleStatus = result
    // });

  }
  ngOnChanges(): void {

    if (this.groupFilters) this.filterBookOfAuthorList(this.groupFilters, this.bookOfAuthor);
  }

  filterBookOfAuthorList(filters: any, bookOfAuthor: any): void {
    this.filteredBookOfAuthor = this.bookOfAuthor;     //Reset User List
    const keys = Object.keys(filters);
    const filterBookOfAuthor = bookOfAuthor => keys.every(key => bookOfAuthor[key] === filters[key]);

    this.filteredBookOfAuthor = this.bookOfAuthor.filter(filterBookOfAuthor);

    this.chRef.detectChanges();
  }

  loadBookOfAuthors(): void {
    this.bookOfAuthor$ = this.bookOfAuthorervice.getAuthorBooks();
    this.bookOfAuthor$.subscribe(result => {
      this.bookOfAuthor = result;
      //console.log("This book of author 1= " + this.bookOfAuthor);
      this.filteredBookOfAuthor = this.filteredBookOfAuthor.length > 0 ? this.filteredBookOfAuthor : this.bookOfAuthor;
      //console.log("This book of author 2= " + this.filteredBookOfAuthor);

      for (let onegenre of this.filteredBookOfAuthor) {

        let filterItems =
          {
            value: onegenre.genre,
            checked: false
          };
        this.filterItems.push(filterItems);

      }
    });

  }

  sortBy(prop: string) {
    return this.filteredBookOfAuthor.sort((a, b) => a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
  }

  toggleHover(id) {
    this.hoveredElement = id
  }

  removeHover(id) {
    this.hoveredElement = null;
  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
    //console.log(this.imageSrc);
    // this.image.setValue(this.imageSrc);
    // this._image.setValue(this.imageSrc)
  }


  // ngOnDestroy()
  // {
  //   // Do not forget to unsubscribe
  //   this.dtTrigger.unsubscribe();
  // }

}
