import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOfAuthorListComponent } from './bookofauthor-list.component';

describe('BookOfAuthorListComponent', () => {
  let component: BookOfAuthorListComponent;
  let fixture: ComponentFixture<BookOfAuthorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOfAuthorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOfAuthorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
