import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOfAuthorComplexComponent } from './bookofauthor-complex.component';

describe('BookOfAuthorComplexComponent', () => {
  let component: BookOfAuthorComplexComponent;
  let fixture: ComponentFixture<BookOfAuthorComplexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOfAuthorComplexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOfAuthorComplexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
