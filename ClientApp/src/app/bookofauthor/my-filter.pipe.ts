import {Pipe, PipeTransform} from '@angular/core';
import {BookOfAuthor} from "../interfaces/bookofauthor";

@Pipe({
  name: 'myfilter',
  pure: false
})
export class MyFilterPipe implements PipeTransform {

  transform(items: any[], value: string): any[] {
    if (!items) return [];
    if (!value) return items;

    return items.filter(singleItem =>
      singleItem['bookname'].toLowerCase().includes(value.toLowerCase())
    );

  }


//    ||
//   singleItem['bookname'].toLowerCase().includes(value.toLowerCase()) ||
//   singleItem['genre'].toLowerCase().includes(value.toLowerCase())


}
