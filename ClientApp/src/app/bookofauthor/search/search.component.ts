import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {BookOfAuthor} from "../../interfaces/bookofauthor";
import {BookOfAuthorService} from "../../services/bookofauthor.service";
import {Router} from "@angular/router";
import {Author} from "../../interfaces/author";
import {AuthorService} from "../../services/author.service";
import {BookService} from "../../services/book.service";
import {Book} from "../../interfaces/book";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {


  bookOfAuthor$: Observable<BookOfAuthor[]>;
  bookOfAuthor: BookOfAuthor[] = [];

  books$: Observable<Book[]>;
  books: Book[] = [];
  genre: string[] =[];

  authors$ : Observable<Author[]>;
  authors : Author[] = [];

  form: FormGroup;


  @Output() autoSearch: EventEmitter<string> = new EventEmitter<string>();
  @Output() groupFilters: EventEmitter<any> = new EventEmitter<any>();

  constructor(private bookOfAuthorervice: BookOfAuthorService,
              private authorservice: AuthorService,
              private bookservice: BookService,
              private chRef: ChangeDetectorRef,
              private fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.buildForm();

    this.bookOfAuthor$ = this.bookOfAuthorervice.getAuthorBooks();
    this.bookOfAuthor$.subscribe(result => {
      this.bookOfAuthor = result;
      //console.log(result);
      this.chRef.detectChanges();
    });

    this.books$ = this.bookservice.getBooks();
    this.books$.subscribe(result => {
      this.books = result;
      for (let item of this.books) {
        this.genre.push(item.genre);
      }

      //console.log(result);
      this.chRef.detectChanges();
    });

    this.authors$ = this.authorservice.getAuthors();

    this.authors$.subscribe(result => {
      this.authors = result;

      this.chRef.detectChanges();

    });

  }

  buildForm(): void {
    this.form = this.fb.group({
      name: new FormControl(''),
      genre: new FormControl('')
    });
  }

  drop(): void {
    this.autoSearch.emit("");
    this.form = this.fb.group({
      name: new FormControl(''),
      genre: new FormControl('')
    });
    this.search(this.form.value);
  }



  search(filters: any): void {
    Object.keys(filters).forEach(key => filters[key] === '' ? delete filters[key] : key);
    this.groupFilters.emit(filters);
  }
}
