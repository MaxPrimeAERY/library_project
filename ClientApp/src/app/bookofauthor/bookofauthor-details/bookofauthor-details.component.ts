import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {BookOfAuthor} from "../../interfaces/bookofauthor";
import {BookOfAuthorService} from "../../services/bookofauthor.service";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {CartService} from "../../services/cart.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {CartItemsService} from "../../services/cart-items.service";
import {AccountService} from "../../services/account.service";
import {MyToastrService} from "../../services/my-toastr.service";
import {CartComponent} from "../../cart/cart.component";

@Component({
  selector: 'app-bookofauthor-details',
  templateUrl: './bookofauthor-details.component.html',
  styleUrls: ['./bookofauthor-details.component.scss'],
  providers: [CartComponent]
})
export class BookOfAuthorDetailsComponent implements OnInit {

  @Input() bookOfAuthor: BookOfAuthor;
  bookId: number;
  cartId: number;
  cartItemId: number;


  insertFormCart: FormGroup;
  updateFormCart: FormGroup;

  foundBook: boolean = false;

  cartIdForm: FormControl;
  boaIdForm: FormControl;
  quantityForm: FormControl;

  quantityAsNumber: number;

  userIdAsNumber: number;

  resCart: any;

  @ViewChild('quantityHTML', { static: false }) quantityHTML: ElementRef;

  constructor(
    private accs: AccountService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private bookOfAuthorService: BookOfAuthorService,
    private cartService: CartService,
    private cartItemsService: CartItemsService,
    private cartComponent: CartComponent,
    private myToastrService: MyToastrService
  ) {
  }

  ngOnInit() {
    let id = +this.route.snapshot.params['id'];
    //console.log("Book of author = " + id);
    this.bookId = id;
    this.bookOfAuthorService.getAuthorBookById(id).then(result =>
      this.bookOfAuthor = result,
    );

  }

  async handleAddToCart() {

    if (this.accs.checkLogInStatus() == true) {

      //console.log("quantityInputValue = " + this.quantityHTML.nativeElement.value);
      this.quantityAsNumber = +this.quantityHTML.nativeElement.value;

      await this.accs.currentUserId.subscribe(result => {
        this.userIdAsNumber = +result;
      });
      //this.userIdAsNumber = +localStorage.getItem('userId');

      //console.log("User =" + this.userIdAsNumber);

      this.cartId = +localStorage.getItem('cartId');
      //console.log("Cart id =" + this.cartId);

      await this.cartItemsService.getByCartId(this.cartId).then(async result => {
        this.resCart = result;
        //console.log("Res cart 1= " + this.resCart);


        for (let cart of this.resCart) {
          //console.log("Cart items = " + cart.boaId);
          if (cart.boaId === this.bookId) {
            this.foundBook = true;
            break;
          }
        }

        if (this.foundBook === false) {

          this.cartIdForm = new FormControl(this.cartId);
          this.boaIdForm = new FormControl(this.bookId);
          this.quantityForm = new FormControl(this.quantityAsNumber);

          this.insertFormCart = this.formBuilder.group({
            'cartId': this.cartIdForm,
            'boaId': this.boaIdForm,
            'quantity': this.quantityForm
          });
          let cartDetails = this.insertFormCart.value;
          this.cartId = +localStorage.getItem('cartId');

          this.cartItemsService.insertCartItems(cartDetails).subscribe();

          //console.log("INSERT CARTITEM");
          this.redirectToPage();
        } else {

          await this.cartItemsService.getByBookOfAuthorId(this.bookId, this.cartId).then(result => {
            this.cartItemId = result.id;
          });

          //console.log("Quant before = " + this.quantityAsNumber);
          for (let cart of this.resCart) {
            if (cart.boaId === this.bookId) {
              this.quantityAsNumber = cart.quantity + this.quantityAsNumber;
              //console.log("Cart quant = " + cart.quantity);
              break;
            }
          }

          //console.log("Quant after = " + this.quantityAsNumber);

          this.quantityForm = new FormControl(this.quantityAsNumber);

          this.updateFormCart = this.formBuilder.group({
            'quantity': this.quantityForm
          });
          let cartDetails = this.updateFormCart.value;

          //console.log("Check 1 = " + this.cartItemId);
          //console.log("Check 2 = " + cartDetails);

          await this.cartItemsService.updateCartItems(this.cartItemId, cartDetails).then();

          //console.log("UPDATE CARTITEM");

          this.redirectToPage();

        }

      });

    } else {
      this.myToastrService.messageWarning("You have to login first!");
    }

  }


  redirectToPage() {
    //this.router.navigateByUrl("/bookofauthor/");
    window.location.reload();

  }

}
