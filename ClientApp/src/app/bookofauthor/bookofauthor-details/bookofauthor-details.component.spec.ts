import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOfAuthorDetailsComponent } from './bookofauthor-details.component';

describe('BookOfAuthorDetailsComponent', () => {
  let component: BookOfAuthorDetailsComponent;
  let fixture: ComponentFixture<BookOfAuthorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOfAuthorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOfAuthorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
