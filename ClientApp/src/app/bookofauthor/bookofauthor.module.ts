import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookOfAuthorRoutingModule } from './bookofauthor-routing.module';
import { BookOfAuthorListComponent } from './bookofauthor-list/bookofauthor-list.component';
import { BookOfAuthorDetailsComponent } from './bookofauthor-details/bookofauthor-details.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {AuthGuardService} from "../guards/auth-guard.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "../_helpers/jwt.interceptor";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";
import {
  MatCheckboxModule,
  MatExpansionModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule, MatSelectModule, MatSidenavModule,
  MatTableModule, MatToolbarModule
} from "@angular/material";
import { MyFilterPipe } from './my-filter.pipe';
import { SearchComponent } from './search/search.component';
import { BookOfAuthorComplexComponent } from './bookofauthor-complex/bookofauthor-complex.component';
import {CommentsModule} from "../comments/comments.module";
import {NgxPaginationModule} from "ngx-pagination";


@NgModule({
  declarations: [
    BookOfAuthorListComponent,
    BookOfAuthorDetailsComponent,
    MyFilterPipe,
    SearchComponent,
    BookOfAuthorComplexComponent
  ],
  imports: [
    CommonModule,
    BookOfAuthorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    MatGridListModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatSelectModule,
    CommentsModule,
    NgxPaginationModule
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}], //multi - for several interceptors
})
export class BookOfAuthorModule { }
