import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {AccountService} from "../services/account.service";
import {Router} from "@angular/router";
import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  //styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private accs: AccountService,
    private router: Router,
    private modalService: BsModalService
  ) {
  }

  currentUserId: number;

  insertForm: FormGroup;
  firstname: FormControl;
  lastname: FormControl;
  username: FormControl;
  password: FormControl;
  cpassword: FormControl;
  email: FormControl;
  allowcomments: FormControl;
  modalRef: BsModalRef;
  errorList: string[];
  modalMessage: string;

  @ViewChild('template', {static: true}) modal: TemplateRef<any>;

  onSubmit() {
    let userDetails = this.insertForm.value;

    this.accs.register(userDetails.firstname, userDetails.lastname, userDetails.username, userDetails.password, userDetails.email, userDetails.allowcomments).subscribe(result => {

      this.router.navigate(['/login']);
    }, error => {

      this.errorList = [];

      for (var i = 0; i < error.error.value.length; i++) {
        this.errorList.push(error.error.value[i]);
        //console.log(error.error.value[i]);
      }

      //console.log(error);
      this.modalMessage = "Your Registration Was Unsuccessful";
      this.modalRef = this.modalService.show(this.modal);
    });

  }


  // onDoubleSubmit() {
  //   this.onSubmit();
  //   this.onCartCreate();
  // }


  mustMatch(passwordControl: AbstractControl): ValidatorFn {
    //key contains string with error, boolean = true if we have an error, null if NO error
    return (cpasswordControl: AbstractControl):
      { [key: string]: boolean } | null => {
      //null if haven't initialized yet
      if (!passwordControl && !cpasswordControl) {
        return null;
      }
      //null if another validator has already found an error on the MatchingControl
      if (cpasswordControl.hasError && !passwordControl.hasError) {
        return null;
      }
      //if validation fails
      if (passwordControl.value !== cpasswordControl.value) {
        return {'mustMatch': true};
      } else {
        return null;
      }

    }
  }

  ngOnInit() {

    if (this.accs.checkLogInStatus() == true){
      this.router.navigate(['/']);
    }

    this.firstname = new FormControl('', [Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this.lastname = new FormControl('', [Validators.required,
      Validators.maxLength(12), Validators.minLength(3), Validators.pattern('^([A-Z][a-z]*((\\s[A-Za-z])?[a-z]*)*)$')]);
    this.username = new FormControl('', [Validators.required,
      Validators.maxLength(12), Validators.minLength(3)]);
    this.password = new FormControl('', [Validators.required,
      Validators.minLength(4)]);
    this.cpassword = new FormControl('', [Validators.required,
      this.mustMatch(this.password)]);
    this.email = new FormControl('', [Validators.required,
      Validators.email]);
    this.allowcomments = new FormControl(true); //true by default
    this.errorList = [];

    this.insertForm = this.formBuilder.group({

        'firstname': this.firstname,
        'lastname': this.lastname,
        'username': this.username,
        'password': this.password,
        'cpassword': this.cpassword,
        'email': this.email,
        'allowcomments': this.allowcomments
      }
    );


  }


}
