import {Component, HostListener, OnInit} from '@angular/core';
import {AccountService} from "../services/account.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //@HostListener('window:beforeunload', ['$closeWindow'])

  insertForm: FormGroup;
  Username: FormControl;
  Password: FormControl;
  RememberMe: FormControl;
  returnUrl: string;
  ErrorMessage: string;
  invalidLogin: boolean;


  constructor(private accs: AccountService,
              private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {

    if (this.accs.checkLogInStatus() == true){
      this.router.navigate(['/']);
    }

    this.Username = new FormControl('', [Validators.required]);
    this.Password = new FormControl('', [Validators.required]);
    this.RememberMe = new FormControl(true);

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    this.insertForm = this.formBuilder.group({
      "Username": this.Username,
      "Password": this.Password,
      "RememberMe": this.RememberMe
    });

    this.autoLogin();
  }

  onSubmit() {
    let userlogin = this.insertForm.value;

    this.accs.login(userlogin.Username, userlogin.Password, userlogin.RememberMe).subscribe(res => {
        let token = (<any>res).token;

        //console.log(token);
        //console.log(res.username);
        //console.log(res.userRole);
        //console.log("RemME " + this.RememberMe.value);
        if (this.RememberMe.value === true) {
          localStorage.setItem('tempUserName', userlogin.Username);
          localStorage.setItem('rememberMe', 'yes')

        } else {

          localStorage.setItem('tempUserName', null);
          localStorage.setItem('rememberMe', 'no')
        }
        //console.log("User logged in Successfully");
        this.invalidLogin = false;
        //console.log(this.returnUrl);
        this.router.navigateByUrl(this.returnUrl);

      },
      error => {
        this.invalidLogin = true;
        this.ErrorMessage = error.error.loginError;
        //console.log(this.ErrorMessage);
      }
    );

  }

  autoLogin() {
    const accessTokenObj = localStorage.getItem("token");
    //console.log("AutoLogin 1 " + accessTokenObj);
    // Retrieve rememberMe value from local storage
    const rememberMe = localStorage.getItem('rememberMe');
    const tempUserName = localStorage.getItem('tempUserName');
    //console.log("AutoLogin 2 " + rememberMe);
    //console.log("AutoLogin 3 " + tempUserName);
    if (rememberMe === 'no') {
      this.Username.setValue('');
    } else {
      this.Username.setValue(tempUserName);
    }

    //console.log(accessTokenObj);
    if (accessTokenObj && rememberMe == 'yes') {
      this.router.navigate(['/home']);
    } else {
      //console.log("You need to login")
    }
  }

}
