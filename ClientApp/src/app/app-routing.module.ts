import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {AccessDeniedComponent} from "./errors/access-denied/access-denied.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";
import {ProfileByUserIdComponent} from "./profiles/profile-by-user-id/profile-by-user-id.component";
import {AuthGuardService} from "./guards/auth-guard.service";
import {ConfirmEmailComponent} from "./confirm-email/confirm-email.component";
import {CartComponent} from "./cart/cart.component";
import {OrdersByUserIdComponent} from "./orders/orders-by-user-id/orders-by-user-id.component";
import {CardOfReaderByUserIdComponent} from "./cardofreaders/card-of-reader-by-user-id/card-of-reader-by-user-id.component";
import {CommentsListComponent} from "./comments/comments-list/comments-list.component";


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {path: "home", redirectTo: 'bookofauthor', pathMatch: 'full'},                             //component: HomeComponent
    {path: '', redirectTo: 'bookofauthor', pathMatch: 'full'},              //component: HomeComponent
    {path: 'profiles', loadChildren: './profiles/profiles.module#ProfilesModule'},
    {path: 'authors', loadChildren: './authors/authors.module#AuthorsModule'},
    {path: 'books', loadChildren: './books/books.module#BooksModule'},
    {path: 'bookofauthor', loadChildren: './bookofauthor/bookofauthor.module#BookOfAuthorModule'},//////////////////
    {path: 'bookofauthorids', loadChildren: './bookofauthorids/bookofauthorids.module#BookOfAuthorIdsModule'},//////////////////
    {path: 'avatars', loadChildren: './avatars/avatars.module#AvatarsModule'},
    {path: 'stocks', loadChildren: './stocks/stocks.module#StocksModule'},
    {path: 'bookimages', loadChildren: './bookimages/bookimage.module#BookImageModule'},
    {path: 'returned-book', loadChildren: './returned-book/returned-book.module#ReturnedBookModule'},
    {path: 'user-role', loadChildren: './user-role/user-role.module#UserRoleModule'},
    {path: 'card-of-reader', loadChildren: './cardofreaders/card-of-reader.module#CardOfReaderModule'},
    {path: "comments", loadChildren: './comments/comments.module#CommentsModule'},
    {path: "login", component: LoginComponent},
    {path: "register", component :RegisterComponent},
    {path: "myprofile", component :ProfileByUserIdComponent, canActivate: [AuthGuardService]},
    {path: "forgotpassword", component: ForgotPasswordComponent},
    {path: "resetpassword", component: ResetPasswordComponent},
    {path: "confirmemail", component: ConfirmEmailComponent},
    {path: "access-denied", component: AccessDeniedComponent},
    {path: "cart", component: CartComponent},
    {path: 'orders', loadChildren: './orders/orders.module#OrdersModule'},
    {path: "myorders", component: OrdersByUserIdComponent, canActivate: [AuthGuardService]},
    {path: "mycardofreader", component: CardOfReaderByUserIdComponent, canActivate: [AuthGuardService]},

    //always at the end
    {path: '**', redirectTo: '/home'} // /home

  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
