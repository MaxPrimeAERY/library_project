import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {AccountService} from "../services/account.service";
import {map, take} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  private myCurrentRole: string = null;

  constructor(private accs: AccountService,
              private router: Router) {
  }

  //keep tracking all routes of client
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.accs.isLoggedIn.pipe(take(1), map((loginStatus: boolean) => {
      const destination: string = state.url;
      const profileId = route.params.id;
      const authorId = route.params.id;
      const avatarId = route.params.id;
      const bookId = route.params.id;
      const bookImageId = route.params.id;
      const bookOfAuthorId = route.params.id;
      const bookOfAuthorIdsId = route.params.id;
      const orderId = route.params.id;
      const stockId = route.params.id;
      const returnedBookId = route.params.id;
      const userRoleId = route.params.id;
      const cardOfReaderId = route.params.id;
      const commentId = route.params.id;

      const Admin = "Admin";
      const Librarian = "Librarian";
      const Reader = "Reader";


      this.accs.currentUserRole.subscribe(res => {
        this.myCurrentRole = res;
      });

      //check if user isn't logged in // redirect user to page that he was trying to access
      if (!loginStatus) {
        this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});

        return false;
      }
      //if already logged in
      switch (destination) {
        case '/profiles' :
        case '/profiles/' + profileId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/profiles/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/authors' :
        case '/authors/' + authorId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/authors/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/avatars' : //CHECK PLZ
        case '/avatars/' + avatarId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/avatars/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1
            || this.myCurrentRole.indexOf(Reader) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/books' :
        case '/books/' + bookId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/books/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/bookimages' :
        case '/bookimages/' + bookImageId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/bookimages/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/orders' :
        case '/orders/' + orderId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/orders/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/myprofile' :
        case '/myprofile/' + profileId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1
          || this.myCurrentRole.indexOf(Reader) !== -1) {
            return true;
          }
        }

        case '/bookofauthor' :
        case '/bookofauthor/' + bookOfAuthorId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1
            || this.myCurrentRole.indexOf(Reader) !== -1) {
            return true;
          }
        }

        case '/bookofauthorids' :
        case '/bookofauthorids/' + bookOfAuthorIdsId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/bookofauthorids/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/myorders' :
        case '/myorders/' + orderId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1
            || this.myCurrentRole.indexOf(Reader) !== -1) {
            return true;
          }
        }

        case '/mycardofreader' :
        case '/mycardofreader/' + cardOfReaderId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1
            || this.myCurrentRole.indexOf(Reader) !== -1) {
            return true;
          }
        }

        case '/stocks' :
        case '/stocks/' + stockId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/stocks/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/stocks/patch' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1
            || this.myCurrentRole.indexOf(Reader) !== -1) {
            return true;
          }
        }

        case '/returned-book' :
        case '/returned-book/' + returnedBookId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/returned-book/update' : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/user-role' :
        case '/user-role/' + userRoleId : {
          if (this.myCurrentRole == Admin) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }
        case '/user-role/update' : {
          if (this.myCurrentRole == Admin) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/card-of-reader' :
        case '/card-of-reader/' + cardOfReaderId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        case '/comments' :
        case '/comments/' + commentId : {
          if (this.myCurrentRole.indexOf(Admin) !== -1
            || this.myCurrentRole.indexOf(Librarian) !== -1) {
            return true;
          } else {
            this.router.navigate(['/access-denied']);
            return false;
          }
        }

        default:
          return false;
      }

      return false;
    }));
  }
}
