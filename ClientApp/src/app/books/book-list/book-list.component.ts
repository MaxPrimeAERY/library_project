import {ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Book} from "../../interfaces/book";
import {Observable, Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {BookService} from "../../services/book.service";
import {Router} from "@angular/router";
import {AccountService} from "../../services/account.service";
import {MyToastrService} from "../../services/my-toastr.service";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {

  insertForm: FormGroup;
  bookName: FormControl;
  genre: FormControl;
  shortReview: FormControl;

  updateForm: FormGroup;
  _bookName: FormControl;
  _genre: FormControl;
  _shortReview: FormControl;
  _id: FormControl;

  // Add Modal
  @ViewChild('template', { static: true }) modal : TemplateRef<any>;

  // Update Modal
  @ViewChild('editTemplate', { static: true }) editmodal : TemplateRef<any>;


  // Modal properties
  modalMessage : string;
  modalRef : BsModalRef;
  selectedBook : Book;
  books$ : Observable<Book[]>;
  books : Book[] = [];
  userRoleStatus : string;


  // Datatables Properties
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject(); //fetch first and then render

  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;




  constructor(private bookservice : BookService,
              private modalService: BsModalService,
              private fb: FormBuilder,
              private chRef : ChangeDetectorRef,
              private router: Router,
              private accs: AccountService,
              private myToastrService: MyToastrService) { }

  /// Load Add New book Modal
  onAddBook()
  {
    this.modalRef = this.modalService.show(this.modal);
  }

  // Method to Add new Book
  onSubmit()
  {
    let newBook = this.insertForm.value;

    this.bookservice.insertBook(newBook).subscribe(
      result =>
      {
        this.bookservice.clearCache();
        this.books$ = this.bookservice.getBooks();

        this.books$.subscribe(newlist => {
          this.books = newlist;
          this.modalRef.hide();
          this.insertForm.reset();
          this.rerender();

        });
        this.myToastrService.messageSuccess("New Book added");

      },
      error => this.myToastrService.messageError('Could not add Book')

    )

  }

  // We will use this method to destroy old table and re-render new table

  rerender()
  {
    this.dtElement.dtInstance.then((dtInstance : DataTables.Api) =>
    {
      // Destroy the table first in the current context
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  // Update an Existing Book
  onUpdate()
  {
    let editBook = this.updateForm.value;
    this.bookservice.updateBook(editBook.id, editBook).subscribe(
      result =>
      {
        this.myToastrService.messageSuccess('Book Updated');
        this.bookservice.clearCache();
        this.books$ = this.bookservice.getBooks();
        this.books$.subscribe(updatedlist =>
        {
          this.books = updatedlist;

          this.modalRef.hide();
          this.rerender();
        });
      },
      error => this.myToastrService.messageError('Could Not Update Book')
    )
  }

  // Load the update Modal

  onUpdateModal(bookEdit: Book) : void
  {
    this._id.setValue(bookEdit.id);
    this._bookName.setValue(bookEdit.bookName);
    this._genre.setValue(bookEdit.genre);
    this._shortReview.setValue(bookEdit.shortReview);

    this.updateForm.setValue({
      'id' : this._id.value,
      'bookName' : this._bookName.value,
      'genre' : this._genre.value,
      'shortReview' : this._shortReview.value
    });

    this.modalRef = this.modalService.show(this.editmodal);

  }

  // Method to Delete the book
  onDelete(book : Book) : void
  {
    this.bookservice.deleteBook(book.id).subscribe(result =>
    {
      this.myToastrService.messageSuccess('Book is deleted successfully');
      this.bookservice.clearCache();
      this.books$ = this.bookservice.getBooks();
      this.books$.subscribe(newlist =>
      {
        this.books = newlist;

        this.rerender();
      })
    })
  }

  onSelect(book: Book) : void
  {
    this.selectedBook = book;

    this.router.navigateByUrl("/books/" + book.id);
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 9,
      autoWidth: true,
      order: [[0, 'asc']]
    };

    this.books$ = this.bookservice.getBooks();

    this.books$.subscribe(result => {
      this.books = result;

      this.chRef.detectChanges();

      this.dtTrigger.next();
    });

    this.accs.currentUserRole.subscribe(result => {this.userRoleStatus = result});


    // Modal Message
    this.modalMessage = "All Fields Are Mandatory";

    // Initializing Add book properties

    this.bookName = new FormControl('', [Validators.required]);
    this.genre = new FormControl('', [Validators.required]);
    this.shortReview = new FormControl('', [Validators.required]);

    this.insertForm = this.fb.group({

      'bookName' : this.bookName,
      'genre': this.genre,
      'shortReview' :this.shortReview
    });

    // Initializing Update Book properties
    this._bookName = new FormControl('', [Validators.required]);
    this._genre = new FormControl('', [Validators.required]);
    this._shortReview = new FormControl('', [Validators.required]);
    //this._imageUrl = new FormControl('', [Validators.pattern(validateImageUrl)]);
    this._id = new FormControl();

    this.updateForm = this.fb.group(
      {
        'id' : this._id,
        'bookName' : this._bookName,
        'genre': this._genre,
        'shortReview' :this._shortReview

      });


  }

  ngOnDestroy()
  {
    // Do not forget to unsubscribe
    this.dtTrigger.unsubscribe();
  }

}
