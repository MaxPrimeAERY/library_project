import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BookListComponent} from "./book-list/book-list.component";
import {BookDetailsComponent} from "./book-details/book-details.component";
import {AuthGuardService} from "../guards/auth-guard.service";


const routes: Routes = [
  {path: '', component: BookListComponent, canActivate: [AuthGuardService]},
  {path: 'book-list', component:BookListComponent, canActivate: [AuthGuardService]},
  {path: ':id', component:BookDetailsComponent, canActivate: [AuthGuardService]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class BooksRoutingModule { }
