import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavMenuComponent} from './nav-menu/nav-menu.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ModalModule} from "ngx-bootstrap";
import {DataTablesModule} from "angular-datatables";
import { AccessDeniedComponent } from './errors/access-denied/access-denied.component';
import {JwtInterceptor} from "./_helpers/jwt.interceptor";
import {AuthGuardService} from "./guards/auth-guard.service";
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {ProfileByUserIdComponent} from "./profiles/profile-by-user-id/profile-by-user-id.component";
import {MatCheckboxModule} from "@angular/material";
import { CartComponent } from './cart/cart.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import {OrdersByUserIdComponent} from "./orders/orders-by-user-id/orders-by-user-id.component";
import {ToastContainerModule, ToastrModule} from "ngx-toastr";
import localeUk from '@angular/common/locales/uk';
import localeUkExtra from '@angular/common/locales/extra/uk';
import {registerLocaleData} from "@angular/common";
import {CardOfReaderByUserIdComponent} from "./cardofreaders/card-of-reader-by-user-id/card-of-reader-by-user-id.component";
import { FooterComponent } from './footer/footer.component';
registerLocaleData(localeUk, localeUkExtra);

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AccessDeniedComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ProfileByUserIdComponent,
    CartComponent,
    ConfirmEmailComponent,
    OrdersByUserIdComponent,
    CardOfReaderByUserIdComponent,
    FooterComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DataTablesModule,
    ModalModule.forRoot(),
    MatCheckboxModule,
    ToastrModule.forRoot(),
    ToastContainerModule
  ],

  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}, //multi - for several interceptors
    ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
