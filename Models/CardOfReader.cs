namespace Library_Project.Models
{
    public class CardOfReader
    {
        public int appUserId { get; set; }
        public int boaId { get; set; }
        public int quantityGet { get; set; }
        public int? quantityReturn { get; set; }
        public int? remainedQuantity { get; set; }
    }
}