using System;

namespace Library_Project.Models
{
    public class OrderInfo
    {
        public int id { get; set; }
        public int orderId { get; set; }
        public int boaId { get; set; }
        public string name { get; set; }
        public string bookname { get; set; }
        public string genre { get; set; }
        public string shortReview { get; set; }
        public string image { get; set; }
        public int quantity { get; set; }
        public DateTime? getTime { get; set; }
    }
}