namespace Library_Project.Models
{
    public class AuthorBookDTOExtended
    {
        public int id { get; set; }
        public string name { get; set; }
        public string bookname { get; set; }
        public string genre { get; set; }
        public string short_review { get; set; }
        public string image { get; set; }
        public int? amount { get; set; }
    }
}