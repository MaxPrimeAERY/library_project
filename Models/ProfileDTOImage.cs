namespace Library_Project.Models
{
    public class ProfileDTOImage
    {
        
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string image { get; set; }
        public bool AllowComments { get; set; }
    }
}