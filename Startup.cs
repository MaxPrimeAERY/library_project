using System;
using System.IO;
using System.Text;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Library_Project.DAL.Pgsql.Repository;
using Library_Project.Email;
using Library_Project.Entity;
using Library_Project.Helpers;
using Library_Project.Services;
using Library_Project.Services.Impl;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Library_Project
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(mvcOptions => { mvcOptions.EnableEndpointRouting = false; })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin().
                    AllowAnyHeader().
                    AllowAnyMethod().Build();
            })); 
            
            var connectionString = Configuration.GetConnectionString("Default");
            
            services.AddDbContext<MyAppContext>(opt => opt.UseNpgsql(connectionString));
            
            services.AddIdentity<AppUser, IdentityRole<int>>()
                .AddEntityFrameworkStores<MyAppContext>()
                .AddDefaultTokenProviders();
            
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.User.RequireUniqueEmail = true;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;
            });
            
            // Configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            
            services.AddAuthentication(o => 
                {
                    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidIssuer = appSettings.Site,
                        ValidAudience = appSettings.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(key)
                    };
                });

            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IAvatarRepository, AvatarRepository>();
            services.AddScoped<IBookImageRepository, BookImageRepository>();
            services.AddScoped<IBookOfAuthorRepository, BookOfAuthorRepository>();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<IStockRepository, StockRepository>();
            services.AddScoped<ICartRepository, CartRepository>();
            services.AddScoped<ICartItemsRepository, CartItemsRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderItemsRepository, OrderItemsRepository>();
            services.AddScoped<IReturnedBookRepository, ReturnedBookRepository>();
            services.AddScoped<ICommentsRepository, CommentsRepository>();
            
            
            services.AddTransient<IAuthorService, AuthorService>();
            services.AddTransient<IAvatarService, AvatarService>();
            services.AddTransient<IBookImageService, BookImageService>();
            services.AddTransient<IBookOfAuthorService, BookOfAuthorService>();
            services.AddTransient<IBookService, BookService>();
            services.AddTransient<IProfileService, ProfileService>();
            services.AddTransient<IStockService, StockService>();
            services.AddTransient<ICartService, CartService>();
            services.AddTransient<ICartItemsService, CartItemsService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IOrderItemsService, OrderItemsService>();
            services.AddTransient<IReturnedBookService, ReturnedBookService>();
            services.AddTransient<IUserRoleService, UserRoleService>();
            services.AddTransient<ICardOfReaderService, CardOfReaderService>();
            services.AddTransient<ICommentsService, CommentsService>();

            services.AddSendGridEmailSender();

            services.AddMvc().AddNewtonsoftJson(o =>
            {
                o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);


            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "wwwroot"; });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            app.UseCors("MyPolicy");
            
            
            if (env.IsProduction())
            {
                //app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
//            if (env.IsDevelopment())
//            {
//                app.UseDeveloperExceptionPage();
//            }
//            else
//            {
//                app.UseExceptionHandler("/Error");
//                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
//                app.UseHsts();
//            }

            app.Use( async ( context, next ) => {
                await next();

                if( context.Response.StatusCode == 404 && !Path.HasExtension( context.Request.Path.Value ) ) {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            //app.UseHttpsRedirection();
            app.UseDefaultFiles(); //THIS MUST BE FIRST!!!!
            app.UseStaticFiles(); //THIS MUST BE SECOND!!!!
            //app.UseSpaStaticFiles();
            app.UseAuthentication();
            
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

//            app.UseSpa(spa =>
//            {
//                // To learn more about options for serving an Angular SPA from ASP.NET Core,
//                // see https://go.microsoft.com/fwlink/?linkid=864501
//
//                spa.Options.SourcePath = "ClientApp";
//
//                if (env.IsProduction())
//                {
//                    spa.UseAngularCliServer(npmScript: "start");
//                }
//                
////                if (env.IsDevelopment())
////                {
////                    spa.UseAngularCliServer(npmScript: "start");
////                }
//            });
            //in properties of appsettings.json file set "Copy always" to output directory
            //To start this app for real on host you need to uncomment app.UseDefaultFiles() and app.Use
            //comment app.UseSpa and app.UseSpaStaticFiles
            //services.AddSpaStaticFiles change destination to "wwwroot"
            //in ClientApp/angular.json change default outputPath to "../wwwroot"
        }
    }
}