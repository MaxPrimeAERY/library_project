using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderSvc;

        public OrderController(IOrderService orderSvc)
        {
            _orderSvc = orderSvc;
        }

        // GET api/values
        [HttpGet("[action]")]
        [Authorize(Roles = "Admin,Librarian" )]
        public async Task<ActionResult<IList<Order>>> GetOrders()
        {
            return new ActionResult<IList<Order>>(await _orderSvc.GetAll());
        }


        // GET api/messages/?userId=2
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrderByUserId(int id)
        {
            return new ActionResult<IEnumerable<Order>>(await _orderSvc.GetByAppUserId(id));

//            var message = new ActionResult<IEnumerable<Order>>(await _orderSvc.GetByAppUserId(aspnetusersId);
//            
//            if (message == null)
//            {
//                return new NotFoundObjectResult(null);
//            }
//            return (message);
        }

        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Order>> GetLastOrderByUserId(int id)
        {
            return await _orderSvc.GetLastOrderByUserId(id);
        }

        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Order>> GetOrderById(int id)
        {
            var message = await _orderSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }

            return (message);
        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPost("[action]")]
        public async Task<ActionResult<Order>> AddOrder([FromBody] Order value)
        {
            return await _orderSvc.Create(value);
        }

        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPost("[action]")]
        public void AddOrderMod([FromBody] Order value)
        {
            _orderSvc.InsertOrderMod(value);
        }

        // PUT api/order/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<Order>> UpdateOrder(int id, [FromBody] Order value)
        {
            await _orderSvc.Update(id, value.OrderStatus);
            return await _orderSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteOrder(int id)
        {
            return _orderSvc.Delete(id);
        }
    }
}