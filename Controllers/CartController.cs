using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartSvc;

        public CartController(ICartService cartSvc)
        {
            _cartSvc = cartSvc;
        }
        
        // GET api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<Cart>>> GetCarts()
        {
            return new ActionResult<IList<Cart>>(await _cartSvc.GetAll());
        }
        
        
        // GET api/messages/?userId=2
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Cart>> GetCartByUserId(int id)
        {
            return new ActionResult<Cart>(await _cartSvc.GetByAppUserId(id));
        }

        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Cart>> GetCartById(int id)
        {
            var message = await _cartSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }
            return (message);
        }

        // POST api/values
        [HttpPost("[action]")]
        public async Task<ActionResult<Cart>> AddCart([FromBody] Cart value)
        {
            return await _cartSvc.Create(value);
        }

        // PUT api/cart/5
//        [HttpPut("[action]/{id}")]
//        public async Task<ActionResult<Cart>> UpdateCart(int id, [FromBody] Cart value)
//        {
//            await _cartSvc.Update(id, value.CartItems, value.Quantity);
//            return await _cartSvc.GetById(id);
//        }

        // DELETE api/values/5
//        [HttpDelete("{id}")]
//        public Task Delete(int id)
//        {
//            return _cartSvc.Delete(id);
//        }
    }
}