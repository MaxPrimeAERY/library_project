using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItemsController : ControllerBase
    {
        private readonly IOrderItemsService _orderItemsSvc;

        public OrderItemsController(IOrderItemsService orderItemsSvc)
        {
            _orderItemsSvc = orderItemsSvc;
        }

        // GET api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<OrderItems>>> GetOrderItems()
        {
            return new ActionResult<IList<OrderItems>>(await _orderItemsSvc.GetAll());
        }


        // GET api/messages/?userId=2
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<OrderItems>>> GetByOrderId(int id)
        {
            return new ActionResult<IEnumerable<OrderItems>>(await _orderItemsSvc.GetByOrderId(id));
        }
        
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<OrderItems>>> GetByBookOfAuthorId(int id)
        {
            return new ActionResult<IEnumerable<OrderItems>>(await _orderItemsSvc.GetByBookOfAuthorId(id));
        }
        
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<IEnumerable<OrderInfo>>> GetOrderInfoByUserId(int userId)
        {
            return new ActionResult<IEnumerable<OrderInfo>>(await _orderItemsSvc.GetOrderInfoByUserId(userId));
        }
        
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]/{orderId}")]
        public async Task<ActionResult<IEnumerable<OrderInfo>>> GetOrderInfoByOrderId(int orderId)
        {
            return new ActionResult<IEnumerable<OrderInfo>>(await _orderItemsSvc.GetOrderInfoByOrderId(orderId));
        }
        
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<OrderInfo>>> GetOrderInfo()
        {
            return new ActionResult<IEnumerable<OrderInfo>>(await _orderItemsSvc.GetOrderInfo());
        }
        
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{userId}/{orderId}")]
        public async Task<ActionResult<IEnumerable<OrderInfo>>> GetOrderInfoByUserOrderId(int userId, int orderId)
        {
            return new ActionResult<IEnumerable<OrderInfo>>(await _orderItemsSvc.GetOrderInfoByUserOrderId(userId, orderId));
        }

        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<OrderItems>> GetOrderItemsById(int id)
        {
            var message = await _orderItemsSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }

            return (message);
        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPost("[action]")]
        public async Task<ActionResult<OrderItems>> AddOrderItems([FromBody] OrderItems value)
        {
            return await _orderItemsSvc.Create(value);
        }

        //PUT api/orderItems/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPatch("[action]/{id}")]
        public async Task<ActionResult<OrderItems>> UpdateOrderItems(int id, [FromBody] OrderItems value)
        {
            await _orderItemsSvc.Update(id, value.BoaId, value.Quantity, value.GetTime);
            return await _orderItemsSvc.GetById(id);
        }

        //DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteOrderItems(int id)
        {
            return _orderItemsSvc.Delete(id);
        }
        
        //DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public void DeleteByOrderId(int id)
        {
            _orderItemsSvc.DeleteByOrderId(id);
        }
    }
}