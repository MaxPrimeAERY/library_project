using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Cors;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService _BookSvc;

        public BookController(IBookService BookSvc)
        {
            _BookSvc = BookSvc;
        }

        // GET api/values
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<Book>>> GetBooks()
        {
            return new ActionResult<IList<Book>>(await _BookSvc.GetAll());
        }
        
        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetBookById(int id)
        {
            var Book = await _BookSvc.GetById(id);
            if (Book == null)
            {
                return new NotFoundObjectResult(null);
            }

            return Book;
        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPost("[action]")]
        public async Task<ActionResult<Book>> AddBook([FromBody] Book value)
        {
            return await _BookSvc.Create(value);
        }

        // PUT api/Book/5                                             ?????????????????????????????????
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<Book>> UpdateBook(int id, Book value)
        {
            await _BookSvc.Update(id, value.BookName, value.Genre, value.ShortReview);
            return await _BookSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteBook(int id)
        {
            return _BookSvc.Delete(id);
        }
    }
}