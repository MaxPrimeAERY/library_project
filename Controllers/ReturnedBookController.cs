using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReturnedBookController : ControllerBase
    {
        private readonly IReturnedBookService _returnedBookSvc;

        public ReturnedBookController(IReturnedBookService returnedBookSvc)
        {
            _returnedBookSvc = returnedBookSvc;
        }

        // GET api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<ReturnedBook>>> GetReturnedBooks()
        {
            return new ActionResult<IList<ReturnedBook>>(await _returnedBookSvc.GetAll());
        }


        // GET api/messages/?userId=2
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<ReturnedBook>>> GetReturnedBookByUserId(int id)
        {
            return new ActionResult<IEnumerable<ReturnedBook>>(await _returnedBookSvc.GetByAppUserId(id));
        }

        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<ReturnedBook>>> GetByBookOfAuthorId(int id)
        {
            return new ActionResult<IEnumerable<ReturnedBook>>(await _returnedBookSvc.GetByBookOfAuthorId(id));
        }

        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<ReturnedBook>> GetReturnedBookById(int id)
        {
            var message = await _returnedBookSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }

            return (message);
        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPost("[action]")]
        public async Task<ActionResult<ReturnedBook>> AddReturnedBook([FromBody] ReturnedBook value)
        {
            return await _returnedBookSvc.Create(value);
        }

        // PUT api/returnedBook/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<ReturnedBook>> UpdateReturnedBook(int id, [FromBody] ReturnedBook value)
        {
            await _returnedBookSvc.Update(id, value.Quantity, value.ReturnTime);
            return await _returnedBookSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteReturnedBook(int id)
        {
            return _returnedBookSvc.Delete(id);
        }
    }
}