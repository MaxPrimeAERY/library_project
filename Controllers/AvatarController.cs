using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class AvatarController : ControllerBase
    {
        private readonly IAvatarService _avatarSvc;

        public AvatarController(IAvatarService avatarSvc)
        {
            _avatarSvc = avatarSvc;
        }
        
        // GET api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<Avatar>>> GetAvatars()
        {
            return new ActionResult<IList<Avatar>>(await _avatarSvc.GetAll());
        }
        
        
        // GET api/messages/?userId=2
        [AllowAnonymous]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Avatar>> GetByAppUserId(int id)
        {
            return new ActionResult<Avatar>(await _avatarSvc.GetByAppUserId(id));
        }
        

        // GET api/values/5
//        [HttpGet("{id}")]
//        public async Task<ActionResult<Avatar>> Get(int id)
//        {
//            var message = await _avatarSvc.GetById(id);
//            if (message == null)
//            {
//                return new NotFoundObjectResult(null);
//            }
//            return (message);
//        }

        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPost("[action]")]
        public async Task<ActionResult<Avatar>> AddAvatar([FromBody] Avatar value)
        {
            return await _avatarSvc.Create(value);
        }

        // PUT api/avatar/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<Avatar>> UpdateAvatar(int id, [FromBody] Avatar value)
        {
            await _avatarSvc.Update(id, value.Image);
            return await _avatarSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteAvatar(int id)
        {
            return _avatarSvc.Delete(id);
        }
    }
}