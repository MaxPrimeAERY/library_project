using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.EntityFramework;
using Library_Project.Entity;
using Library_Project.Models;
using Library_Project.MvcExt;
using Library_Project.Services;
using Library_Project.Services.Impl;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _profileService;

        public ProfileController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<AppUser>>> GetProfiles()
        {
            return new ActionResult<IList<AppUser>>(await _profileService.GetAll());
        }
        
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<AppUser>> GetProfileById(int id)
        {
            return new ActionResult<AppUser>(await _profileService.GetById(id));
        }

        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<ProfileDTOImage>> MyProfile([FromRoute] int id)
        {
            return new ActionResult<ProfileDTOImage>(await _profileService.GetFullProfile(id));
        }
        
        [AllowAnonymous]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<SimpleProfile>> SimpleProfile([FromRoute] int id)
        {
            return new ActionResult<SimpleProfile>(await _profileService.GetSimpleProfile(id));
        }

        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{username}")]
        //[ExactQueryParam("username")]
        public async Task<ActionResult<ProfileDTO>> GetAppUserByUserName(string username)
        {
            return new ActionResult<ProfileDTO>(await _profileService.GetAppUserByUserName(username));
        }

        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<AppUser>> UpdateProfile([FromRoute] int id, [FromBody] AppUser formdata)
        {
            await _profileService.Update(id, formdata.UserName, formdata.Email, formdata.PhoneNumber,
                formdata.FirstName, formdata.LastName, formdata.AllowComments);
            return await _profileService.GetById(id);
        }

        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPatch("[action]/{id}")]
        public async Task<ActionResult<AppUser>> PatchProfile([FromRoute] int id, [FromBody] AppUser formdata)
        {
            await _profileService.Patch(id, formdata.FirstName, formdata.LastName, formdata.PhoneNumber);
            return await _profileService.GetById(id);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        [Route("[action]/{id}")]
        public Task DeleteProfile(int id)
        {
            return _profileService.Delete(id);
        }
    }
}