using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class CartItemsController : ControllerBase
    {
        private readonly ICartItemsService _cartItemsSvc;

        public CartItemsController(ICartItemsService cartItemsSvc)
        {
            _cartItemsSvc = cartItemsSvc;
        }

        // GET api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<CartItems>>> GetCartItems()
        {
            return new ActionResult<IList<CartItems>>(await _cartItemsSvc.GetAll());
        }


        // GET api/messages/?userId=2
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<CartItems>>> GetByCartId(int id)
        {
            return new ActionResult<IEnumerable<CartItems>>(await _cartItemsSvc.GetByCartId(id));
        }
        
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}/{cartId}")]
        public async Task<ActionResult<CartItems>> GetByBookOfAuthorId(int id, int cartId)
        {
            return new ActionResult<CartItems>(await _cartItemsSvc.GetByBookOfAuthorId(id, cartId));
        }
        
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<IEnumerable<CartInfo>>> GetCartInfoByUserId(int userId)
        {
            return new ActionResult<IEnumerable<CartInfo>>(await _cartItemsSvc.GetCartInfoByUserId(userId));
        }

        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<CartItems>> GetCartItemsById(int id)
        {
            var message = await _cartItemsSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }

            return (message);
        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPost("[action]")]
        public async Task<ActionResult<CartItems>> AddCartItems([FromBody] CartItems value)
        {
            return await _cartItemsSvc.Create(value);
        }

        //PUT api/cartItems/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<CartItems>> UpdateCartItems(int id, [FromBody] CartItems value)
        {
            await _cartItemsSvc.Update(id, value.Quantity);
            return await _cartItemsSvc.GetById(id);
        }

        //DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteCartItems(int id)
        {
            return _cartItemsSvc.Delete(id);
        }
        
        //DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpDelete("[action]/{id}")]
        public void DeleteByCartId(int id)
        {
            _cartItemsSvc.DeleteByCartId(id);
        }
    }
}