using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentsService _commentsSvc;

        public CommentsController(ICommentsService commentsSvc)
        {
            _commentsSvc = commentsSvc;
        }

        // GET api/values
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<Comments>>> GetComments()
        {
            return new ActionResult<IList<Comments>>(await _commentsSvc.GetAll());
        }


        // GET api/messages/?userId=2
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<Comments>>> GetCommentsByUserId(int id)
        {
            return new ActionResult<IEnumerable<Comments>>(await _commentsSvc.GetByAppUserId(id));
        }

        [AllowAnonymous]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<Comments>>> GetByBookOfAuthorId(int id)
        {
            return new ActionResult<IEnumerable<Comments>>(await _commentsSvc.GetByBookOfAuthorId(id));
        }

        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Comments>> GetCommentById(int id)
        {
            var message = await _commentsSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }

            return (message);
        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPost("[action]")]
        public async Task<ActionResult<Comments>> AddComment([FromBody] Comments value)
        {
            return await _commentsSvc.Create(value);
        }

        // PUT api/comments/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<Comments>> UpdateComment(int id, [FromBody] Comments value)
        {
            await _commentsSvc.Update(id, value.Text, value.Rating, value.ViewedBy);
            return await _commentsSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteComment(int id)
        {
            return _commentsSvc.Delete(id);
        }

        // PATCH api/comments/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPatch("[action]/{id}/{userId}")]
        public async Task<ActionResult<Comments>> PatchMyComment(int id, int userId, [FromBody] Comments value)
        {
            await _commentsSvc.Patch(id, userId, value.Text, value.Rating, value.CommentTime, value.ViewedBy);
            return await _commentsSvc.GetByCommentAppUserId(id, userId);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpDelete("[action]/{id}/{userId}")]
        public Task DeleteMyComment(int id, int userId)
        {
            return _commentsSvc.DeleteMyComment(id, userId);
        }
    }
}