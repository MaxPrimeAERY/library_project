using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library_Project.Email;
using Library_Project.Entity;
using Library_Project.Helpers;
using Library_Project.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole<int>> _roleManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly AppSettings _appSettings;
        private IEmailSender _emailSender;

        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager,
            RoleManager<IdentityRole<int>> roleManager,
            IConfiguration configuration,
            IOptions<AppSettings> appSettings,
            IEmailSender emailSender) //, IEmailService service, IObserverService observerService, ILoginObserver loginObserver, ILogoffObserver logoffObserver)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _appSettings = appSettings.Value;
            _emailSender = emailSender;
        }

        [HttpPost]
        public async Task<object> Register([FromBody] RegisterViewModel model)
        {
            List<string> errorList = new List<string>();

            var user = new AppUser()
            {
                UserName = model.UserName,
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.AllowComments = model.AllowComments;

            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                if (user.UserName.Equals("admin"))
                {
                    await _userManager.AddToRoleAsync(user, "Admin");
                }
                else
                {
                    await _userManager.AddToRoleAsync(user, "Reader");
                }

                //confirm email
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                var confirmEmailUrl =
                    Request.Headers["baseUrlConfirmPassForComponent"]; //очень важная строчка
                var uriBuilder = new UriBuilder(confirmEmailUrl);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query["token"] = code;
                query["userid"] = (user.Id).ToString();
                uriBuilder.Query = query.ToString();
                var urlString = uriBuilder.ToString();

                //var emailBody = $"Please confirm your email by clicking on the link below </br>{urlString}";
                await _emailSender.SendEmailAsync(user.Email, "LOCALHOSTER - Confirm Your Email",
                    "Please confirm your e-mail by clicking this link: <a href=\"" + urlString + "\">click here</a>");
                return Ok(new
                    {username = user.UserName, email = user.Email, status = 1, message = "Registration Successful"});
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                    errorList.Add(error.Description);
                }
            }

            return BadRequest(new JsonResult(errorList));
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {

            var result =
                await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, false);
            if (result.Succeeded)
            {
                // Get the User from Database
                var user = await _userManager.FindByNameAsync(model.Username);

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.Secret));
                double tokenExpiryTime = Convert.ToDouble(_appSettings.ExpireTime);

                if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
                {
                    // Confirmation of email
                    // THen Check If Email Is confirmed
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError(string.Empty, "User hasn't confirmed an email.");

                        return Unauthorized(new
                        {
                            LoginError =
                                "We sent you a confirmation email. Please confirm your registration with localhost to log in."
                        });
                    }

                    var roles = await _userManager.GetRolesAsync(user);

                    var tokenHandler = new JwtSecurityTokenHandler();
                    //string singleRole = null;
                    

                    ClaimsIdentity getClaimsIdentity()
                    {
                        return new ClaimsIdentity(
                            getClaims()
                        );

                        Claim[] getClaims()
                        {
                            var myRoles = roles.ToList();
                            List<Claim> claims = new List<Claim>();
                            claims.Add(new Claim(ClaimTypes.Name, model.Username));
                            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                            claims.Add(new Claim(ClaimTypes.NameIdentifier, (user.Id).ToString()));
                            
                            foreach (var item in myRoles)
                            {
                                claims.Add(new Claim(ClaimTypes.Role, item));
                            }
                            
                            claims.Add(new Claim("LoggedOn", DateTime.Now.ToString()));

                            return claims.ToArray();
                        }
                    }

                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = getClaimsIdentity(),
                        SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature),
                        Issuer = _appSettings.Site,
                        Audience = _appSettings.Audience,
                        Expires = DateTime.UtcNow.AddMinutes(tokenExpiryTime)
                    };

                    // Generate Token

                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    return Ok(new
                    {
                        token = tokenHandler.WriteToken(token), expiration = token.ValidTo//,
                        //username = user.UserName,userId = user.Id//,
                        //userRole = roles.ToList()
                    });
                }
            }

            // return error
            ModelState.AddModelError("", "Username/Password was not Found");
            return Unauthorized(new
                {LoginError = "Please Check the Login Credentials - Invalid Username/Password was entered"});
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ConfirmEmail([FromBody] ConfirmEmailViewModel model)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);
            var confirm = await _userManager.ConfirmEmailAsync(user, Uri.UnescapeDataString(model.Token));

            if (confirm.Succeeded)
            {
                return Ok();
            }

            return Unauthorized();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                Console.WriteLine("User confirmed =" + _userManager.IsEmailConfirmedAsync(user));
                if (!(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    ModelState.AddModelError(string.Empty, "User hasn't confirmed an email.");

                    return Unauthorized(new
                    {
                        LoginError =
                            "We sent you a confirmation email. Please confirm your registration with localhost to log in."
                    });
                }

                if (user == null)
                {
                    return new JsonResult("ERROR");
                }


                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                //var callbackUrl = Url.Action("ResetPassword", "Account", new {code = code, userId = user.Id}, //ResetPassword - если что
                //    protocol: HttpContext.Request.Scheme);
                var baseUrlResetPasswordForComponent =
                    Request.Headers["baseUrlResetPasswordForComponent"]; //очень важная строчка

                var uriBuilder = new UriBuilder(baseUrlResetPasswordForComponent);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query["code"] = code;
                query["userid"] = (user.Id).ToString();
                uriBuilder.Query = query.ToString();
                var urlString = uriBuilder.ToString();
                await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                    $"Please reset your password by clicking here: <a href='{urlString}'>link</a>");
                return Ok();
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Username/Password was not Found");
            return Unauthorized(new
                {LoginError = "Please Check the Login Credentials - Invalid Username/Password was entered"});
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordViewModel model)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);
            var resetPasswordResult =
                await _userManager.ResetPasswordAsync(user, Uri.UnescapeDataString(model.Code), model.Password);

            if (model.Password == null)
            {
                return Unauthorized(new {PassError = "Password == null"});
            }

            if (resetPasswordResult.Succeeded)
            {
                return Ok();
            }

            return Unauthorized(new {PassError = "Invalid token"});
        }

    }
}