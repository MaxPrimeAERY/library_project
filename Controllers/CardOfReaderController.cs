using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Models;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class CardOfReaderController : ControllerBase
    {
        private readonly ICardOfReaderService _cardOfReaderSvc;

        public CardOfReaderController(ICardOfReaderService cardOfReaderSvc)
        {
            _cardOfReaderSvc = cardOfReaderSvc;
        }

        // GET api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<CardOfReader>>> GetCardOfReaders()
        {
            return new ActionResult<IList<CardOfReader>>(await _cardOfReaderSvc.GetAll());
        }


        // GET api/messages/?userId=2
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<CardOfReader>>> GetCardOfReaderByUserId(int id)
        {
            return new ActionResult<IEnumerable<CardOfReader>>(await _cardOfReaderSvc.GetCardOfReaderByUserId(id));
        }
    }
}