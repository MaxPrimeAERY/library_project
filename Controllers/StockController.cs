using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class StockController : ControllerBase
    {
        private readonly IStockService _stockSvc;

        public StockController(IStockService stockSvc)
        {
            _stockSvc = stockSvc;
        }
        
        // GET api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<Stock>>> GetStocks()
        {
            return new ActionResult<IList<Stock>>(await _stockSvc.GetAll());
        }
        
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]/{boaId}")]
        public async Task<ActionResult<Stock>> GetStockByBookOfAuthorId(int boaId)
        {
            return new ActionResult<Stock>(await _stockSvc.GetByBookOfAuthorId(boaId));
        }
//
//        // GET api/values/5
//        [HttpGet("{id}")]
//        public async Task<ActionResult<Stock>> Get(int id)
//        {
//            var message = await _stockSvc.GetById(id);
//            if (message == null)
//            {
//                return new NotFoundObjectResult(null);
//            }
//            return (message);
//        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPost("[action]")]
        public async Task<ActionResult<Stock>> AddStock([FromBody] Stock value)
        {
            return await _stockSvc.Create(value);
        }

        // PUT api/stock/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<Stock>> UpdateStock(int id, [FromBody] Stock value)
        {
            await _stockSvc.Update(id, value.boaId, value.TotalAmount, value.Amount);
            return await _stockSvc.GetById(id);
        }
        
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpPatch("[action]/{id}")]
        public async Task<ActionResult<Stock>> PatchStock(int id, [FromBody] Stock value)
        {
            await _stockSvc.Patch(id, value.boaId, value.Amount);
            return await _stockSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteStock(int id)
        {
            return _stockSvc.Delete(id);
        }
    }
}