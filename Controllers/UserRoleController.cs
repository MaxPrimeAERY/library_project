using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserRoleController : ControllerBase
    {
        private readonly IUserRoleService _userRoleService;

        public UserRoleController(IUserRoleService userRoleService)
        {
            _userRoleService = userRoleService;
        }


        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IList<IdentityUserRole<int>>>> GetUserRoles()
        {
            return new ActionResult<IList<IdentityUserRole<int>>>(await _userRoleService.GetAllUserRoles());
        }
        
        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IList<IdentityRole<int>>>> GetRoles()
        {
            return new ActionResult<IList<IdentityRole<int>>>(await _userRoleService.GetAllRoles());
        }
        
        [HttpGet]
        [Route("[action]/{userId}")]
        public async Task<ActionResult<IList<IdentityUserRole<int>>>> GetUserRolesByUserId(int userId)
        {
            return new ActionResult<IList<IdentityUserRole<int>>>(await _userRoleService.GetUserRolesByUserId(userId));
        }
        
        // POST api/values
        [HttpPost("[action]")]
        public async Task<ActionResult<IdentityUserRole<int>>> AddUserRole([FromBody] IdentityUserRole<int> value)
        {
            return await _userRoleService.Create(value);
        }
        
        [HttpDelete]
        [Route("[action]/{userId}/{roleId}")]
        public Task DeleteUserRole(int userId, int roleId)
        {
            return _userRoleService.Delete(userId, roleId);
        }

    }
}