using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Library_Project.MvcExt;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class BookOfAuthorController : ControllerBase
    {
        private readonly IBookOfAuthorService _bookOfAuthorSvc;

        public BookOfAuthorController(IBookOfAuthorService bookOfAuthorSvc)
        {
            _bookOfAuthorSvc = bookOfAuthorSvc;
        }
        
        [AllowAnonymous]
        [HttpGet("getbookofauthors")]
        public async Task<ActionResult<IList<BookOfAuthor>>> Get()
        {
            return new ActionResult<IList<BookOfAuthor>>(await _bookOfAuthorSvc.GetAll());
        }
        
        [AllowAnonymous]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<BookOfAuthor>> GetBookOfAuthorById(int id)
        {
            var bookofAuthor = await _bookOfAuthorSvc.GetById(id);
            if (bookofAuthor == null)
            {
                return new NotFoundObjectResult(null);
            }

            return bookofAuthor;
        }

        [AllowAnonymous]
        [HttpGet]
        [ExactQueryParam("authorId")]
        public async Task<ActionResult<IEnumerable<BookOfAuthor>>> GetByAuthorId(int authorId)
        {
            return new ActionResult<IEnumerable<BookOfAuthor>>(await _bookOfAuthorSvc.GetByAuthorId(authorId));
        }
        
        [AllowAnonymous]
        [HttpGet]
        [ExactQueryParam("bookId")]
        public async Task<ActionResult<IEnumerable<BookOfAuthor>>> GetByBookId(int bookId)
        {
            return new ActionResult<IEnumerable<BookOfAuthor>>(await _bookOfAuthorSvc.GetByBookId(bookId));
        }
        
        [AllowAnonymous]
        [HttpGet]
        [ExactQueryParam("imageId")]
        public async Task<ActionResult<IEnumerable<BookOfAuthor>>> GetByImageId(int imageId)
        {
            return new ActionResult<IEnumerable<BookOfAuthor>>(await _bookOfAuthorSvc.GetByImageId(imageId));
        }
        
        [AllowAnonymous]
        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<AuthorBookDTOExtended>>> GetAuthorBook()
        {
            return new ActionResult<IEnumerable<AuthorBookDTOExtended>>(await _bookOfAuthorSvc.GetAuthorBook());
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("[action]/{id}")]
        public async Task<ActionResult<AuthorBookDTOExtended>> GetAuthorBookForCart(int id)
        {
            //Console.WriteLine("");
            var bookofAuthor = await _bookOfAuthorSvc.GetAuthorBookById(id);
            if (bookofAuthor == null)
            {
                return new NotFoundObjectResult(null);
            }

            return bookofAuthor;
        }
        
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPost("addbookOfAuthor")]
        public async Task<ActionResult<BookOfAuthor>> Post([FromBody] BookOfAuthor value)
        {
            return await _bookOfAuthorSvc.Create(value);
        }

        // PUT api/Author/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("updatebookOfAuthor/{id}")]
        public async Task<ActionResult<BookOfAuthor>> Put(int id, [FromBody] BookOfAuthor value)
        {
            
            await _bookOfAuthorSvc.Update(id, value.AuthorId, value.BookId, value.ImageId);
            return await _bookOfAuthorSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("deletebookOfAuthor/{id}")]
        public Task Delete(int id)
        {
            return _bookOfAuthorSvc.Delete(id);
        }

    }
}