using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class BookImageController : ControllerBase
    {
        private readonly IBookImageService _bookImageSvc;

        public BookImageController(IBookImageService bookImageSvc)
        {
            _bookImageSvc = bookImageSvc;
        }
        
        // GET api/values
        [Authorize(Roles = "Admin,Librarian,Reader")]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<BookImage>>> GetBookImages()
        {
            return new ActionResult<IList<BookImage>>(await _bookImageSvc.GetAll());
        }

        // GET api/values/5
//        [HttpGet("{id}")]
//        public async Task<ActionResult<BookImage>> Get(int id)
//        {
//            var message = await _bookImageSvc.GetById(id);
//            if (message == null)
//            {
//                return new NotFoundObjectResult(null);
//            }
//            return (message);
//        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPost("[action]")]
        public async Task<ActionResult<BookImage>> AddBookImage([FromBody] BookImage value)
        {
            return await _bookImageSvc.Create(value);
        }

        // PUT api/bookImage/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<BookImage>> UpdateBookImage(int id, [FromBody] BookImage value)
        {
            await _bookImageSvc.Update(id, value.Image);
            return await _bookImageSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteBookImage(int id)
        {
            return _bookImageSvc.Delete(id);
        }
    }
}