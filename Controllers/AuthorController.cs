using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Library_Project.Entity.Entities;
using Library_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace Library_Project.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _AuthorSvc;

        
        public AuthorController(IAuthorService AuthorSvc)
        {
            _AuthorSvc = AuthorSvc;
        }

        // GET api/values
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ActionResult<IList<Author>>> GetAuthors()
        {
            return new ActionResult<IList<Author>>(await _AuthorSvc.GetAll());
        }
        
        // GET api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Author>> GetByAuthorId(int id)
        {
            var Author = await _AuthorSvc.GetById(id);
            if (Author == null)
            {
                return new NotFoundObjectResult(null);
            }

            return Author;
        }

        // POST api/values
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPost("[action]")]
        public async Task<ActionResult<Author>> AddAuthor([FromBody] Author value)
        {
            return await _AuthorSvc.Create(value);
        }

        // PUT api/Author/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<Author>> UpdateAuthor(int id, [FromBody] Author value)
        {
            
            await _AuthorSvc.Update(id, value.Name);
            return await _AuthorSvc.GetById(id);
        }

        // DELETE api/values/5
        [Authorize(Roles = "Admin,Librarian")]
        [HttpDelete("[action]/{id}")]
        public Task DeleteAuthor(int id)
        {
            return  _AuthorSvc.Delete(id);
        }
    }
}