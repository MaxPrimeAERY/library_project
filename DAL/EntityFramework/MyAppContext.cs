using System;
using System.Reflection;
using Library_Project.Entity;
using Microsoft.EntityFrameworkCore;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.CodeAnalysis.Text;

namespace Library_Project.DAL.EntityFramework
{
    public class MyAppContext :IdentityDbContext<AppUser, IdentityRole<int>, int>
    {
        public MyAppContext(DbContextOptions<MyAppContext> options) : base(options)
        {
            bool p = Database.EnsureCreated();

            Console.WriteLine("Db creation status = "+p);
        }

        
        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<Avatar> Avatar { get; set; }
        public virtual DbSet<BookImage> BookImage { get; set; }
        public virtual DbSet<BookOfAuthor> BookOfAuthor { get; set; }
        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Stock> Stock { get; set; }
        public virtual DbSet<Cart> Cart { get; set; }
        public virtual DbSet<CartItems> CartItems { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderItems> OrderItems { get; set; }
        public virtual DbSet<ReturnedBook> ReturnedBook { get; set; }
        public virtual DbSet<Comments> Comments { get; set; }
        
        

        public DbQuery<AuthorBookDTOExtended> AuthorBookDbQuery { get; set; }
        public DbQuery<AuthorBookDTOExtended> AuthorBookExtendedDbQuery { get; set; }
        public DbQuery<ProfileDTO> AppUserDbQuery { get; set; }
        public DbQuery<ProfileDTOImage> AppProfileDbQuery { get; set; }
        public DbQuery<SimpleProfile> AppSimpleProfilesDbQ { get; set; }

        public DbQuery<CartInfo> CartInfoQuery { get; set; }
        
        public DbQuery<OrderInfo> OrderInfoQuery { get; set; }
        
        public DbQuery<CardOfReader> CardOfReaderDbQuery { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<IdentityRole<int>>().HasData(
                new { Id = 1, Name = "Admin", NormalizedName = "ADMIN", ConcurrencyStamp = Guid.NewGuid().ToString()},
                new { Id = 2, Name = "Librarian", NormalizedName = "LIBRARIAN", ConcurrencyStamp = Guid.NewGuid().ToString()},
                new { Id = 3, Name = "Reader", NormalizedName = "READER", ConcurrencyStamp = Guid.NewGuid().ToString()}
            );
            
            modelBuilder.Entity<Avatar>()
                .HasIndex(b => b.AppUserId)
                .IsUnique();
            
            modelBuilder.Entity<Cart>()
                .HasIndex(b => b.AppUserId)
                .IsUnique();
            
            modelBuilder.Entity<Stock>()
                .HasIndex(b => b.boaId)
                .IsUnique();
        }
        
    }
}