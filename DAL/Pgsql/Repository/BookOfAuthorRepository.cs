using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.DAL.Pgsql.Repository
{
    internal class BookOfAuthorRepository : GlobalRepository<int, BookOfAuthor>, IBookOfAuthorRepository
    {
        public BookOfAuthorRepository(MyAppContext ctx) : base(ctx)
        {
        }

        public async Task<IList<BookOfAuthor>> GetByAuthorId(int authorId)
        {
            return await Context.Set<BookOfAuthor>()
                .Where(p => p.AuthorId == authorId)
                .ToListAsync();
        }

        public async Task<IList<BookOfAuthor>> GetByBookId(int bookId)
        {
            return await Context.Set<BookOfAuthor>()
                .Where(p => p.BookId == bookId)
                .ToListAsync();
        }

        public async Task<IList<BookOfAuthor>> GetByImageId(int imageId)
        {
            return await Context.Set<BookOfAuthor>()
                .Where(p => p.ImageId == imageId)
                .ToListAsync();
        }

        //$@"SELECT anu.""Id"", anu.""UserName"", anu.""Email"", anu.""PhoneNumber"", anu.""FirstName"", anu.""LastName"",a.image from ""AspNetUsers"" as anu LEFT JOIN ""Avatar"" as a ON anu.""Id"" = a.""AppUserId"" where ""UserName""= {username} LIMIT 1")

        public async Task<IList<AuthorBookDTOExtended>> GetAuthorBook()
        {
            return await Context.AuthorBookExtendedDbQuery.FromSql(
                    "SELECT boa.id, a.name, b.bookname, b.genre, b.short_review, bi.image, s.amount FROM \"BookOfAuthor\" as boa LEFT JOIN \"Author\" as a  ON a.id =  boa.author_id LEFT JOIN \"Book\" as b  ON b.id =  boa.book_id LEFT JOIN \"BookImage\" as bi  ON bi.id =  boa.image_id LEFT JOIN \"Stock\" as s  ON s.boa_id =  boa.id GROUP BY boa.id, a.id, b.id, bi.id, s.id;")
                .ToListAsync();
        }

        public async Task<AuthorBookDTOExtended> GetAuthorBookById(int id)
        {
            return await Context.AuthorBookExtendedDbQuery.FromSql(
                    $@"SELECT boa.id, a.name, b.bookname, b.genre, b.short_review, bi.image, s.amount FROM ""BookOfAuthor"" as boa LEFT JOIN ""Author"" as a  ON a.id =  boa.author_id LEFT JOIN ""Book"" as b  ON b.id =  boa.book_id LEFT JOIN ""BookImage"" as bi  ON bi.id =  boa.image_id LEFT JOIN ""Stock"" as s ON s.boa_id =  boa.id WHERE boa.id = {id}  GROUP BY boa.id, a.id, b.id, bi.id, s.id")
                .FirstOrDefaultAsync();
        }
    }
}