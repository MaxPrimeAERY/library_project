using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;

namespace Library_Project.DAL.Pgsql.Repository
{
    internal class BookRepository : GlobalRepository<int, Book>, IBookRepository
    {
        public BookRepository(MyAppContext ctx) : base(ctx)
        {
            
        }

    }
}