using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.DAL.Pgsql.Repository 
{
    internal class AuthorRepository : GlobalRepository<int, Author>, IAuthorRepository
    {
        public AuthorRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
//        public async Task<IList<Author>> GetAll()
//        {
//            return await Context.Set<Author>().ToListAsync();
//        }
//        
//        public async Task<int> Insert(Author entity)
//        {
//            var item = await Context.Set<Author>().AddAsync(entity);
//            await SaveAsync();
//            return item.Entity.Id;
//        }
//
//        public async Task<bool> Update(Author entity)
//        {
//            Context.Entry(entity).State = EntityState.Modified;
//            await SaveAsync();
//            return true;
//        }
//
//        public async Task<int> Upsert(Author entity)
//        {
//            if (Object.Equals(entity.Id, default(int)))
//                return await Insert(entity);
//            else
//            {
//                if (await Update(entity))
//                    return entity.Id;
//                else
//                    return default(int);
//            }
//        }
//
//        public async Task<int> GetCount()
//        {
//            return await Context.Set<Author>().CountAsync();
//        }
//
//        public async Task<Author> GetById(int id)
//        {
//            return await Context.Set<Author>()
//                .FindAsync(id);
//        }
//
//        public async Task<bool> Delete(int id)
//        {
//            Author element =
//                await GetById(id);
//            Author result = Context.Set<Author>()
//                .Remove(element).Entity;
//            return 0 != await SaveAsync();
//        }

    }
}