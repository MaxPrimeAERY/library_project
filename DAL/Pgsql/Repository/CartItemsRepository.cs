using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    public class CartItemsRepository : GlobalRepository<int, CartItems>, ICartItemsRepository
    {
        public CartItemsRepository(MyAppContext ctx) : base(ctx)
        {
        }

        public async Task<IList<CartItems>> GetByCartId(int cartId)
        {
            return await Context.Set<CartItems>()
                .Where(p => p.CartId == cartId)
                .ToListAsync();
        }

        public async Task<CartItems> GetByBookOfAuthorId(int boaId, int cartId)
        {
            return await Context.Set<CartItems>()
                .Where(p => p.BoaId == boaId && p.CartId == cartId)
                .FirstOrDefaultAsync();
        }

        public void DeleteByCartId(int cartId)
        {
            Context.Set<CartItems>().RemoveRange(
                Context.Set<CartItems>()
                    .Where(p => p.CartId == cartId));

            Context.SaveChanges();
        }

        [SuppressMessage("ReSharper", "EF1000")]
        public async Task<IList<CartInfo>> GetCartInfoByUserId(int userId)
        {
            //return await 
            return await Context.CartInfoQuery.FromSql(
                $@"SELECT ci.id, a.name, b.bookname, b.genre, b.short_review, bi.image, ci.quantity FROM ""CartItems"" as ci " +
                $@"LEFT JOIN ""Cart"" as c  ON ci.cart_id =  c.id " +
                $@"LEFT JOIN ""BookOfAuthor"" as boa  ON ci.boa_id =  boa.id " +
                $@"LEFT JOIN ""Author"" as a  ON boa.author_id =  a.id " +
                $@"LEFT JOIN ""Book"" as b  ON boa.book_id =  b.id " +
                $@"LEFT JOIN ""BookImage"" as bi  ON boa.image_id = bi.id " +
                $@"WHERE c.""AppUserId"" = {userId} "+
                $@"GROUP BY ci.id, a.id, b.id, bi.id;").ToListAsync();
        }

//        SELECT ci.id, a.name, b.bookname, b.genre, b.short_review, bi.image, ci.quantity FROM "CartItems" as ci
//            LEFT JOIN "Cart" as c  ON ci.cart_id =  c.id
//            LEFT JOIN "BookOfAuthor" as boa  ON ci.boa_id =  boa.id
//            LEFT JOIN "Author" as a  ON boa.author_id =  a.id
//            LEFT JOIN "Book" as b  ON boa.book_id =  b.id
//            LEFT JOIN "BookImage" as bi  ON boa.image_id = bi.id 
//            WHERE ci.id = 7 AND c."AppUserId" = 1
//        GROUP BY ci.id, a.id, b.id, bi.id;
    }
}