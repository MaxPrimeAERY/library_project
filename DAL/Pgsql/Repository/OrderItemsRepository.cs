using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    public class OrderItemsRepository : GlobalRepository<int, OrderItems>, IOrderItemsRepository
    {
        public OrderItemsRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<IList<OrderItems>> GetByOrderId(int orderId)
        {
            return await Context.Set<OrderItems>()
                .Where(p => p.OrderId == orderId)
                .ToListAsync();
        }
        
        public async Task<IList<OrderItems>> GetByBookOfAuthorId(int boaId)
        {
            return await Context.Set<OrderItems>()
                .Where(p => p.BoaId == boaId)
                .ToListAsync();
        }
        
        [SuppressMessage("ReSharper", "EF1000")]
        public async Task<IList<OrderInfo>> GetOrderInfoByUserId(int userId)
        {
            //return await 
            return await Context.OrderInfoQuery.FromSql(
                $@"SELECT oi.id, oi.order_id as ""orderId"", boa_id as ""boaId"", a.name, b.bookname, b.genre, b.short_review as ""shortReview"", bi.image, oi.quantity, oi.get_time as ""getTime"" FROM ""OrderItems"" as oi " +
                $@"LEFT JOIN ""Order"" as o  ON oi.order_id =  o.id " +
                $@"LEFT JOIN ""BookOfAuthor"" as boa  ON oi.boa_id =  boa.id " +
                $@"LEFT JOIN ""Author"" as a  ON boa.author_id =  a.id " +
                $@"LEFT JOIN ""Book"" as b  ON boa.book_id =  b.id " +
                $@"LEFT JOIN ""BookImage"" as bi  ON boa.image_id = bi.id " +
                $@"WHERE o.""AppUserId"" = {userId} "+
                $@"GROUP BY oi.id, a.id, b.id, bi.id;").ToListAsync();
        }
        
        [SuppressMessage("ReSharper", "EF1000")]
        public async Task<IList<OrderInfo>> GetOrderInfoByOrderId(int orderId)
        {
            //return await 
            return await Context.OrderInfoQuery.FromSql(
                $@"SELECT oi.id, oi.order_id as ""orderId"", boa_id as ""boaId"", a.name, b.bookname, b.genre, b.short_review as ""shortReview"", bi.image, oi.quantity, oi.get_time as ""getTime"" FROM ""OrderItems"" as oi " +
                $@"LEFT JOIN ""Order"" as o  ON oi.order_id =  o.id " +
                $@"LEFT JOIN ""BookOfAuthor"" as boa  ON oi.boa_id =  boa.id " +
                $@"LEFT JOIN ""Author"" as a  ON boa.author_id =  a.id " +
                $@"LEFT JOIN ""Book"" as b  ON boa.book_id =  b.id " +
                $@"LEFT JOIN ""BookImage"" as bi  ON boa.image_id = bi.id " +
                $@"WHERE oi.order_id = {orderId} "+
                $@"GROUP BY oi.id, a.id, b.id, bi.id;").ToListAsync();
        }
        
        [SuppressMessage("ReSharper", "EF1000")]
        public async Task<IList<OrderInfo>> GetOrderInfoByUserOrderId(int userId, int orderId)
        {
            //return await 
            return await Context.OrderInfoQuery.FromSql(
                $@"SELECT oi.id, oi.order_id as ""orderId"", boa_id as ""boaId"", a.name, b.bookname, b.genre, b.short_review as ""shortReview"", bi.image, oi.quantity, oi.get_time as ""getTime"" FROM ""OrderItems"" as oi " +
                $@"LEFT JOIN ""Order"" as o  ON oi.order_id =  o.id " +
                $@"LEFT JOIN ""BookOfAuthor"" as boa  ON oi.boa_id =  boa.id " +
                $@"LEFT JOIN ""Author"" as a  ON boa.author_id =  a.id " +
                $@"LEFT JOIN ""Book"" as b  ON boa.book_id =  b.id " +
                $@"LEFT JOIN ""BookImage"" as bi  ON boa.image_id = bi.id " +
                $@"WHERE o.""AppUserId"" = {userId} AND o.id = {orderId} "+
                $@"GROUP BY oi.id, a.id, b.id, bi.id;").ToListAsync();
        }

        public async Task<IList<OrderInfo>> GetOrderInfo()
        {
            //return await 
            return await Context.OrderInfoQuery.FromSql(
                $@"SELECT oi.id, oi.order_id as ""orderId"", boa_id as ""boaId"", a.name, b.bookname, b.genre, b.short_review as ""shortReview"", bi.image, oi.quantity, oi.get_time as ""getTime"" FROM ""OrderItems"" as oi " +
                $@"LEFT JOIN ""Order"" as o  ON oi.order_id =  o.id " +
                $@"LEFT JOIN ""BookOfAuthor"" as boa  ON oi.boa_id =  boa.id " +
                $@"LEFT JOIN ""Author"" as a  ON boa.author_id =  a.id " +
                $@"LEFT JOIN ""Book"" as b  ON boa.book_id =  b.id " +
                $@"LEFT JOIN ""BookImage"" as bi  ON boa.image_id = bi.id " +
                $@"GROUP BY oi.id, a.id, b.id, bi.id;").ToListAsync();
        }

        
        public void DeleteByOrderId(int orderId)
        {
            Context.Set<OrderItems>().RemoveRange(
                Context.Set<OrderItems>()
                    .Where(p => p.OrderId == orderId));

            Context.SaveChangesAsync();
        }
        
    }
}