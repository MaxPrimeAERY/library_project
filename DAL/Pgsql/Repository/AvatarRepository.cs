using System.Collections.Generic;
using System.IO;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    internal class AvatarRepository : GlobalRepository<int, Avatar>, IAvatarRepository
    {
        public AvatarRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
//        public virtual async Task<int> myInsert(Avatar avatar, List<IFormFile> image)
//        {
//            foreach (var item in image)
//            {
//                if (item.Length > 0)
//                {
//                    using (var stream = new MemoryStream())
//                    {
//                        await item.CopyToAsync(stream);
//                        avatar.Image = stream.ToArray();
//                    }
//                }
//            }
//
//            var fullItem = await Context.Set<Avatar>().AddAsync(avatar);
//            await SaveAsync();
//            return fullItem.Entity.Id;
//        }


        public async Task<Avatar> GetByAppUserId(int appUserId)
        {
            return await Context.Set<Avatar>()
                .Where(p => p.AppUserId == appUserId)
                .FirstOrDefaultAsync();
        }
        
    }
}