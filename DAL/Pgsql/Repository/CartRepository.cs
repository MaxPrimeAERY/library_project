using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    internal class CartRepository : GlobalRepository<int, Cart>, ICartRepository
    {
        public CartRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<Cart> GetByAppUserId(int aspnetusersId)
        {
            return await Context.Set<Cart>()
                .Where(p => p.AppUserId == aspnetusersId)
                .FirstOrDefaultAsync();
        }
        
    }
}