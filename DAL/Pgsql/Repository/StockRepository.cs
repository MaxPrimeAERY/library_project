using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;

namespace Library_Project.DAL.Pgsql.Repository
{
    internal class StockRepository : GlobalRepository<int, Stock>, IStockRepository
    {
        public StockRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<Stock> GetByBookOFAuthorId(int boaId)
        {
            return await Context.Set<Stock>()
                .Where(p => p.boaId == boaId)
                .FirstOrDefaultAsync();
        }
    }
}