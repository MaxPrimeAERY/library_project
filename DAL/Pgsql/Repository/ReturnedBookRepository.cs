using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    public class ReturnedBookRepository : GlobalRepository<int, ReturnedBook>, IReturnedBookRepository
    {
        public ReturnedBookRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<IList<ReturnedBook>> GetByAppUserId(int aspnetusersId)
        {
            return await Context.Set<ReturnedBook>()
                .Where(p => p.AppUserId == aspnetusersId)
                .ToListAsync();
        }
        
        public async Task<IList<ReturnedBook>> GetByBookOfAuthorId(int boaId)
        {
            return await Context.Set<ReturnedBook>()
                .Where(p => p.BoaId == boaId)
                .ToListAsync();
        }
    }
}