

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Library_Project.DAL.Pgsql.Repository
{
    internal class OrderRepository : GlobalRepository<int, Order>, IOrderRepository
    {
        public OrderRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
        public void InsertOrderMod(Order order)
        {
            Context.Set<Order>().Add(order);
            Context.SaveChanges();
        }
        
        public async Task<IList<Order>> GetByAppUserId(int aspnetusersId)
        {
            return await Context.Set<Order>()
                .Where(p => p.AppUserId == aspnetusersId)
                .ToListAsync();
        }
        
        public async Task<Order> GetLastOrderByUserId(int aspnetusersId)
        {
            return await Context.Set<Order>()
                .Where(u => u.AppUserId == aspnetusersId)
                .OrderByDescending(p => p.OrderTime)
                .FirstOrDefaultAsync();
        }
        
    }
}