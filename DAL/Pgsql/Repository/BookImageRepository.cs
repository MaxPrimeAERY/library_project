using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    public class BookImageRepository : GlobalRepository<int, BookImage>, IBookImageRepository
    {
        public BookImageRepository(MyAppContext ctx) : base(ctx)
        {
            
        }

    }
}