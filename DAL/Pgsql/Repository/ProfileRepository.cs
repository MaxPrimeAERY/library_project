using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity;
using Library_Project.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    internal class ProfileRepository : GlobalRepository<int, AppUser>, IProfileRepository
    {
        //private readonly MyAppContext _myAppContext;

        public ProfileRepository(MyAppContext context) : base(context)
        {
            
        }

        //SELECT anu."Id", anu."UserName", anu."Email", anu."PhoneNumber", anu."FirstName", anu."LastName", a."image" from "AspNetUsers" as anu, "Avatar" as a where a."AppUserId" = anu."Id"
        public async Task<ProfileDTO> GetAppUserByUserName(string username)
        {
            return await Context.AppUserDbQuery
                .FromSql(
                    $@"SELECT ANU.""Id"", ""UserName"", ""Email"", ""PhoneNumber"", ""FirstName"", ""LastName"" from ""AspNetUsers"" as ANU where ""UserName""= {username}")
                .FirstOrDefaultAsync();
        }

        public async Task<ProfileDTOImage> GetFullProfile(int id)
        {
            return await Context.AppProfileDbQuery
                .FromSql(
                    $@"SELECT anu.""Id"", anu.""UserName"", anu.""Email"", anu.""PhoneNumber"", anu.""FirstName"", anu.""LastName"", anu.""AllowComments"",a.image from ""AspNetUsers"" as anu LEFT JOIN ""Avatar"" as a ON anu.""Id"" = a.""AppUserId"" where anu.""Id""= {id} LIMIT 1")
                .FirstOrDefaultAsync();
        }

        public async Task<SimpleProfile> GetSimpleProfile(int id)
        {
            return await Context.AppSimpleProfilesDbQ
                .FromSql(
                    $@"SELECT anu.""Id"", anu.""UserName"", anu.""FirstName"", anu.""LastName"", anu.""AllowComments"",a.image from ""AspNetUsers"" as anu LEFT JOIN ""Avatar"" as a ON anu.""Id"" = a.""AppUserId"" where anu.""Id""= {id} LIMIT 1")
                .FirstOrDefaultAsync();
        }
    }
}