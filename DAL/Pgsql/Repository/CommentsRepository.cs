using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Library_Project.DAL.EntityFramework.Repository;
using Library_Project.Entity.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.DAL.Pgsql.Repository
{
    public class CommentsRepository : GlobalRepository<int, Comments>, ICommentsRepository
    {
        public CommentsRepository(MyAppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<Comments> GetByCommentAppUserId(int commentId, int aspnetusersId)
        {
            return await Context.Set<Comments>()
                .Where(p => p.Id == commentId && p.AppUserId == aspnetusersId)
                .FirstOrDefaultAsync();
        }
        
        public async Task<IList<Comments>> GetByAppUserId(int aspnetusersId)
        {
            return await Context.Set<Comments>()
                .Where(p => p.AppUserId == aspnetusersId)
                .ToListAsync();
        }
        
        public async Task<IList<Comments>> GetByBookOfAuthorId(int boaId)
        {
            return await Context.Set<Comments>()
                .Where(p => p.BoaId == boaId).OrderByDescending(o => o.CommentTime)
                .ToListAsync();
        }
        
//        public async Task PatchMyComment(int id, int userId)
//        {
//
//            var findComment = Context.Set<Comments>().FirstOrDefault(p => p.Id == id && p.AppUserId == userId);
//
//            if (findComment != null)
//            {
//                Context.Entry(findComment).State = EntityState.Modified;
//                
//                await Context.SaveChangesAsync();
//            }
//        }
        
        public async Task DeleteMyComment(int id, int userId)
        {
            var findComment = Context.Set<Comments>().FirstOrDefault(p => p.Id == id && p.AppUserId == userId);

            if (findComment != null)
            {
                Context.Set<Comments>().Remove(findComment);

                await Context.SaveChangesAsync();
            }
        }
        
        
    }
}