using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.DAL.Abstract
{
    public interface IOrderRepository : IBaseRepository<int, Order>
    {
        Task<IList<Order>> GetByAppUserId(int aspnetusersId);

        Task<Order> GetLastOrderByUserId(int aspnetusersId);

        void InsertOrderMod(Order order);
    }
}