using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Entity.Entities.Abstract;

namespace Library_Project.DAL.Abstract
{
    public interface IBaseRepository<TKey, TEntity> where TEntity : IBaseEntity<TKey>
    {
        Task<TKey> Insert(TEntity entity);
        Task<bool> Update(TEntity entity);
        
        Task<bool> Patch(TEntity entity);
        Task<TKey> Upsert(TEntity entity);

        Task<int> GetCount();

        Task<TEntity> GetById(TKey id);
        Task<bool> Delete(TKey id);

        Task<IList<TEntity>> GetAll();
    }
}