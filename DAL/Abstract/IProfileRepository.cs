using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity;
using Library_Project.Models;

namespace Library_Project.DAL.Abstract
{
    public interface IProfileRepository : IBaseRepository<int, AppUser>
    {
        Task<ProfileDTO> GetAppUserByUserName(string username);
        Task<ProfileDTOImage> GetFullProfile(int id);
        Task<SimpleProfile> GetSimpleProfile(int id);
    }
}