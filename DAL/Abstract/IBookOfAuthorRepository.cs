using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.DAL.Abstract
{
    public interface IBookOfAuthorRepository : IBaseRepository<int, BookOfAuthor>
    {
        Task<IList<BookOfAuthor>> GetByAuthorId(int fullId);
        
        Task<IList<BookOfAuthor>> GetByBookId(int fullId);

        Task<IList<BookOfAuthor>> GetByImageId(int fullId);

        Task<IList<AuthorBookDTOExtended>> GetAuthorBook();
        Task<AuthorBookDTOExtended> GetAuthorBookById(int fullId);
    }
}