using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;

namespace Library_Project.DAL.Abstract
{
    public interface IOrderItemsRepository : IBaseRepository<int, OrderItems>
    {
        Task<IList<OrderItems>> GetByOrderId(int orderId);
        
        Task<IList<OrderItems>> GetByBookOfAuthorId(int boaId);

        Task<IList<OrderInfo>> GetOrderInfoByUserId(int userId);

        Task<IList<OrderInfo>> GetOrderInfoByOrderId(int orderId);

        Task<IList<OrderInfo>> GetOrderInfoByUserOrderId(int userId, int orderId);

        Task<IList<OrderInfo>> GetOrderInfo();

        void DeleteByOrderId(int orderId);
    }
}