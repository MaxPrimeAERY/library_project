using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Microsoft.AspNetCore.Http;

namespace Library_Project.DAL.Abstract
{
    public interface IAvatarRepository  : IBaseRepository<int, Avatar>
    {
        //Task<int> myInsert(Avatar avatar, List<IFormFile> image);
        Task<Avatar> GetByAppUserId(int appUserId);
    }
}