using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.DAL.Abstract
{
    public interface IBookImageRepository  : IBaseRepository<int, BookImage>
    {
        
    }
}