using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.DAL.Abstract
{
    public interface IReturnedBookRepository : IBaseRepository<int, ReturnedBook>
    {
        Task<IList<ReturnedBook>> GetByAppUserId(int aspnetusersId);

        Task<IList<ReturnedBook>> GetByBookOfAuthorId(int boaId);
    }
}