using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.DAL.Abstract
{
    public interface ICartRepository : IBaseRepository<int, Cart>
    {
        Task<Cart> GetByAppUserId(int aspnetusersId);
    }
}