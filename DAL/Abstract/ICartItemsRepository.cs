using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;

namespace Library_Project.DAL.Abstract
{
    public interface ICartItemsRepository : IBaseRepository<int, CartItems>
    {
        Task<IList<CartItems>> GetByCartId(int cartId);
        
        Task<CartItems> GetByBookOfAuthorId(int boaId, int cartId);

        void DeleteByCartId(int cartId);

        Task<IList<CartInfo>> GetCartInfoByUserId(int userId);
    }
}