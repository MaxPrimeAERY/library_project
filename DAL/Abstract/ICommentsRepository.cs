using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.DAL.Abstract
{
    public interface ICommentsRepository : IBaseRepository<int, Comments>
    {
        Task<Comments> GetByCommentAppUserId(int commentId, int aspnetusersId);
        Task<IList<Comments>> GetByAppUserId(int aspnetusersId);

        Task<IList<Comments>> GetByBookOfAuthorId(int boaId);

        //Task PatchMyComment(int id, int userId);
        Task DeleteMyComment(int id, int userId);
    }
}