using System.ComponentModel.DataAnnotations;

namespace Library_Project.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}