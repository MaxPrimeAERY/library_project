using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;

namespace Library_Project.Services
{
    public interface IOrderItemsService
    {
        Task<OrderItems> GetById(int id);
        Task<IList<OrderItems>> GetAll();
        Task<IList<OrderItems>> GetByOrderId(int orderId);
        Task<IList<OrderItems>> GetByBookOfAuthorId(int boaId);
        Task<IList<OrderInfo>> GetOrderInfoByUserId(int userId);
        Task<IList<OrderInfo>> GetOrderInfoByOrderId(int orderId);
        Task<IList<OrderInfo>> GetOrderInfo();
        Task<IList<OrderInfo>> GetOrderInfoByUserOrderId(int userId, int orderId);
        Task Delete(int id);
        void DeleteByOrderId(int orderId);
        Task Update(int orderItemsId, int valueBoaId, int valueQuantity, DateTime? valueGetTime);
        Task<OrderItems> Create(OrderItems value);
    }
}