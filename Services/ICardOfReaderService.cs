using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Models;

namespace Library_Project.Services
{
    public interface ICardOfReaderService
    {
        Task<IList<CardOfReader>> GetAll();
        Task<IList<CardOfReader>> GetCardOfReaderByUserId(int userId);
    }
}