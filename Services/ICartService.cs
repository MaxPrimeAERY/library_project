using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface ICartService
    {
        Task<Cart> GetById(int id);
        Task<IList<Cart>> GetAll();
        Task<Cart> GetByAppUserId(int id);
//        Task Delete(int id);
//        Task Update(int corId, List<int> valueCartItems, List<int> valueQuantity);
        Task<Cart> Create(Cart value);
    }
}