using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface ICommentsService
    {
        Task<Comments> GetById(int id);
        Task<IList<Comments>> GetAll();
        Task<Comments> GetByCommentAppUserId(int id, int userId);
        Task<IList<Comments>> GetByAppUserId(int id);
        Task<IList<Comments>> GetByBookOfAuthorId(int boaId);
        Task<Comments> Create(Comments value);
        Task Update(int id, string valueText, int? valueRating, int? valueViewedBy);

        Task Patch(int commentsId, int userId, string valueText, int? valueRating, DateTime? valueCommentTime,
            int? valueViewedBy);
        Task Delete(int id);
        //Task PatchMyComment(int id, int userId);
        Task DeleteMyComment(int id, int userId);
    }
}