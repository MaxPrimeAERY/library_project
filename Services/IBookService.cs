using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface IBookService
    {
        Task<Book> GetById(int id);
        Task<IList<Book>> GetAll();
        
        Task Delete(int id);
        Task Update(int bookId, string bookName, string genre, string shortReview);
        Task<Book> Create(Book value);
    }
}