using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Library_Project.Services
{
    public interface IUserRoleService
    {
        Task<IdentityUserRole<int>> Create(IdentityUserRole<int> value);
        Task Delete(int userId, int roleId);

        Task<IList<IdentityUserRole<int>>> GetAllUserRoles();

        Task<IList<IdentityUserRole<int>>> GetUserRolesByUserId(int userId);
        
        Task<IList<IdentityRole<int>>> GetAllRoles();

        //Task<IdentityUserRole<int>> GetById(int id);
    }
}