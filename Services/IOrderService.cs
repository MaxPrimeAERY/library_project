using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface IOrderService
    {
        Task<Order> GetById(int id);
        Task<IList<Order>> GetAll();
        Task<IList<Order>> GetByAppUserId(int id);
        Task<Order> GetLastOrderByUserId(int id);
        Task Delete(int id);
        Task Update(int orderId, string valueOrderStatus);
        Task<Order> Create(Order value);

        void InsertOrderMod(Order order);
    }
}