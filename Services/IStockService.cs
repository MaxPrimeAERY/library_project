using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface IStockService
    {
        Task<Stock> GetById(int id);
        Task<IList<Stock>> GetAll();
        Task<Stock> GetByBookOfAuthorId(int id);
        Task Delete(int id);
        Task Update(int corId, int valueBoaId, int valueTotalAmount, int? valueAmount);
        Task Patch(int corId, int valueBoaId,  int? valueAmount);
        Task<Stock> Create(Stock value);
    }
}