using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface IAuthorService
    {
        Task<Author> GetById(int id);
        Task<IList<Author>> GetAll();
        Task Delete(int id);
        Task Update(int authorId, string valueText);
        Task<Author> Create(Author value);
    }
}