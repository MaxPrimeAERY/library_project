using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Microsoft.AspNetCore.Http;

namespace Library_Project.Services
{
    public interface IAvatarService
    {
        Task<Avatar> GetById(int id);
        Task<IList<Avatar>> GetAll();
        Task<Avatar> GetByAppUserId(int id);
        Task Delete(int id);
        Task Update(int authorId, string valueText);
        Task<Avatar> Create(Avatar value);
        //Task<Avatar> myCreate(Avatar avatar, List<IFormFile> image);
    }
}