using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Services
{
    public interface IBookOfAuthorService
    {
        Task<BookOfAuthor> GetById(int id);
        Task<IList<BookOfAuthor>> GetAll();
        Task<IList<BookOfAuthor>> GetByAuthorId(int id);
        Task<IList<BookOfAuthor>> GetByBookId(int id);
        Task<IList<BookOfAuthor>> GetByImageId(int id);
        Task<IList<AuthorBookDTOExtended>>  GetAuthorBook();
        Task<AuthorBookDTOExtended> GetAuthorBookById(int fullId);
        Task Delete(int id);
        Task Update(int bookOfAuthorId, int? valueBooksId, int? valueBookId, int? valueImageId);
        Task<BookOfAuthor> Create(BookOfAuthor value);
    }
}