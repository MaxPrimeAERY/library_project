using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.DAL.EntityFramework;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.Services.Impl
{
    public class UserRoleService : IUserRoleService
    {
        private readonly MyAppContext _myAppContext;

        public UserRoleService(MyAppContext myAppContext)
        {
            _myAppContext = myAppContext;
        }

        public async Task<IList<IdentityUserRole<int>>> GetAllUserRoles()
        {
            return await _myAppContext.Set<IdentityUserRole<int>>().ToListAsync();
        }
        
        public async Task<IList<IdentityUserRole<int>>> GetUserRolesByUserId(int userId)
        {
            return await _myAppContext.Set<IdentityUserRole<int>>().Where(u => u.UserId == userId).ToListAsync();
        }

        public async Task<IList<IdentityRole<int>>> GetAllRoles()
        {
            return await _myAppContext.Set<IdentityRole<int>>().ToListAsync();
        }

        public async Task<IdentityUserRole<int>> Create(IdentityUserRole<int> value)
        {
            var userRole = new IdentityUserRole<int>()
            {
                UserId = value.UserId,
                RoleId = value.RoleId
            };

            await _myAppContext.Set<IdentityUserRole<int>>().AddAsync(userRole);
            await _myAppContext.SaveChangesAsync();

            return userRole;
        }

        public async Task Delete(int userId, int roleId)
        {
            var findProfile = _myAppContext.Set<IdentityUserRole<int>>()
                .FirstOrDefault(p => p.UserId == userId && p.RoleId == roleId);


            if (findProfile != null)
            {
                _myAppContext.Set<IdentityUserRole<int>>().Remove(findProfile);

                await _myAppContext.SaveChangesAsync();
            }
        }
    }
}