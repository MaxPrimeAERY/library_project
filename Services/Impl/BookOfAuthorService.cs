using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;
using Library_Project.Models;
using Microsoft.AspNetCore.Mvc;

namespace Library_Project.Services.Impl
{
    public class BookOfAuthorService : IBookOfAuthorService
    {
        private IBookOfAuthorRepository _bookOfAuthorRepo;

        public BookOfAuthorService(IBookOfAuthorRepository bookOfAuthorRepo)
        {
            _bookOfAuthorRepo = bookOfAuthorRepo;

        }

        public Task<BookOfAuthor> GetById(int id)
        {
            return _bookOfAuthorRepo.GetById(id);
        }

        public Task<IList<BookOfAuthor>> GetAll()
        {
            return _bookOfAuthorRepo.GetAll();
        }
        
        public Task<IList<BookOfAuthor>> GetByAuthorId(int id)
        {
            return _bookOfAuthorRepo.GetByAuthorId(id);
        }
        
        public Task<IList<BookOfAuthor>> GetByBookId(int id)
        {
            return _bookOfAuthorRepo.GetByBookId(id);
        }

        public Task<IList<BookOfAuthor>> GetByImageId(int id)
        {
            return _bookOfAuthorRepo.GetByImageId(id);
        }

        public Task<IList<AuthorBookDTOExtended>> GetAuthorBook()
        {
            return _bookOfAuthorRepo.GetAuthorBook();
        }

        public Task<AuthorBookDTOExtended> GetAuthorBookById(int id)
        {
            return _bookOfAuthorRepo.GetAuthorBookById(id);
        }

        public async Task Delete(int id)
        {
            var res = await _bookOfAuthorRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }

        public  async Task<BookOfAuthor> Create(BookOfAuthor value)
        {
            var bookofAuthor = new BookOfAuthor()
            {
                AuthorId = value.AuthorId,
                BookId = value.BookId,
                ImageId = value.ImageId
            };

            var id = await _bookOfAuthorRepo.Insert(bookofAuthor);
            bookofAuthor.Id = id;
            return bookofAuthor;
        }
        
        public async Task Update(int bookofAuthorId, int? valueAuthorId, int? valueBookId, int? valueImageId)
        {
            var bookofAuthor = await _bookOfAuthorRepo.GetById(bookofAuthorId);
            bookofAuthor.AuthorId = valueAuthorId;
            bookofAuthor.BookId = valueBookId;
            bookofAuthor.ImageId = valueImageId;
            await _bookOfAuthorRepo.Update(bookofAuthor);
        }

    }
}