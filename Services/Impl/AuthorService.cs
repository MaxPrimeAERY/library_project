using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class AuthorService : IAuthorService
    {
        private IAuthorRepository _authRepo;

        public AuthorService(IAuthorRepository authRepo)
        {
            _authRepo = authRepo;

        }
        public Task<Author> GetById(int id)
        {
            return _authRepo.GetById(id);
        }

        public Task<IList<Author>> GetAll()
        {
            return _authRepo.GetAll();
        }

        public async Task Delete(int id)
        {
            var res = await _authRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Author> Create(Author value)
        {
            var Author = new Author()
            {
                Name = value.Name
            };

            var id = await _authRepo.Insert(Author);
            Author.Id = id;
            return Author;
        }

        public async Task Update(int authId, string valueName)
        {
            var Author = await _authRepo.GetById(authId);
            Author.Name = valueName;
            await _authRepo.Update(Author);
        }

       
    }
}