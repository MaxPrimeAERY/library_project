using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class BookImageService : IBookImageService
    {
        private IBookImageRepository _bookImageRepo;

        public BookImageService(IBookImageRepository bookImageRepo)
        {
            _bookImageRepo = bookImageRepo;

        }
        public Task<BookImage> GetById(int id)
        {
            return _bookImageRepo.GetById(id);
        }

        public Task<IList<BookImage>> GetAll()
        {
            return _bookImageRepo.GetAll();
        }
        
        
        public async Task Delete(int id)
        {
            var res = await _bookImageRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<BookImage> Create(BookImage value)
        {
            var BookImage = new BookImage()
            {
                Image = value.Image
            };

            var id = await _bookImageRepo.Insert(BookImage);
            BookImage.Id = id;
            return BookImage;
        }

        public async Task Update(int userId, string valueImage) //надо тестить
        {
            var BookImage = await _bookImageRepo.GetById(userId);
            BookImage.Image = valueImage;
            await _bookImageRepo.Update(BookImage);
        }
    }
}