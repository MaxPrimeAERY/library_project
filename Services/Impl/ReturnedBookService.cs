using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class ReturnedBookService : IReturnedBookService
    {
        private IReturnedBookRepository _returnedBookRepo;

        public ReturnedBookService(IReturnedBookRepository returnedBookRepo)
        {
            _returnedBookRepo = returnedBookRepo;
        }

        public Task<ReturnedBook> GetById(int id)
        {
            return _returnedBookRepo.GetById(id);
        }

        public Task<IList<ReturnedBook>> GetAll()
        {
            return _returnedBookRepo.GetAll();
        }

        public Task<IList<ReturnedBook>> GetByAppUserId(int id)
        {
            return _returnedBookRepo.GetByAppUserId(id);
        }

        public Task<IList<ReturnedBook>> GetByBookOfAuthorId(int id)
        {
            return _returnedBookRepo.GetByBookOfAuthorId(id);
        }

        public async Task Delete(int id)
        {
            var res = await _returnedBookRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }

        public async Task<ReturnedBook> Create(ReturnedBook value)
        {
            var returnedBook = new ReturnedBook()
            {
                AppUserId = value.AppUserId,
                BoaId = value.BoaId,
                Quantity = value.Quantity,
                ReturnTime = value.ReturnTime
            };

            var id = await _returnedBookRepo.Insert(returnedBook);
            returnedBook.Id = id;
            return returnedBook;
        }


        public async Task Update(int returnedBookId, int valueQuantity, DateTime? valueReturnTime)
        {
            var returnedBook = await _returnedBookRepo.GetById(returnedBookId);
            returnedBook.Quantity = valueQuantity;
            returnedBook.ReturnTime = valueReturnTime;
            await _returnedBookRepo.Update(returnedBook);
        }
    }
}