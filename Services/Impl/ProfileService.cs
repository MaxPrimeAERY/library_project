using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity;
using Library_Project.Models;

namespace Library_Project.Services.Impl
{
    public class ProfileService : IProfileService
    {
        private IProfileRepository _profileRepository;

        public ProfileService(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        public Task<ProfileDTO> GetAppUserByUserName(string username)
        {
            return _profileRepository.GetAppUserByUserName(username);
        }

        public Task<ProfileDTOImage> GetFullProfile(int id)
        {
            return _profileRepository.GetFullProfile(id);
        }

        public Task<SimpleProfile> GetSimpleProfile(int id)
        {
            return _profileRepository.GetSimpleProfile(id);
        }

        public Task<AppUser> GetById(int id)
        {
            return _profileRepository.GetById(id);
        }

        public Task<IList<AppUser>> GetAll()
        {
            return _profileRepository.GetAll();
        }

        public async Task Patch(int id, string valueFirst, string valueLast, string valuePhone)
        {
            var profile = await _profileRepository.GetById(id);
            profile.FirstName = valueFirst;
            profile.LastName = valueLast;
            profile.PhoneNumber = valuePhone;
            await _profileRepository.Update(profile);
        }

        public async Task Update(int id, string valueUserName, string valueEmail, string valuePhone, string valueFirst,
            string valueLast, bool valueAllowComments)
        {
            var profile = await _profileRepository.GetById(id);
            profile.UserName = valueUserName;
            profile.Email = valueEmail;
            profile.PhoneNumber = valuePhone;
            profile.FirstName = valueFirst;
            profile.LastName = valueLast;
            profile.AllowComments = valueAllowComments;
            await _profileRepository.Update(profile);
        }

        public async Task Delete(int id)
        {
            var res = await _profileRepository.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
    }
}