using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;
using Library_Project.Models;

namespace Library_Project.Services.Impl
{
    public class OrderItemsService : IOrderItemsService
    {
        private IOrderItemsRepository _orderItemsRepo;

        public OrderItemsService(IOrderItemsRepository orderItemsRepo)
        {
            _orderItemsRepo = orderItemsRepo;
        }

        public Task<OrderItems> GetById(int id)
        {
            return _orderItemsRepo.GetById(id);
        }

        public Task<IList<OrderItems>> GetAll()
        {
            return _orderItemsRepo.GetAll();
        }

        public Task<IList<OrderItems>> GetByOrderId(int id)
        {
            return _orderItemsRepo.GetByOrderId(id);
        }

        public Task<IList<OrderItems>> GetByBookOfAuthorId(int boaId)
        {
            return _orderItemsRepo.GetByBookOfAuthorId(boaId);
        }

        public Task<IList<OrderInfo>> GetOrderInfoByUserId(int userId)
        {
            return _orderItemsRepo.GetOrderInfoByUserId(userId);
        }

        public Task<IList<OrderInfo>> GetOrderInfoByOrderId(int orderId)
        {
            return _orderItemsRepo.GetOrderInfoByOrderId(orderId);
        }

        public Task<IList<OrderInfo>> GetOrderInfo()
        {
            return _orderItemsRepo.GetOrderInfo();
        }

        public Task<IList<OrderInfo>> GetOrderInfoByUserOrderId(int userId, int orderId)
        {
            return _orderItemsRepo.GetOrderInfoByUserOrderId(userId, orderId);
        }

        public async Task Delete(int id)
        {
            var res = await _orderItemsRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }

        public void DeleteByOrderId(int id)
        {
            try
            {
                _orderItemsRepo.DeleteByOrderId(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<OrderItems> Create(OrderItems value)
        {
            var orderItems = new OrderItems()
            {
                OrderId = value.OrderId,
                BoaId = value.BoaId,
                Quantity = value.Quantity,
                GetTime = value.GetTime
            };

            var id = await _orderItemsRepo.Insert(orderItems);
            orderItems.Id = id;
            return orderItems;
        }

        public async Task Update(int orderItemsId, int valueBoaId, int valueQuantity, DateTime? valueGetTime)
        {
            var orderItems = await _orderItemsRepo.GetById(orderItemsId);
            orderItems.BoaId = valueBoaId;
            orderItems.Quantity = valueQuantity;
            orderItems.GetTime = valueGetTime;
            await _orderItemsRepo.Update(orderItems);
        }
    }
}