using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepo;

        public OrderService(IOrderRepository orderRepo)
        {
            _orderRepo = orderRepo;
        }

        public Task<Order> GetById(int id)
        {
            return _orderRepo.GetById(id);
        }

        public Task<IList<Order>> GetAll()
        {
            return _orderRepo.GetAll();
        }

        public Task<IList<Order>> GetByAppUserId(int id)
        {
            return _orderRepo.GetByAppUserId(id);
        }

        public Task<Order> GetLastOrderByUserId(int id)
        {
            return _orderRepo.GetLastOrderByUserId(id);
        }

        public async Task Delete(int id)
        {
            var res = await _orderRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }

        public async Task<Order> Create(Order value)
        {
            var order = new Order()
            {
                AppUserId = value.AppUserId,
                OrderTime = value.OrderTime,
                OrderStatus = value.OrderStatus
            };

            var id = await _orderRepo.Insert(order);
            order.Id = id;
            return order;
        }

        public void InsertOrderMod(Order order)
        {
            _orderRepo.InsertOrderMod(order);
        }

        public async Task Update(int orderId, string valueOrderStatus)
        {
            var order = await _orderRepo.GetById(orderId);
            order.OrderStatus = valueOrderStatus;
            await _orderRepo.Update(order);
        }
    }
}