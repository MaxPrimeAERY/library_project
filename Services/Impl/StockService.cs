using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class StockService : IStockService
    {
        private IStockRepository _stockRepo;

        public StockService(IStockRepository stockRepo)
        {
            _stockRepo = stockRepo;

        }
        public Task<Stock> GetById(int id)
        {
            return _stockRepo.GetById(id);
        }

        public Task<IList<Stock>> GetAll()
        {
            return _stockRepo.GetAll();
        }
        
        
        public Task<Stock> GetByBookOfAuthorId(int id)
        {
            return _stockRepo.GetByBookOFAuthorId(id);
        }

        public async Task Delete(int id)
        {
            var res = await _stockRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Stock> Create(Stock value)
        {
            var stock = new Stock()
            {
                boaId = value.boaId,
                TotalAmount = value.TotalAmount,
                Amount = value.Amount
            };

            var id = await _stockRepo.Insert(stock);
            stock.Id = id;
            return stock;
        }

        public async Task Update(int cardId, int valueBoaId, int valueTotalAmount, int? valueAmount)
        {
            var stock = await _stockRepo.GetById(cardId);
            stock.boaId = valueBoaId;
            stock.TotalAmount = valueTotalAmount;
            stock.Amount = valueAmount;
            await _stockRepo.Update(stock);
        }
        
        public async Task Patch(int cardId, int valueBoaId, int? valueAmount)
        {
            var stock = await _stockRepo.GetById(cardId);
            stock.boaId = valueBoaId;
            stock.Amount = valueAmount;
            await _stockRepo.Patch(stock);
        }
    }
}