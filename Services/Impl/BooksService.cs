using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class BookService : IBookService
    {
        private IBookRepository _bookRepo;

        public BookService(IBookRepository bookRepo)
        {
            _bookRepo = bookRepo;

        }
        public Task<Book> GetById(int id)
        {
            return _bookRepo.GetById(id);
        }

        public Task<IList<Book>> GetAll()
        {
            return _bookRepo.GetAll();
        }
        

        public async Task Delete(int id)
        {
            var res = await _bookRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Book> Create(Book value)
        {
            var book = new Book()
            {
                BookName = value.BookName,
                Genre = value.Genre,
                ShortReview = value.ShortReview
            };

            var id = await _bookRepo.Insert(book);
            book.Id = id;
            return book;
        }

        public async Task Update(int booksId, string bookName, string genre, string shortReview)
        {
            var book = await _bookRepo.GetById(booksId);
            book.BookName = bookName;
            book.Genre = genre;
            book.ShortReview = shortReview;
            await _bookRepo.Update(book);
        }
    }
}