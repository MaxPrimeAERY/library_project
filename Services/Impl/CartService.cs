using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class CartService : ICartService
    {
        private ICartRepository _cartRepo;

        public CartService(ICartRepository cartRepo)
        {
            _cartRepo = cartRepo;

        }
        public Task<Cart> GetById(int id)
        {
            return _cartRepo.GetById(id);
        }

        public Task<IList<Cart>> GetAll()
        {
            return _cartRepo.GetAll();
        }
        
        public Task<Cart> GetByAppUserId(int id)
        {
            return _cartRepo.GetByAppUserId(id);
        }

//        public async Task Delete(int id)
//        {
//            var res = await _cartRepo.Delete(id);
//            if (!res)
//            {
//                throw new Exception("Not found.");
//            }
//        }
        
        public  async Task<Cart> Create(Cart value)
        {
            var cart = new Cart()
            {
                AppUserId = value.AppUserId
            };

            var id = await _cartRepo.Insert(cart);
            cart.Id = id;
            return cart;
        }

//        public async Task Update(int cartId, List<int> valueCartItems, List<int> valueQuantity)
//        {
//            var cart = await _cartRepo.GetById(cartId);
//            cart.CartItems = valueCartItems;
//            cart.Quantity = valueQuantity;
//            await _cartRepo.Update(cart);
//        }
    }
}