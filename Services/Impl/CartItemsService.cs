using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;
using Library_Project.Models;

namespace Library_Project.Services.Impl
{
    public class CartItemsService : ICartItemsService
    {
        private ICartItemsRepository _cartItemsRepo;

        public CartItemsService(ICartItemsRepository cartItemsRepo)
        {
            _cartItemsRepo = cartItemsRepo;

        }
        public Task<CartItems> GetById(int id)
        {
            return _cartItemsRepo.GetById(id);
        }

        public Task<IList<CartItems>> GetAll()
        {
            return _cartItemsRepo.GetAll();
        }
        
        public Task<IList<CartItems>> GetByCartId(int id)
        {
            return _cartItemsRepo.GetByCartId(id);
        }

        public Task<CartItems> GetByBookOfAuthorId(int boaId, int cartId)
        {
            return _cartItemsRepo.GetByBookOfAuthorId(boaId, cartId);
        }

        public Task<IList<CartInfo>> GetCartInfoByUserId(int userId)
        {
            return  _cartItemsRepo.GetCartInfoByUserId(userId);
        }

        public async Task Delete(int id)
        {
            var res = await _cartItemsRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public void DeleteByCartId(int id)
        {
            try
            {
                _cartItemsRepo.DeleteByCartId(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           
        }
        
        public  async Task<CartItems> Create(CartItems value)
        {
            var cartItems = new CartItems()
            {
                CartId = value.CartId,
                BoaId = value.BoaId,
                Quantity = value.Quantity
            };

            var id = await _cartItemsRepo.Insert(cartItems);
            cartItems.Id = id;
            return cartItems;
        }

        public async Task Update(int cartItemsId, int valueQuantity)
        {
            var cartItems = await _cartItemsRepo.GetById(cartItemsId);
            cartItems.Quantity = valueQuantity;
            await _cartItemsRepo.Update(cartItems);
        }
    }
}