using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;

namespace Library_Project.Services.Impl
{
    public class CommentsService : ICommentsService
    {
        private ICommentsRepository _commentsRepo;

        public CommentsService(ICommentsRepository commentsRepo)
        {
            _commentsRepo = commentsRepo;
        }

        public Task<Comments> GetById(int id)
        {
            return _commentsRepo.GetById(id);
        }
        
        public Task<Comments> GetByCommentAppUserId(int id, int userId)
        {
            return _commentsRepo.GetByCommentAppUserId(id, userId);
        }


        public Task<IList<Comments>> GetAll()
        {
            return _commentsRepo.GetAll();
        }

        public Task<IList<Comments>> GetByAppUserId(int id)
        {
            return _commentsRepo.GetByAppUserId(id);
        }

        public Task<IList<Comments>> GetByBookOfAuthorId(int id)
        {
            return _commentsRepo.GetByBookOfAuthorId(id);
        }

        public async Task Delete(int id)
        {
            var res = await _commentsRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public async Task DeleteMyComment(int id, int userId)
        {
            await _commentsRepo.DeleteMyComment(id, userId);
        }

        public async Task<Comments> Create(Comments value)
        {
            var comments = new Comments()
            {
                AppUserId = value.AppUserId,
                BoaId = value.BoaId,
                Text = value.Text,
                Rating = value.Rating,
                CommentTime = value.CommentTime,
                ViewedBy = value.ViewedBy
            };

            var id = await _commentsRepo.Insert(comments);
            comments.Id = id;
            return comments;
        }


        public async Task Update(int commentsId, string valueText, int? valueRating, int? valueViewedBy)
        {
            var comments = await _commentsRepo.GetById(commentsId);
            comments.Text = valueText;
            comments.Rating = valueRating;
            comments.ViewedBy = valueViewedBy;
            await _commentsRepo.Update(comments);
        }
        
        public async Task Patch(int commentsId, int userId, string valueText, int? valueRating, DateTime? valueCommentTime, int? valueViewedBy)
        {
            var comments = await _commentsRepo.GetByCommentAppUserId(commentsId, userId);
            comments.Text = valueText;
            comments.Rating = valueRating;
            comments.CommentTime = valueCommentTime;
            comments.ViewedBy = valueViewedBy;
            await _commentsRepo.Patch(comments);
        }

    }
}