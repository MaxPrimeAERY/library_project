using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.DAL.Abstract;
using Library_Project.Entity.Entities;
using Microsoft.AspNetCore.Http;

namespace Library_Project.Services.Impl
{
    public class AvatarService : IAvatarService
    {
        private IAvatarRepository _avatarRepo;

        public AvatarService(IAvatarRepository avatarRepo)
        {
            _avatarRepo = avatarRepo;

        }
        public Task<Avatar> GetById(int id)
        {
            return _avatarRepo.GetById(id);
        }

        public Task<IList<Avatar>> GetAll()
        {
            return _avatarRepo.GetAll();
        }

        public Task<Avatar> GetByAppUserId(int id)
        {
            return _avatarRepo.GetByAppUserId(id);
        }
        
        public async Task Delete(int id)
        {
            var res = await _avatarRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Avatar> Create(Avatar value)
        {
            var Avatar = new Avatar()
            {
                Image = value.Image,
                AppUserId = value.AppUserId
            };

            var id = await _avatarRepo.Insert(Avatar);
            Avatar.Id = id;
            return Avatar;
        }
        
//        public  async Task<Avatar> myCreate(Avatar value, List<IFormFile> image)
//        {
//            
//            var Avatar = new Avatar()
//            {
//                Image = value.Image,
//                AppUserId = value.AppUserId
//            };
//
//            var id = await _avatarRepo.myInsert(Avatar, image);
//            Avatar.Id = id;
//            return Avatar;
//        }
        
        

        public async Task Update(int userId, string valueImage) //надо тестить
        {
            var Avatar = await _avatarRepo.GetById(userId);
            Avatar.Image = valueImage;
            await _avatarRepo.Update(Avatar);
        }
    }
}