using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Library_Project.DAL.EntityFramework;
using Library_Project.Models;
using Microsoft.EntityFrameworkCore;

namespace Library_Project.Services.Impl
{
    public class CardOfReaderService : ICardOfReaderService
    {
        private readonly MyAppContext _myAppContext;

        public CardOfReaderService(MyAppContext myAppContext)
        {
            _myAppContext = myAppContext;
        }
        
//        SELECT o."AppUserId" as "appUserId", oi.boa_id as "boaId", SUM(oi.quantity) as "quantityGet", SUM(rb.quantity) as "quantityReturn", SUM(oi.quantity - rb.quantity) as "remainedQuantity" FROM "OrderItems" oi
//            LEFT JOIN "Order" as o  ON oi.order_id =  o.id
//            LEFT JOIN "BookOfAuthor" as boa  ON oi.boa_id =  boa.id
//            LEFT JOIN "Author" as a  ON boa.author_id =  a.id
//            LEFT JOIN "Book" as b  ON boa.book_id =  b.id
//            LEFT JOIN "ReturnedBook" as rb  ON o."AppUserId" =  rb."AppUserId"
//        WHERE o."AppUserId" = 1
//        GROUP BY oi.boa_id, o."AppUserId";

        public async Task<IList<CardOfReader>> GetAll()
        {
            return await _myAppContext.CardOfReaderDbQuery.FromSql(
                    $@"SELECT o.""AppUserId"" as ""appUserId"", oi.boa_id as ""boaId"", SUM(oi.quantity) as ""quantityGet"", SUM(rb.quantity) as ""quantityReturn"", SUM(oi.quantity - rb.quantity) as ""remainedQuantity"" FROM ""OrderItems"" oi "+
                    $@"LEFT JOIN ""Order"" as o  ON oi.order_id =  o.id "+
                    $@"LEFT JOIN ""ReturnedBook"" as rb  ON o.""AppUserId"" =  rb.""AppUserId""  AND oi.boa_id = rb.boa_id "+
                    $@"GROUP BY oi.boa_id, o.""AppUserId"" ")
                .ToListAsync();
        }

        [SuppressMessage("ReSharper", "EF1000")]
        public async Task<IList<CardOfReader>> GetCardOfReaderByUserId(int userId)
        {
            return await _myAppContext.CardOfReaderDbQuery.FromSql(
                    $@"SELECT o.""AppUserId"" as ""appUserId"", oi.boa_id as ""boaId"", SUM(oi.quantity) as ""quantityGet"", SUM(rb.quantity) as ""quantityReturn"", SUM(oi.quantity - rb.quantity) as ""remainedQuantity"" FROM ""OrderItems"" oi "+
                $@"LEFT JOIN ""Order"" as o  ON oi.order_id =  o.id "+
                $@"LEFT JOIN ""ReturnedBook"" as rb  ON o.""AppUserId"" =  rb.""AppUserId""  AND oi.boa_id = rb.boa_id "+
                $@"WHERE o.""AppUserId"" = {userId}  AND o.order_status LIKE 'Finished' "+
                $@"GROUP BY oi.boa_id, o.""AppUserId"" ")
                .ToListAsync();
        }
        
    }
}