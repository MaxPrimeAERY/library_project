using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface IBookImageService
    {
        Task<BookImage> GetById(int id);
        Task<IList<BookImage>> GetAll();
        Task Delete(int id);
        Task Update(int bookId, string valueText);
        Task<BookImage> Create(BookImage value);
    }
}