using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity;
using Library_Project.Models;

namespace Library_Project.Services
{
    public interface IProfileService
    {
        Task<ProfileDTO> GetAppUserByUserName(string username);
        
        Task<ProfileDTOImage> GetFullProfile(int id);
        
        Task<SimpleProfile> GetSimpleProfile(int id);
        
        Task<AppUser> GetById(int id);

        Task<IList<AppUser>> GetAll();
        Task Patch(int id, string valueFirst, string valueLast, string valuePhone);
        
        Task Update(int id, string valueUserName, string valueEmail, string valuePhone,
            string valueFirst, string valueLast, bool valueAllowComments);
        
        Task Delete(int id);
    }
}