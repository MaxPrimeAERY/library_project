using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;

namespace Library_Project.Services
{
    public interface IReturnedBookService
    {
        Task<ReturnedBook> GetById(int id);
        Task<IList<ReturnedBook>> GetAll();
        Task<IList<ReturnedBook>> GetByAppUserId(int id);
        Task<IList<ReturnedBook>> GetByBookOfAuthorId(int boaId);
        Task Delete(int id);
        Task Update(int id, int valueQuantity, DateTime? valueReturnTime);
        Task<ReturnedBook> Create(ReturnedBook value);
    }
}