using System.Collections.Generic;
using System.Threading.Tasks;
using Library_Project.Entity.Entities;
using Library_Project.Models;

namespace Library_Project.Services
{
    public interface ICartItemsService
    {
        Task<CartItems> GetById(int id);
        Task<IList<CartItems>> GetAll();
        Task<IList<CartItems>> GetByCartId(int cartId);
        Task<CartItems> GetByBookOfAuthorId(int boaId, int cartId);
        Task<IList<CartInfo>> GetCartInfoByUserId(int userId);
        Task Delete(int id);
        void DeleteByCartId(int id);
        
        Task Update(int cartItemsId, int valueQuantity);
        Task<CartItems> Create(CartItems value);
    }
}