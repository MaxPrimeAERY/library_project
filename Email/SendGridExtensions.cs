using Library_Project.EmailServices;
using Microsoft.Extensions.DependencyInjection;

namespace Library_Project.Email
{
    public static class SendGridExtensions
    {
        public static IServiceCollection AddSendGridEmailSender(this IServiceCollection services)
        {
            services.AddTransient<IEmailSender, SendGridEmailSender>();

            return services;
        }
    }
}