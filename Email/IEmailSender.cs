using System.Threading.Tasks;

namespace Library_Project.Email
{
    public interface IEmailSender
    {
        Task<SendEmailResponse> SendEmailAsync(string userEmail, string emailSubject, string message);
        
        
        
    }
}